local client_al = require'epp_client_al'
local create_client_instance = client_al.create_client_instance

local query_tpl = require'epp_query_templates'

local tls_opts = {
  mode = "client",
  protocol = "tlsv1",
  key = "/home/ded/new/test/clientAkey.pem",
  certificate = "/home/ded/new/test/clientA.pem",
  cafile = "/home/ded/new/test/rootA.pem",
  verify = {"peer", "fail_if_no_peer_cert"},
  options = {"all", "no_sslv2"},
}

--  local client = create_client_instance('89.209.65.167', 7700, tls_opts)
local client = create_client_instance('127.0.0.1', 7700, tls_opts)
client:connect()

local function do_command(cmd)
  local resp, req = client:process_cmd(cmd)
  print(">>>>")
  print(req)
  print("<<<<")
  print(resp)
end

---[[
do_command { query_tpl.login,
  clID = 'inua.started',
--  clID = 'inua.drs',
  pw = 'quaint00',
--  newPW = 'new00',
} --]]
--[[
do_command { query_tpl.host_info,
  host__name = 'ded.in.ua',
  host__name = 'ns1.sitebox.in.ua',
} --]]
--[[
do_command { query_tpl.host_check,
  host__name = { 'ded.in.ua', 'ns1.sitebox.in.ua', '-heheheh.in.ua' }
} --]]
--[[
do_command { query_tpl.host_create,
  host__name = 'ns6.ded.in.ua',
  host__ip = {'1.1.1.1' '2.2.2.2', '01::01', '126.0.0.111'},
} --]]
--[[
do_command { query_tpl.host_update,
  host__name = 'ns2.ded3.in.ua',
  addrs_add = { '4.4.4.4', '192.192.1.1' },
  addrs_rem = { '1::23:42:1:1' },
  status_add = { 'clientUpdateProhibited' },
  status_rem = { 'clientDeleteProhibited' },
--  status_rem = { 'clientUpdateProhibited' },
}
--]]
--[[
do_command { query_tpl.host_delete,
  host__name = 'ns2.utel4.in.ua',
} --]]

--[[
do_command { query_tpl.contact_check,
  contact__id = { 'dedushka', 'babushka' },
H
} --]]
--[[
do_command { query_tpl.contact_info,
  contact__id = 'dedushka',
--  contact__authInfo = 'OLOLOL',
}
--]]
---[[
do_command { query_tpl.contact_create,
  contact__id = 'mamoogun',
--  contact__id = 'babushka',
  contact__voice = '+044.4444444',
  contact__fax = '+044.4444444',
  contact__email = 'dedoogun@gmail.com',
  contact__authInfo = 'OLOLOL',
  disclose = '0:name=int,addr=loc,name=loc,fax',
  postalinfo_int = {
    contact__name = 'Dedooshka Mooroz',
    contact__org = 'North Pole Ltd',
    contact__street = 'Polarnaya str.',
    contact__city = 'Polus',
    contact__sp = 'North Circle',
    contact__pc = '00000',
    contact__cc = 'NP',
  },
  postalinfo_loc = {
    contact__name = 'Dedooshka Mooroz L',
    contact__org = 'North Pole Ltd L',
    contact__street = 'Polarnaya str. L',
    contact__city = 'Polus L',
    contact__sp = 'North Circle L',
    contact__pc = '00000',
    contact__cc = 'NP',
  },
} --]]
---[[
do_command { query_tpl.contact_update,
  contact__id = 'mamoogun',
  contact__voice = '+044.4444444',
  contact__fax = '+044.4444444',
  contact__email = 'dedoogun@gmail.com',
  contact__authInfo = 'OLOLOL',
  disclose = '0:name=int,addr=loc,name=loc,fax',
  postalinfo_int = {
    contact__name = 'Dedooshka Mooroz',
    contact__org = 'North Pole Ltd',
    contact__street = 'Polarnaya str.',
    contact__city = 'Polus',
    contact__sp = 'North Circle',
    contact__pc = '00000',
    contact__cc = 'NP',
  },
  postalinfo_loc = {
    contact__name = 'Dedooshka Mooroz L',
    contact__org = 'North Pole Ltd L',
    contact__street = 'Polarnaya str. L',
    contact__city = 'Polus L',
    contact__sp = 'North Circle L',
    contact__pc = '00000',
    contact__cc = 'NP',
  },
  add = { 'clientTransferProhibited', 'clientUpdateProhibited' },
  rem = { 'clientDeleteProhibited' },
} --]]
--[[
do_command { query_tpl.contact_delete,
  contact__id = 'mamoogun'
} --]]


--[[
do_command { query_tpl.domain_info,
  domain__name = 'ded.in.ua',
  domain__authInfo = 'AUTH000',
} --]]
--[[
do_command { query_tpl.domain_check,
  domain__name = { 'ded.in.ua', 'ded2.in.ua', 'ded3.in.ua', 'freehust.in.ua' },
} --]]
--[[
do_command { query_tpl.domain_delete,
  domain__name = 'ded5.in.ua',
--  domain__name = 'ded3.in.ua',
} --]]
--[[
do_command { query_tpl.domain_create,
  domain__name = 'dedx.in.ua',
  domain__registrant = 'dedushka',
  domain__period = 2,
  domain__ns = {
    'ns1.imena.com.ua',
    'ns2.dedx.in.ua/11.10.10.1/1::23:42:1:1',
    'ns3.freedns.org/'
  },
  domain__contact = { 'admin=dedushka', 'tech=babushka' },
} --]]
--[[
do_command { query_tpl.domain_update,
  domain__name = 'dedx.in.ua',
--  domain__registrant = 'babushka',
--  domain__authInfo = '00',
  domain__null = true, -- nullify authInfo
--  status_add = { 'clientHold', 'clientTransferProhibited' },
--  status_rem = { 'clientRenewProhibited' },
--  status_add = { 'clientRenewProhibited' },
--  ns_add = { 'ns.freedns.org', 'ns1.imena.com.ua', 'a.dedx.in.ua/3.3.3.3' },
--  ns_rem = { 'ns3.freedns.org', 'ns2.dedx.in.ua' },
--  contact_add = { 'admin=dedushka', 'tech=dedushka', 'admin=babushka' },
--  contact_rem = { 'tech=babushka' },
} --]]
--[[
do_command { query_tpl.domain_transfer,
--  transfer_op = 'query',
  transfer_op = 'request',
  domain__name = 'dveri.in.ua',
  domain__authInfo = 'AUTH000',
  domain__period = 1,
} --]]
--[[
do_command { query_tpl.domain_restore,
  domain__name = 'dedx.in.ua',
} --]]
--[[
do_command { query_tpl.domain_renew,
  domain__name = 'ded6.in.ua',
  domain__period = 1,
  domain__curExpDate = '2014-06-01',
} --]]

--[[
do_command { query_tpl.poll,
--  poll_op = 'req',
  poll_op = 'ack',
  msgID = 1,
}--]]

