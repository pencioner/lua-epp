local client = require'epp_client_al'
local tester = require'epp_tester'

local the_tls_opts = {
  mode = "client",
  protocol = "tlsv1",
  key = "keys/clientAkey.pem",
  certificate = "keys/clientA.pem",
  cafile = "keys/rootA.pem",
  verify = {"peer", "fail_if_no_peer_cert"},
  options = {"all", "no_sslv2"},
}

require'seed_data'

local _R = require'epp_response_codes'
local G_mt = {
  __index = _R,
}
setmetatable(_G, G_mt) -- little hack: proxy response codes namespace to global

local the_host, the_port = '127.0.0.1', 7701

for i=1,3 do
  local connection = client.create_client_instance(
    the_host, the_port, the_tls_opts
  )
  _G['client_connection_'..i] = connection
  _G['registrator_'..i] = tester.make_tester(connection)

  for j=1,3 do
    connection = client.create_client_instance(
      the_host, the_port, the_tls_opts
    )
    _G['dealer_connection_'..i..j] = connection
    _G['dealer_'..j..'_reg_'..i] = tester.make_tester(connection)
  end
end

-- ok let's go
registrator_1
  :do_login{ newPW = 'ntest1' }:check{ E_SYNTAX }
  :do_login{ pw = 'test2' }:check{ E_SYNTAX }
  :do_login{ clID = 'test1-mnt0' }:check{ E_NOT_EXISTS }
  :do_login{ clID = 'reg1' }:check{ E_AUTHENTICATION }
  :do_login{ pw = 'reg1pwd' }:check{ OK }
  :do_logout{}:check{ LOGGED_OUT }
  :do_logout{}:check{ E_USE }
  :do_login{ pw = 'reg1pwd' }:check{ E_AUTHENTICATION } --changed by newPW
  :do_login{ newPW = 'reg1pwd', pw = 'ntest1' }:check{ OK }:do_logout{}
  :do_login{ newPW=false, pw = 'reg1pwd' }:check{ OK }

registrator_2:do_login{ clID = 'reg2', pw = 'reg2pwd' }:check{ OK }
registrator_3:do_login{ clID = 'reg3', pw = 'reg3pwd' }:check{ OK }

registrator_1
  :do_contact_create{}:check{ E_PARAM_MISSING }
  :do_contact_create{ contact__email = "@email@example.com" }
    :check{ E_PARAM_VALUE }
  :do_contact_create{ contact__id = "registrant1*" }:check{ E_PARAM_VALUE }
  :do_contact_create{ contact__email = "registrant1@example.com" }
    :check{ E_PARAM_VALUE }
  :do_contact_create{ contact__id = "registrant1" }:check{ E_PARAM_MISSING }
  :do_contact_create{ postalinfo_int = {
                        contact__name = 'Registrant1' }
                    }:check{ E_PARAM_VALUE }
  :do_contact_create{ postalinfo_int = {
                        contact__name = 'Registrant1name',
                        contact__city = 'Registrant1city',
                        contact__cc   = 'UA' }
                    }:check{ OK }
  :do_contact_create{ postalinfo_loc = {
                        contact__name = 'имя',
                        contact__city = 'город',
                        contact__cc   = 'UA' }
                    }:check{ E_EXISTS }
  :do_contact_create{ contact__id = "admin1-1",
                      contact__email = "admin1@example.com" }:check{ OK }
  :do_contact_create{ contact__id = "admin1-2" }:check{ OK }
  :do_contact_create{ contact__id = "admin1-3" }:check{ OK }
  :do_contact_create{ contact__id = "admin1-4" }:check{ OK }
  :do_contact_create{ contact__id = "admin1-5" }:check{ OK }
  :do_contact_create{ contact__id = "admin1-6" }:check{ OK }
  :do_contact_create{ contact__id = "admin1-7",
                      disclose = '1:voice,org=int,addr=loc',
                      postalinfo_loc=false }
    :check{ OK }
  :do_contact_info{ contact__id = "admin1-7" }
    :check{ OK, 'voice', 'org', 'addr' }
  :do_contact_create{ contact__id = "admin1-8",
                      contact__email = "admin@admin.adm",
                      contact__voice = "+011.1111111",
                      contact__fax = "+022.2222222" }:check{ OK }
  :do_contact_create{ contact__id = "admin1-9" }:check{ OK }
  :do_contact_create{ contact__id = "tech1-1",
                      contact__email = "tech1@example.com",
                      postalinfo_int = {
                        contact__name   = 'contact name',
                        contact__city   = 'contact city',
                        contact__street = 'contact street',
                        contact__sp     = 'SP region',
                        contact__pc     = '31337',
                        contact__cc     = 'UA' }
                    }:check{ OK }
  :do_contact_create{ contact__id = "tech1-2" }:check{ OK }
  :do_contact_create{ contact__id = "tech1-3" }:check{ OK }
  :do_contact_create{ contact__id = "tech1-4" }:check{ OK }
  :do_contact_create{ contact__id = "tech1-5" }:check{ OK }
  :do_contact_create{ contact__id = "tech1-6" }:check{ OK }
  :do_contact_create{ contact__id = "tech1-7" }:check{ OK }
  :do_contact_create{ contact__id = "tech1-8" }:check{ OK }
  :do_contact_create{ contact__id = "tech1-9" }:check{ OK }

registrator_1
  :do_domain_create{}:check{ E_SYNTAX }
  :do_domain_create{ domain__name = 'test-.in.ua' }:check{ E_PARAM_VALUE }
  :do_domain_create{ domain__name = 'test-1-1.in.ua',
                     domain__registrant = 'registrant1',
                     domain__contact = { 'admin=registrant',
                                         'tech=registrant' }
                   }:check{ E_NOT_EXISTS }
  :do_domain_create{ domain__contact = { 'admin=admin1-1',
                                         'tech=tech1-1', 'admin=admin1-1' }
                   }:check{ E_SYNTAX }
  :do_domain_create{ domain__contact = { 'admin=admin1-1',
                                         'tech=tech1-1' }
                   }:check{ OK }
  :do_domain_create{ }:check{ E_EXISTS }
  :do_domain_create{ domain__contact = {
                       'admin=admin1-1','admin=admin1-2','admin=admin1-3',
                       'admin=admin1-4','admin=admin1-5','admin=admin1-6',
                       'admin=admin1-7','admin=admin1-8','admin=admin1-9',
                       'tech=tech1-1', 'tech=tech1-2', 'tech=tech1-3',
                       'tech=tech1-4', 'tech=tech1-5', 'tech=tech1-6',
                       'tech=tech1-7', 'tech=tech1-8', 'tech=tech1-9' },
                     domain__name = 'test-1-2.in.ua'
                   }:check{ E_SYNTAX }
  :do_domain_create{ domain__contact = {
                       'admin=admin1-1','admin=admin1-2','admin=admin1-3',
                       'admin=admin1-4','admin=admin1-5','admin=admin1-6',
                       'admin=admin1-7',
                       'tech=tech1-1', 'tech=tech1-2', 'tech=tech1-3',
                       'tech=tech1-4', 'tech=tech1-5', 'tech=tech1-6',
                       'tech=tech1-7' }
                   }:check{ OK }
  :do_domain_create{ domain__name = 'test-1-3.in.ua',
                     domain__period = 3,
                   }:check{ E_PARAM_RANGE }
  :do_domain_create{ domain__period = 2 }:check{ OK }
  :do_domain_create{ domain__name = 'test-1-4.in.ua',
                     domain__ns = { 'ns1.example.com/' },
                   }:check{ OK }
  :do_domain_create{ domain__name = 'test-1-5.in.ua',
                     domain__ns = { 'ns1.example.com/', 'ns1.example.com/',
 'ns1.example.com/','ns1.example.com/','ns1.example.com/','ns1.example.com/',
 'ns1.example.com/','ns1.example.com/','ns1.example.com/','ns1.example.com/',
 'ns1.example.com/','ns1.example.com/','ns1.example.com/','ns1.example.com/' }
                   }:check{ E_SYNTAX }
  :do_domain_create{ domain__name = 'test-1-5.in.ua',
                     domain__ns = { 'ns1.example.com/' }
                   }:check{ E_EXISTS }
  :do_domain_create{ domain__name = 'test-1-5.in.ua',
                     domain__ns = { 'ns1.example.com', 'ns1.example.com' }
                    }:check{ E_SYNTAX }
  :do_domain_create{ domain__name = 'test-1-5.in.ua',
                     domain__ns = { 'ns1.example.com',
 'ns2.example.com','ns7.example.com','ns8.example.com','ns13.example.com',
 'ns3.example.com','ns6.example.com','ns9.example.com','ns12.example.com',
 'ns4.example.com','ns5.example.com','ns10.example.com','ns11.example.com' }
                   }:check{ E_NOT_EXISTS }
  :do_domain_create{ domain__name = 'test-1-5.in.ua',
                     domain__ns = { 'ns1.example.com',
 'ns2.example.com/','ns7.example.com/','ns8.example.com/','ns13.example.com/',
 'ns3.example.com/','ns6.example.com/','ns9.example.com/','ns12.example.com/',
 'ns4.example.com/','ns5.example.com/','ns10.example.com/','ns11.example.com/' 
                                  }
                   }:check{ OK }
  :do_domain_create{ domain__name = 'test-1-6.in.ua',
                     domain__ns = { 'ns1.test-1-6.in.ua/' }
                   }:check{ E_PARAM_MISSING }
  :do_domain_create{ domain__name = 'test-1-6.in.ua',
                     domain__ns = { 'ns1.test-1-6.in.ua/1.1.1.1',
 'ns2.test-1-6.in.ua/::234', 'ns7.test-1-6.in.ua/2.2.2.2',
 'ns8.test-1-6.in.ua/3.3.3.3', 'ns13.test-1-6.in.ua/4.4.4.4',
 'ns3.test-1-6.in.ua/5.5.5.5', 'ns6.test-1-6.in.ua/6.6.6.6',
 'ns9.test-1-6.in.ua/7.7.7.7', 'ns12.test-1-6.in.ua/8.8.8.8',
 'ns4.test-1-6.in.ua/9.9.9.9', 'ns5.test-1-6.in.ua/ff::ff',
 'ns10.test-1-6.in.ua/a.a.a.a','ns11.test-1-6.in.ua/127.0.0.1' }
                   }:check{ E_PARAM_RANGE }
  :do_domain_create{ domain__name = 'test-1-6.in.ua',
                     domain__ns = { 'ns1.test-1-6.in.ua/1.1.1.1',
 'ns2.test-1-6.in.ua/::234:11:11:11', 'ns7.test-1-6.in.ua/2.2.2.2',
 'ns8.test-1-6.in.ua/3.3.3.3', 'ns13.test-1-6.in.ua/4.4.4.4',
 'ns3.test-1-6.in.ua/5.5.5.5', 'ns6.test-1-6.in.ua/6.6.6.6',
 'ns9.test-1-6.in.ua/7.7.7.7', 'ns12.test-1-6.in.ua/8.8.8.8',
 'ns4.test-1-6.in.ua/9.9.9.9', 'ns5.test-1-6.in.ua/ff::ff',
 'ns10.test-1-6.in.ua/a.a.a.a','ns11.test-1-6.in.ua/12.7.0.1' }
                   }:check{ E_PARAM_VALUE }
  :do_domain_create{ domain__name = 'test-1-6.in.ua',
                     domain__ns = { 'ns1.test-1-6.in.ua/1.1.1.1',
 'ns2.test-1-6.in.ua/::234:11:11:11', 'ns7.test-1-6.in.ua/2.2.2.2',
 'ns8.test-1-6.in.ua/3.3.3.3', 'ns13.test-1-6.in.ua/4.4.4.4',
 'ns3.test-1-6.in.ua/5.5.5.5', 'ns6.test-1-6.in.ua/6.6.6.6',
 'ns9.test-1-6.in.ua/7.7.7.7', 'ns12.test-1-6.in.ua/8.8.8.8',
 'ns4.test-1-6.in.ua/9.9.9.9', 'ns5.test-1-6.in.ua/ff::ff',
 'ns10.test-1-6.in.ua/a::a:a:a','ns11.test-1-6.in.ua/147.0.0.1' }
                   }:check{ OK }
  :do_domain_create{ domain__name = 'test-1-7.in.ua',
                     domain__ns = {'ns10.example.com', 'ns11.example.com'},
                   }:check{ OK }

  :do_domain_update { domain__name = 'test-1-6.in.ua',
                      domain__authInfo = '32729955' }:check{ OK }
  :do_domain_info { domain__name = 'test-1-6.in.ua' }
    :check{ OK, domain__name = 'test-1-6.in.ua', 'ok', -- 'linked', FIXME
            domain__roid = true, domain__status = true,
            domain__registrant = 'registrant1',
            domain__contact = true, domain__ns = true,
            domain__host = true, 'ns1.test-1-6.in.ua', 'ns5.test-1-6.in.ua',
            domain__clID = 'reg1',
            domain__crID = 'reg1', domain__crDate = true, os.date'%F',
            domain__upID = 'reg1', domain__upDate = true,
            domain__exDate = true,
          }
    :check{ domain__authInfo = true }
    :check{ domain__pw = '32729955' }

registrator_2
  :do_domain_info { domain__name = 'test-1-6.in.ua' }
    :check{ domain__infData = true }
    :check{ OK, domain__name = 'test-1-6.in.ua', 'ok', -- 'linked', FIXME
            domain__roid = true, domain__status = true,
            domain__registrant = 'registrant1',
            domain__contact = true, domain__ns = true,
            domain__host = false,
            domain__clID = 'reg1',
            domain__crID = 'reg1', domain__crDate = true, os.date'%F',
            domain__upID = false, domain__upDate = false,
            domain__exDate = true,
            domain__authInfo = false,
          }
  :do_domain_info { domain__authInfo = '32729955' }
    :check{ OK,
            domain__upID = 'reg1', domain__upDate = true,
            domain__host = true, domain__authInfo = true
          }

registrator_1
  :do_host_create{ host__name = '%.test-1-7.in.ua' }
    :check{ E_PARAM_VALUE }
  :do_host_create{ host__name = 'ns1.test-1-7.in.ua' }:check{ E_PARAM_MISSING }
  :do_host_create{ host__ip = { '&&&&' } }:check{ E_PARAM_VALUE }
  :do_host_create{ host__ip = { '::01', '127.0.0.1' } }:check{ E_PARAM_RANGE }
  :do_host_create{ host__ip = { '66::66:1:2', '66::66:1:2' } }:check{ E_SYNTAX }
  :do_host_create{ host__ip = { '1.1.1.1', '2.2.2.2', '3.3.3.3', '4.4.4.4',
                                '5.5.5.5', '6.6.6.6', '7.7.7.7', '8.8.8.8',
                                '9.9.9.9', '12.1.1.1', '11.1.1.1', '1.2.3.4',
                                '25.25.23.23', '123.234.56.78' }
                 }:check{ E_SYNTAX }
  :do_host_create{ host__ip = { '01::01', '126.0.0.111'} }:check{ OK }
  :do_host_create{ host__name = 'ns2.test-1-7.in.ua' }:check{ OK }
  :do_host_create{ host__name = 'ns14.example.com' }:check{ E_SYNTAX }

registrator_2
  :do_contact_create{ contact__id = "registrant2",
                      postalinfo_int = {
                      contact__name = 'NAME',
                      contact__city = 'CITY',
                      contact__cc   = 'UA' },
                      contact__email = 'abracadabra@hottab.com',
                    }:check{ OK }
  :do_contact_create{ contact__id = "admin2-1" }:check{ OK }
  :do_contact_create{ contact__id = "admin2-2" }:check{ OK }
  :do_contact_create{ contact__id = "tech2-1" }:check{ OK }
  :do_contact_create{ contact__id = "tech2-2" }:check{ OK }


registrator_2
  :do_domain_create{ domain__name = 'test-2-1.in.ua',
                     domain__registrant = 'registrant1',
                     domain__contact = { 'admin=admin2-1', 'tech=tech2-1' }
                   }:check{ OK }
  :do_domain_create{ domain__name = 'test-2-2.in.ua',
                     domain__registrant = 'registrant2',
                     domain__contact = { 'admin=admin2-2',
                                         'tech=tech2-2' }
                   }:check{ OK }
  :do_domain_create{ domain__name = 'test-2-3.in.ua',
                     domain__ns = {'ns1.test-2-3.in.ua/12::12:12:12'} }
    :check{ OK, domain__name = { 'test-2-3.in.ua' } }

registrator_2
  :do_domain_update{ domain__name = 'test-.in.ua' }:check{ E_PARAM_VALUE }
  :do_domain_update{ domain__name = 'test-2-3.in.ua' }:check{ E_SYNTAX }
  :do_domain_update{ ns_rem = { '0.0.0.0' } }:check{ E_NOT_EXISTS }
  :do_domain_update{ ns_rem = { 'ns1.test-1-7.in.ua', 'ns1.test-1-7.in.ua' } }
    :check{ OK }
  :do_domain_update{ ns_rem = { 'ns1.test-1-7.in.ua' } }:check{ OK }
  :do_domain_update{ domain__authInfo = 'AUTH000',
                     status_add = { 'clientTransferProhibited', 
                                    'clientUpdateProhibited' }
                   }:check{ OK }


registrator_3
  :do_domain_transfer{ transfer_op = 'request',
                       domain__name = 'test-2-#.in.ua',
                       domain__authInfo = 'AUTH000',
                       domain__period = 1,
                     }:check{ E_PARAM_VALUE }
  :do_domain_transfer{ domain__name = 'test-2-3.in.ua' }
    :check{ E_STATUS_PROHIBITE }

registrator_2
  :do_poll{ poll_op = req }:check{ EMPTY_Q }
  :do_domain_update{ status_rem = { 'clientTransferProhibited' } }
    :check{ E_STATUS_PROHIBITE }
  :do_domain_update{ status_rem = { 'clientTransferProhibited' },
                     status_add=false,ns_rem=false,domain__authInfo=false }
    :check{ E_STATUS_PROHIBITE }
  :do_domain_update{ status_rem = { 'clientUpdateProhibited' } }:check{ OK }
  :do_domain_update{ status_rem = { 'clientTransferProhibited', 'ok' } }
    :check{ E_PARAM_VALUE }
  :do_domain_update{ status_rem = { 'clientTransferProhibited' } }:check{ OK }

registrator_3
  :do_domain_transfer{ }:check{ PENDING, domain__trStatus = 'pending' }
  :do_domain_transfer{ }:check{ E_PENDING }
  :do_domain_info{ domain__name = 'test-2-3.in.ua' }
    :check{ OK, 'pendingTransfer' }

local current_msg
registrator_2
  :do_poll{ poll_op = 'req' }
    :check{ msgQ = function(tag)
              current_msg = tag.id
              assert('1' == tag.count, "msg count should be 1")
              return true
            end
          }
  :do_poll{ poll_op = 'ack' }:check{ E_PARAM_RANGE }
  :do_poll{ poll_op = 'ack', msgID = current_msg }:check{ EMPTY_Q }
  :do_poll{ poll_op = 'req' }:check{ EMPTY_Q }
  :do_domain_transfer{ transfer_op = 'reject',
                       domain__name = 'test-2-3.in.ua'
                     }:check{ OK }

registrator_3
  :do_domain_transfer{ transfer_op = 'query' }
    :check{ domain__trStatus = 'clientRejected' }
  :do_domain_transfer{ transfer_op = 'request' }:check{ PENDING }
  :do_domain_transfer{ transfer_op = 'query' }
    :check{ domain__trStatus = 'pending' }

registrator_2
  :do_domain_info{ domain__name = 'test-2-3.in.ua', domain__authInfo=false }
    :check{ OK, 'pendingTransfer' }
  :do_domain_transfer{ transfer_op = 'query' }
    :check{ domain__trStatus = 'pending' }
  :do_domain_transfer{ transfer_op = 'cancel' }:check{ E_AUTHORIZATION }

registrator_1
  :do_domain_transfer{ transfer_op = 'query',
                       domain__name = 'test-2-3.in.ua'
                     }:check{ E_AUTHINFO }
  :do_domain_transfer{ transfer_op = 'query',
                       domain__name = 'test-1-5.in.ua'
                     }:check{ E_NOT_PENDING }

registrator_3
  :do_poll{ poll_op = 'req' }
    :check{ msgQ = function(tag)
              current_msg = tag.id
              assert('1' == tag.count, "msg count should be 1")
              return true
            end
          }
  :do_poll{ poll_op = 'ack' }:check{ E_PARAM_RANGE }
  :do_poll{ poll_op = 'ack', msgID = current_msg }:check{ EMPTY_Q }
  :do_poll{ poll_op = 'req' }:check{ EMPTY_Q }

  :do_domain_transfer{ transfer_op = 'cancel' }
    :check{ domain__trStatus = 'clientCancelled' }

registrator_1
  :do_domain_transfer{ transfer_op = 'request',
                       domain__name = 'test-2-3.in.ua',
                     }:check{ E_SYNTAX }
  :do_domain_transfer{ domain__authInfo = 'AUTH000' }:check{ PENDING }
  :do_domain_transfer{ transfer_op = 'approve' }:check{ E_AUTHORIZATION }
  :do_domain_transfer{ transfer_op = 'reject' }:check{ E_AUTHORIZATION }

registrator_2
  :do_domain_transfer{ transfer_op = 'approve' }
    :check{ domain__trStatus = 'clientApproved', domain__reID = 'reg1' }
  :do_domain_transfer{ transfer_op = 'reject' }:check{ E_AUTHORIZATION }

registrator_1
  :do_domain_transfer{ transfer_op = 'query' }:check{ E_AUTHINFO }
  :do_domain_transfer{ transfer_op = 'query', domain__authInfo=false }
    :check{ domain__trStatus = 'clientApproved' }

registrator_1
  :do_domain_update{ domain__name = 'test-1-1.in.ua',
                     domain__authInfo = 'testtest' }:check{ OK }
  :do_domain_update{ domain__name = 'test-1-2.in.ua' }:check{ OK }
  :do_domain_update{ domain__name = 'test-1-3.in.ua' }:check{ OK }

registrator_3
  :do_domain_transfer{ transfer_op = 'request',
                       domain__name = 'test-1-1.in.ua',
                       domain__authInfo = 'testtest' }:check{ PENDING }
  :do_domain_transfer{ domain__name = 'test-1-2.in.ua' }:check{ PENDING }
  :do_domain_transfer{ domain__name = 'test-1-3.in.ua' }:check{ PENDING }

registrator_1
  :do_domain_transfer{ domain__name = 'test-1-1.in.ua',
                       transfer_op = 'approve' }:check{ OK }
  :do_domain_transfer{ domain__name = 'test-1-2.in.ua' }:check{ OK }
  :do_domain_transfer{ domain__name = 'test-1-3.in.ua' }:check{ OK }

registrator_2
  :do_poll{ poll_op = 'req' }
    :check{ msgQ = function(tag)
              current_msg = tag.id
              assert('3' == tag.count, "msg count should be 3")
              return true
            end
          }
  :do_poll{ poll_op = 'ack', msgID = current_msg }:check{ }
  :do_poll{ poll_op = 'req' }
    :check{ msgQ = function(tag)
              current_msg = tag.id
              assert('2' == tag.count, "msg count should be 2")
              return true
            end
          }
  :do_poll{ poll_op = 'ack', msgID = current_msg }:check{ }
  :do_poll{ poll_op = 'req' }
    :check{ msgQ = function(tag)
              current_msg = tag.id
              assert('1' == tag.count, "msg count should be 1")
              return true
            end
          }
  :do_poll{ poll_op = 'ack', msgID = current_msg }:check{ EMPTY_Q }
  :do_poll{ poll_op = 'req' }:check{ EMPTY_Q }

registrator_1
  :do_host_update{ host__name = 'ns1.test-1-6.in.ua' }:check{ E_SYNTAX }
  :do_host_update{ status_add = { 'clientUpdateProhibited',
                                  'clientDeleteProhibited' } }:check{ OK }
  :do_host_update{ status_add = { 'clientDeleteProhibited' } }
    :check{ E_STATUS_PROHIBITE }
  :do_host_update{ status_rem = { 'clientUpdateProhibited',
                                  'clientDeleteProhibited' },
                   status_add=false }:check{ E_STATUS_PROHIBITE }
  :do_host_update{ status_rem = { 'clientUpdateProhibited'} }:check{ OK }
  :do_host_update{ status_rem = { 'clientDeleteProhibited' },
                   addrs_rem = { '192.168.1.1' } }:check{ E_PARAM_RANGE }
  :do_host_update{ addrs_add = { '1.1.1.1', '1.1.1.1' },addrs_rem=false }
    :check{ E_SYNTAX }
  :do_host_update{ addrs_add = { '2.2.2.2' }, addrs_rem = { '2.2.2.2' } }
    :check{ E_SYNTAX }
  :do_host_update{ addrs_add=false }:check{ E_PARAM_POLICY }
  :do_host_update{ addrs_rem=false }:check{ OK }
  :do_host_update{ addrs_add = { '1.1.1.1' } }:check{ E_PARAM_POLICY }
  :do_host_update{ host__name = 'ns13.test-1-6.in.ua',
                   status_rem=false, addrs_add=false,
                   status_add = { 'clientDeleteProhibited' } }:check{ OK }
  :do_host_delete{ host__name = 'ns13.test-1-6.in.ua' }
    :check{ E_STATUS_PROHIBITE }
  :do_host_update{ host__name = 'ns1.test-1-6.in.ua', status_add=false,
                   addrs_add = { '1.1.1.1',
                     '2.2.2.1', '2.2.2.2', '2.2.1.2', '2.1.2.2', '1.2.2.2',
                     '2.1.2.1', '2.2.2.3', '2.2.3.2', '2.1.1.2', '1.2.1.2',
                     '1.1.2.1', '1.1.2.2', '1.1.1.2', '2.1.1.1', '1.2.1.1',
                   }
                 }:check{ E_SYNTAX }
  :do_host_update{ addrs_add = {
                     '2.1.2.2', '1.2.2.2',
                     '2.1.2.1', '2.2.2.3', '2.2.3.2', '2.1.1.2', '1.2.1.2',
                     '1.1.2.1', '1.1.2.2', '1.1.1.2', '2.1.1.1', '1.2.1.1',
                   }
                 }:check{ OK }
  :do_host_update{ host__name = 'ns2.test-1-6.in.ua' }:check{ OK }
  :do_host_info{ }:check{ E_SYNTAX }
  :do_host_info{ host__name = 'ns13.test-1-6.in.ua' }
    :check{ OK, 'clientDeleteProhibited' }
  :do_host_info{ host__name = 'ns1.test-1-6.in.ua' }
    :check{ OK, '2.1.2.2', '1.2.2.2', '2.1.2.1', '2.2.2.3', '2.2.3.2',
                '2.1.1.2', '1.2.1.2', '1.1.2.1', '1.1.2.2', '1.1.1.2',
                '2.1.1.1', '1.2.1.1', '1.1.1.1' }
  :do_domain_update{ domain__name = 'test-1-7.in.ua',
                     ns_add = { 'ns1.test-1-6.in.ua', 'ns13.test-1-6.in.ua' }
                   }:check{ OK }
  :do_domain_update{ domain__name = 'test-1-5.in.ua' }:check{ OK }

registrator_2
  :do_host_info{ host__name = 'ns1.test-1-6.in.ua' }
    :check{ OK, '2.1.2.2', '1.2.2.2', '2.1.2.1', '2.2.2.3', '2.2.3.2',
                '2.1.1.2', '1.2.1.2', '1.1.2.1', '1.1.2.2', '1.1.1.2',
                '2.1.1.1', '1.2.1.1', '1.1.1.1' }
  :do_domain_update{ domain__name = 'test-2-1.in.ua',
                     ns_add = { 'ns1.test-1-6.in.ua', 'ns13.test-1-6.in.ua' }
                   }:check{ OK }
  :do_domain_update{ domain__name = 'test-2-2.in.ua' }:check{ OK }
  :do_poll{ poll_op = 'req' }:check{ EMPTY_Q }

registrator_3
  :do_domain_update{ domain__name = 'test-1-1.in.ua',
                     ns_add = { 'ns1.test-1-6.in.ua', 'ns13.test-1-6.in.ua' }
                   }:check{ OK }
  :do_domain_update{ domain__name = 'test-1-2.in.ua' }:check{ OK }

registrator_1
  :do_host_delete{ host__name = 'ns13.test-1-6.in.ua' }
    :check{ E_STATUS_PROHIBITE }
  :do_host_update{ host__name = 'ns13.test-1-6.in.ua', addrs_add=false,
                   status_rem = { 'clientDeleteProhibited' } }:check{ OK }
  :do_host_update{ host__name = 'ns1.test-1-6.in.ua' }:check{ OK }
  :do_host_delete{ host__name = 'ns13.test-1-6.in.ua' }
    :check{ E_ASSOC_PROHIBITE }
  :do_host_delete{ force = '1' }:check{ OK }
  :do_domain_delete{ domain__name = 'test-1-5.in.ua' }:check{ OK }


  :do_host_delete{ host__name = 'ns1.test-1-7.in.ua', force=false }:check{ OK }
  :do_domain_delete{ domain__name = 'test-1-7.in.ua' }
    :check{ E_ASSOC_PROHIBITE }
  :do_domain_delete{ force = '1' }:check{ OK }
  :do_host_delete{ host__name = 'ns2.test-1-7.in.ua' }:check{ E_NOT_EXISTS }

  :do_host_update{ host__name = 'ns1.test-1-6.in.ua',
                   addrs_add=false, status_rem=false,
                   status_add = { 'clientDeleteProhibited' } }:check{ OK }

  :do_domain_delete{ domain__name = 'test-1-6.in.ua', force = '1' }
    :check{ E_ASSOC_PROHIBITE }
  :do_host_update{ host__name = 'ns1.test-1-6.in.ua', status_add=false,
                   status_rem = { 'clientDeleteProhibited' } }:check{ OK }
  :do_domain_delete{ force='0' }:check{ E_ASSOC_PROHIBITE }
  :do_domain_delete{ force='1' }:check{ OK }

registrator_2 -- after force delete
  :do_poll{ poll_op = 'req' }:check{ OK, 'lost object association',
                                     "test-2-2.in.ua", "ns13.test-1-6.in.ua"
                                   }

dealer_1_reg_1
  :do_login{ clID = 'deal1.reg1', pw = 'deal1pwd' }
  :do_domain_create{ domain__name = 'dtest-1-1.in.ua',
                     domain__contact = {
                       'admin=admin1-1','admin=admin1-2','admin=admin1-3',
                       'admin=admin1-4','admin=admin1-5','admin=admin1-6',
                       'admin=admin1-7',
                       'tech=tech1-1', 'tech=tech1-2', 'tech=tech1-3',
                       'tech=tech1-4', 'tech=tech1-5', 'tech=tech1-6',
                       'tech=tech1-7' }
                   }:check{ E_SYNTAX }
  :do_domain_create{ domain__registrant = 'admin1-1' }:check{ OK }
  :do_domain_update{ domain__name = 'dtest-1-1.in.ua',
                     domain__authInfo = '111' }:check{ OK }

dealer_2_reg_2
  :do_login{ clID = 'deal2.reg2', pw = 'deal2pwd' }
  :do_domain_create{ domain__name = 'dtest-2-2.in.ua',
                     domain__contact = {
                       'admin=admin1-1','admin=admin1-2','admin=admin1-3',
                       'admin=admin1-4','admin=admin1-5','admin=admin1-6',
                       'admin=admin1-7',
                       'tech=tech1-1', 'tech=tech1-2', 'tech=tech1-3',
                       'tech=tech1-4', 'tech=tech1-5', 'tech=tech1-6',
                       'tech=tech1-7' },
                     domain__registrant = 'admin1-2',
                     domain__ns = { 'ns1.dtest-2-2.in.ua/2:2:2:2:2:2:2:2' }
                   }:check{ OK }
  :do_domain_update{ domain__name = 'dtest-2-2.in.ua',
                     domain__authInfo = '222' }:check{ OK }
  :do_domain_info{ domain__name = 'dtest-2-2.in.ua' }
    :check{ domain__status={ s='ok' } }

dealer_3_reg_3
  :do_login{ clID = 'deal3.reg3', pw = 'deal3pwd' }
  :do_domain_create{ domain__name = 'dtest-3-3.in.ua',
                     domain__contact = {
                       'admin=admin1-1','admin=admin1-2','admin=admin1-3',
                       'admin=admin1-4','admin=admin1-5','admin=admin1-6',
                       'admin=admin1-7',
                       'tech=tech1-1', 'tech=tech1-2', 'tech=tech1-3',
                       'tech=tech1-4', 'tech=tech1-5', 'tech=tech1-6',
                       'tech=tech1-7' },
                     domain__registrant = 'admin1-3'
                   }:check{ OK }
  :do_domain_update{ domain__name = 'dtest-3-3.in.ua',
                     domain__authInfo = '333' }:check{ OK }


dealer_2_reg_1
  :do_login{ clID = 'deal2.reg1', pw = 'deal2pwd' }:check{ OK }
  :do_domain_transfer{ transfer_op = 'request',
                       domain__name = 'dtest-1-1.in.ua',
                       domain__authInfo = '111' }:check{ PENDING }
  :do_domain_transfer{ domain__name = 'dtest-2-2.in.ua',
                       domain__authInfo = '222' }:check{ PENDING }
  :do_domain_info{ domain__name = 'dtest-1-1.in.ua' }
    :check{ OK, 'pendingTransfer' }
  :do_domain_info{ domain__name = 'dtest-2-2.in.ua' }
    :check{ OK, 'pendingTransfer', domain__host=false, domain__authInfo=false }
  :do_domain_info{ domain__authInfo = '222' }:check{ domain__host=true }

dealer_2_reg_2
  :do_domain_info{ domain__name = 'dtest-2-2.in.ua' }
    :check{ OK, domain__status={ s='pendingTransfer' },
            domain__name = 'dtest-2-2.in.ua',
            domain__host='ns1.dtest-2-2.in.ua',
            domain__registrant = 'admin1-2',
            domain__contact = true,
            domain__ns = true,
            domain__clID = 'reg2',
            domain__dlID = 'deal2.reg2',
            domain__crID = 'deal2.reg2',
            domain__crDate = function() return true end, -- TODO check date
            domain__exDate = function() return true end, -- TODO check date
            domain__authInfo = true,
          }

registrator_1
  :do_domain_transfer{ transfer_op = 'approve',
                       domain__name = 'dtest-1-1.in.ua'
                     }:check{ E_AUTHORIZATION }

registrator_2
  :do_domain_transfer{ transfer_op = 'approve',
                       domain__name = 'dtest-2-2.in.ua'
                     }:check{ E_AUTHORIZATION }

dealer_1_reg_1
  :do_domain_transfer{ transfer_op = 'approve',
                       domain__name = 'dtest-1-1.in.ua'
                     }:check{ OK }

dealer_2_reg_2
  :do_domain_transfer{ transfer_op = 'reject',
                       domain__name = 'dtest-2-2.in.ua'
                     }:check{ OK }
  :do_domain_transfer{ }:check{ E_TRANSF_NOT_ELIGIBLE }
  :do_domain_transfer{ transfer_op = 'request' }:check{ E_TRANSF_NOT_ELIGIBLE }

registrator_1
  :do_domain_transfer{ transfer_op = 'request', 
                       domain__name = 'dtest-1-1.in.ua', 
                       __domain__authInfo = '111' }:check{ OK }

dealer_2_reg_1
  :do_domain_transfer{ domain__name = 'dtest-1-1.in.ua',
                       transfer_op = 'query', domain__authInfo=false }
    :check{ OK, 'serverApproved' }

dealer_3_reg_1
  :do_login{ clID = 'deal3.reg1', pw = 'deal3pwd' }:check{ OK }
  :do_domain_transfer{ transfer_op = 'request',
                       domain__name = 'dtest-2-2.in.ua',
                       domain__authInfo = '222' }:check{ PENDING }

dealer_2_reg_2
  :do_domain_transfer{ transfer_op = 'approve',
                       domain__name = 'dtest-2-2.in.ua'
                     }:check{ OK }

do 
  local DB = require 'epp_db_management'
  DB.tables.epp_domain:where{d_name='test-1-4.in.ua'}
                      :update{d_statusi='{redemptionPeriod}'}
  DB.db_connection:commit()
end

registrator_1
  :do_domain_restore{ domain__name = 'test-1-4.in.ua' }:check{}
  :do_domain_restore{ domain__name = 'test-1-4.in.ua' }
    :check{ E_STATUS_PROHIBITE }

  :do_domain_info{ domain__name = 'test-1-4.in.ua' }
    :check{ OK, '!redemptionPeriod' }

  :do_domain_check{ domain__name = { 'test-1-4.in.ua' } }
    :check{ OK, domain__name = { 'test-1-4.in.ua', avail='0' },
            domain__chkData = true, domain__cd = true }
  :do_domain_check{ domain__name = { 'ttest-1-4.in.ua' } }
    :check{ OK, domain__name = { 'ttest-1-4.in.ua', avail='1' },
            domain__chkData = true, domain__cd = true }
  :do_domain_check{ domain__name = { 'aaaa' } }
    :check{ OK, domain__reason = function(t)
                  return t[2]:match'not in service', 'have incorrect reason'
                end }
  :do_domain_check{ domain__name = { 'test-1-1.in.ua',
                                     'test-1-2.in.ua',
                                     'test-1-3.in.ua' } }
    :check{ OK, '!avail="1"',
            'test-1-1.in.ua', 'test-1-2.in.ua', 'test-1-3.in.ua' }
  :do_domain_check{ domain__name = { 'test-1-1.in.ua', '1.in.ua',
     '2.in.ua', '5.in.ua', '6.in.ua', '7.in.ua', '8.in.ua',
     '3.in.ua', '4.in.ua', '9.in.ua', 'a.in.ua'} }:check{ E_SYNTAX }

  :do_host_check{ host__name = { 'ns1.dtest-2-2.in.ua' }}
    :check{ OK, host__name = { 'ns1.dtest-2-2.in.ua', avail='0' }}
  :do_host_check{ host__name = { 'ns2.dtest-2-2.in.ua' }}
    :check{ OK, host__name = { 'ns2.dtest-2-2.in.ua', avail='0' },
            host__reason = function(t)
              return t[2]:match'Not owner of parent domain',
                     'have incorrect reason'
            end }

dealer_2_reg_2
  :do_host_check{ host__name = { 'ns2.dtest-2-2.in.ua' }}
    :check{ OK, host__name = { 'ns2.dtest-2-2.in.ua', avail='0' }}

dealer_3_reg_1
  :do_host_check{ host__name = { 'ns2.dtest-2-2.in.ua' }}
    :check{ OK, host__name = { 'ns2.dtest-2-2.in.ua', avail='1' }}
  :do_host_check{ host__name = { 'ahahaha.lololol.org', 'aaaa' }}
    :check{ OK, '!avail="1"', 'ahahaha.lololol.org', 'aaaa',
            host__reason = 'Cannot resolve host domain name' }
  :do_host_check{ host__name = { '1', '2', '3', '4', '5', '9', '6', '7',
                                 '8', '9', '0' } }:check{ E_SYNTAX }

registrator_3
  :do_contact_check{ contact__id = { 'abc123'} }
    :check{ OK, contact__id = { 'abc123', avail='1' }}
  :do_contact_check{ contact__id = { 'admin1-1'} }
    :check{ OK, contact__id = { 'admin1-1', avail='0' }}
  :do_contact_check{ contact__id = { '#%$^&#$&'} }
    :check{ OK, contact__id = { avail='0' }}
  :do_contact_check{ contact__id = {
    '111','222','333','444','555','666','777','888','999','000','aaa'}}
    :check{ E_SYNTAX }

  :do_contact_delete{ contact__id = 'admin1-1' }:check{ E_AUTHORIZATION }

registrator_1
  :do_contact_delete{ contact__id = 'admin1-1' }:check{ E_ASSOC_PROHIBITE }
  :do_contact_delete{ contact__id = 'admin1-1' }:check{ E_ASSOC_PROHIBITE }
  :do_contact_delete{ contact__id = 'admin1-9' }:check{ OK }
  :do_contact_update{ contact__id = 'admin1-8' }:check{ E_SYNTAX }
  :do_contact_update{ contact__email = "odmen@ekzample.com",
                      postalinfo_int = {
                        contact__org    = 'dermozona',
                        contact__name   = 'odmen',
                        contact__city   = 'seetee',
                        contact__street = 'streat',
                        contact__sp     = 'SP',
                        contact__pc     = '31337',
                        contact__cc     = 'ZW' },
                      status_add = { 'clientUpdateProhibited' },
                      disclose = '1:name=int,org=int,addr=int,fax,email,voice',
                    }:check{ OK }
  :do_contact_update{}:check{ E_STATUS_PROHIBITE }
  :do_contact_update{ postalinfo_int=false, contact__email=false,
                      status_add=false, disclose=false,
                      status_rem = { 'clientUpdateProhibited' } }:check{ OK }
  :do_contact_update{ status_rem=false,
                      status_add = { 'clientDeleteProhibited' } }:check{ OK }
  :do_contact_delete{ }:check{ E_NOT_EXISTS }
  :do_contact_delete{ contact__id = 'admin1-8' }:check{ E_STATUS_PROHIBITE }
  :do_contact_info{ contact__id = 'admin1-8' }
    :check{ OK, --'clientDeleteProhibited',
            contact__infData = function(tag)
              for _tag,_val in pairs{
                    contact__org   = 'dermozona',
                    contact__voice = "+011.1111111",
                    contact__fax   = "+022.2222222",
                    contact__email = "odmen@ekzample.com",
                               } do
                local subtag = xml.find(tag, _tag)
                if not subtag or _val ~= subtag[2] then
                  return nil, 'missing or incorrect '.._tag:gsub('__',':')
                end
              end
              return true
            end,
            contact__disclose = function(tag)
              for _tag,_type in pairs{
                    contact__voice = '',
                    contact__fax   = '',
                    contact__email = '',
                    contact__name  = 'int',
                    contact__org   = 'int',
                    contact__addr  = 'int',
                                } do
                local tag = xml.find(tag, _tag)
                if not tag then
                  return nil, 'missing '.._tag:gsub('__',':')
                end
                if _type ~= '' and _type ~= tag.type then
                  return nil, 'incorrect '.._tag:gsub('__',':')..' type'
                end
              end
              return true
            end,
            contact__postalInfo = function(tag)
              if 'int' ~= tag.type then
                return nil, 'missing or incorrect `type` aatribute'
              end
              for _tag,_val in pairs{
                    contact__org    = 'dermozona',
                    contact__street = 'streat',
                    contact__name   = 'odmen',
                    contact__city   = 'seetee',
                    contact__sp     = 'SP',
                    contact__pc     = '31337',
                    contact__cc     = 'ZW'
                               } do
                local subtag = xml.find(tag, _tag)
                if not subtag or _val ~= subtag[2] then
                  return nil, 'missing or incorrect '.._tag:gsub('__',':')
                end
              end
              return true
            end,
          }
  :do_contact_update{ status_add=false,
                      disclose = '0:name=int,org=int,addr=int,'
                                 ..'name=loc,org=loc,addr=loc,'
                                 ..'fax,email,voice',
                      postalinfo_loc = {
                        contact__org    = 'гомозона',
                        contact__name   = 'одминчег',
                        contact__city   = 'сэло',
                        contact__street = 'ул. Ленина',
                        contact__sp     = 'VI',
                        contact__pc     = '31337',
                        contact__cc     = 'CN' }
                    }:check{ OK }
  :do_contact_update{ postalinfo_loc = false }:check{ OK } -- FIXME

local date = require'date'
local not_published = '--[[not published--]]'
registrator_2
  :do_contact_info{ contact__id = 'admin1-8' }
    :check{ OK, contact__disclose = false,
            contact__org    = not_published,
            contact__name   = not_published,
            contact__city   = not_published,
            contact__street = not_published,
            contact__sp     = not_published,
            contact__pc     = not_published,
            contact__cc     = not_published,
            contact__voice  = not_published,
            contact__fax    = not_published,
            contact__email  = not_published,
          }
  :do_contact_delete{ contact__id = 'admin1-8' }:check{ E_AUTHORIZATION }
  :do_contact_update{ contact__id = 'admin2-1',
                      status_add = { 'clientDeleteProhibited' } }:check{ OK }
  :do_contact_delete{ contact__id = 'admin2-1'}:check{ E_STATUS_PROHIBITE }
  :do_contact_update{ status_add = {'clientUpdateProhibited' } }:check{ OK }
  :do_contact_update{ status_add=false,
                      status_rem = { 'clientDeleteProhibited' } }
    :check{ E_STATUS_PROHIBITE }
  :do_contact_update{ status_rem = {'clientUpdateProhibited' } }:check{ OK }
  :do_contact_update{ status_rem = { 'clientDeleteProhibited' } }:check{ OK }
  :do_contact_delete{ contact__id = 'admin2-1'}:check{ E_ASSOC_PROHIBITE }
  :do_contact_delete{ contact__id = '/\\/\\ y DUCK' }:check{ E_PARAM_VALUE }

registrator_3
  :do_domain_renew{ domain__name = 'test-2-3.in.ua' }:check{ E_SYNTAX }
  :do_domain_renew{ domain__name = 'dtest-3-3.in.ua',
                    domain__period = 1, domain__curExpDate = date():fmt'%F' }
    :check{ E_RENEW_NOT_ELIGIBLE }
  :do_domain_renew{ domain__curExpDate = date():addyears(1):fmt'%F' }
    :check{}
  :do_domain_renew{ domain__period = 2,
                    domain__curExpDate = date():addyears(2):fmt'%F' }
    :check{ E_PARAM_RANGE }
  :do_domain_info{ domain__name = 'dtest-3-3.in.ua' }
    :check{ OK, date():addyears(2):fmt'%F' }
  :do_host_create{ host__name = 'ns.dtest-3-3.in.ua', host__ip = {'dd::dd'}}
    :check{ E_AUTHORIZATION }

dealer_3_reg_3
  :do_host_create{ host__name = 'ns.dtest-3-3.in.ua', host__ip = {'dd::dd'}}
    :check{ OK }
  :do_domain_delete{ domain__name = 'dtest-3-3.in.ua' }
    :check{ E_ASSOC_PROHIBITE }
  :do_domain_update{ domain__name = 'dtest-3-3.in.ua',
                     ns_add = { 'ns.dtest-3-3.in.ua' } }:check{ OK }
  :do_host_delete{ host__name = 'ns.dtest-3-3.in.ua' }
    :check{ E_ASSOC_PROHIBITE }
  :do_domain_update{ domain__name = 'dtest-3-3.in.ua', ns_add=false,
                     ns_rem = { 'ns.dtest-3-3.in.ua' } }:check{ OK }

registrator_3
  :do_domain_update{ domain__name = 'test-1-1.in.ua',
                     ns_add = { 'ns.dtest-3-3.in.ua' } }:check{ OK }

dealer_3_reg_3
  :do_host_delete{ host__name = 'ns.dtest-3-3.in.ua' }:check{ OK }

