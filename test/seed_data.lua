local Ornament = require 'ornament'
require 'epp_settings'

local orn  = Ornament.new { user='ded', password='npbsmem9' }
local conn = orn:connect { database='epp_test' }

local epp_maintainer = conn:table "epp_maintainer" { }
local epp_contact = conn:table "epp_contact" { }
local epp_cpi = conn:table "epp_contact_postal_info" { }
local epp_domain = conn:table "epp_domain" {}
local epp_domain_contact = conn:table "epp_domain_contact_lnk" {}
local epp_host = conn:table "epp_host" {}
local epp_domain_ns = conn:table "epp_domain_ns_lnk" {}

for _,tbl in pairs {
  "epp_domain_ns_lnk", "epp_domain_contact_lnk",
  "epp_contact_postal_info", "epp_message_queue", "epp_domain_transfer",
  "epp_host", "epp_domain", "epp_contact", "epp_maintainer",
  } do
  conn.handle:prepare("TRUNCATE TABLE "..tbl.." RESTART IDENTITY CASCADE"):execute()
end

epp_maintainer:insert{
  m_id = EPP_ADMIN_MNT_ID,
  m_organization = '',
  m_authinfo = 'SHA256-PW $5$012345678$LhZI4Pe3p0zOjSjdxMN7L4S09l1kpWeGAPzF.s1V0RB',
}
local id_m = epp_maintainer:select{m_id = 'admin'}.id_m

crDate = os.date("%F %T") --'2013-05-23 05:50:00'

local posix = require'posix'
local gen_crypt = function(pwd, salt, prefix)
  local rv = prefix
  if pwd then
    if salt then
      if not salt:match(prefix:gsub('%$', '%%$'), nil) then
        salt = prefix..salt
      end
      rv = posix.crypt(pwd, salt)
    else -- no salt means RAW store representation
      rv = pwd
    end
  end
  return rv
end

registrators, dealers = {}, {}
for i=1,3 do
  local reg_name = 'reg'..i
  epp_maintainer:insert{
    m_id = reg_name,
    m_organization = reg_name..' REGISTRAR',
    m_authinfo = 'MD5-PW ' .. gen_crypt('reg'..i..'pwd', 'aaaaaaaa', '$1$'),
    m_clID_m = id_m,
    m_crID_m = id_m,
    m_crDate = crDate,
  }
  registrators[reg_name] = epp_maintainer:select{m_id = reg_name}
end

for reg_name,reg in pairs(registrators) do
  
  for i=1,3 do
    local dil_name = 'deal'..i..'.'..reg_name
    epp_maintainer:insert{
      m_id = dil_name,
      m_organization = reg_name..' DEALER '..i,
      m_authinfo = 'MD5-PW ' .. gen_crypt('deal'..i..'pwd', 'bbbbbbbb', '$1$'),
      m_clID_m = reg.id_m,
      m_crID_m = reg.id_m,
      m_crDate = crDate,
    }
    dealers[dil_name] = epp_maintainer:select{m_id = dil_name}
  end
end

print "-------------------------------"
for k,v in pairs(registrators) do print('r', k,v) end
for k,v in pairs(dealers) do print('d', k,v) end

conn.handle:commit()

