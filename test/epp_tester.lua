require 'snippets'
local xml = require 'luaxml'

local templates = require'epp_query_templates'

local parse_cmd_result = function(result)
  local rv = { raw = result }
  local result = xml.eval(result)
  rv.parsed = result
  if result then
    rv.code = xml.find(result, 'result').code
    rv.code = rv.code and tonumber(rv.code)
  end
  return rv
end

local maker_do_commands = function(self, cmd_name)
  local cmd_env = self.envs[cmd_name] or { templates[cmd_name] }
  self.envs[cmd_name] = cmd_env

  return function(self, cmd_env_part)
    table.hash_merge(cmd_env, cmd_env_part, true) -- update
    for k,v in pairs(cmd_env) do
      if not v then cmd_env[k] = nil end -- cleanup/reset
    end
  
    local result, req = self.client:process_cmd(cmd_env)
    print("########### "..(self.envs.login.clID or '<not logged in>'))
    print">>>> COMMAND:"
    print(req)
    print"<<<< RESPONSE:"
    print(result)
    print""
    self.result = parse_cmd_result(result)
    self.raw_results[cmd_name] = result
  
    return result and self
  end
end

local RES = require 'epp_response_codes'
local function code_name(code)
  for k,v in pairs(RES) do
    if v == code then return k end
  end
  return "HZ"
end

local check = function(self, asserts)
  local _ = code_name
  local x = self.result.parsed
  local exp_code = asserts[1] or RES.OK
  local res_code = self.result.code
  local _= assert(exp_code == res_code,
             "resp ".. _(res_code) .. "("..res_code..") <> " ..
                       _(exp_code) .. "("..exp_code..")"
           )
  for k,v in pairs(asserts) do
    if 'number' ~= type(k) then -- check tags
      k = k:gsub("__", ":")
      local tag = xml.find(x,k)
      local type_v = type(v)
      if 'boolean' == type_v then -- tag presence/absence check
        assert(not not v == not not tag, (v and "miss " or "has ")..'tag '..k)
      elseif 'table' == type_v then -- tag value check
        if v[1] then
          assert(tag[2] == v[1], k..' value <> '..v[1])
        end
        for kk,vv in pairs(v) do -- tag attributes check
          if 'number' ~= type(kk) then
            local val = tostring(tag[kk])
            assert(val == vv, k..' attr '..kk..' '..val..' <> '..vv)
          end
        end
      elseif 'function' == type_v then -- lambda function check
        assert(v(tag, k, self.result.parsed, self.result.raw))
      else
        assert(tag[2] == v, 'value of xml tag '..tag[1]..' is not '..v)
      end
    elseif k ~= 1 then -- not a status check, the textual grep check
      local _v = v:gsub('-', '%%-')
      if '!' == _v:sub(1,1) then
        assert(
          not self.result.raw:match(_v:sub(2)),
          "response shouldn't contain "..v
        )
      else
        assert(self.result.raw:match(_v), "response should contain ".. v)
      end
    end
  end
  return self
end

local tester_instance_mt = {
  check = check,
  maker_do_commands = maker_do_commands,
}

function make_tester(cli)
  cli:connect()
  return setmetatable(
    { client = cli, envs = {}, raw_results = {} },
    { __index = function(tbl, idx)
    if 'do_' == idx:sub(1,3) then
      local do_func = tbl:maker_do_commands(idx:sub(4))
      tbl[idx] = do_func
      return do_func
    end
    return tester_instance_mt[idx]
  end,
      tester_instance_mt }
  )
end

return {
  make_tester = make_tester,
}

