
var last_active_tab = 1
var last_active_subtabs = new Array(tabs_cnt+1)
var tabs_active = 0
var sub_act_bgcolor = "#DDD"
var sub_ina_bgcolor = "white"
var button_pressed = false

for(var i=0; i < tabs_cnt; ++i) {
   last_active_subtabs[i+1] = 1
}

function activate_tab(num) {
   if (tabs_active && last_active_tab == num) {
      return
   }
   var lnum = last_active_tab+"_"+last_active_subtabs[last_active_tab]
   var nnum = num+"_"+last_active_subtabs[num]
   document.getElementById("sublayer_"+lnum).style.display = "none"
   document.getElementById("menu_"+lnum).style.backgroundColor = sub_ina_bgcolor
   document.getElementById("layer_"+last_active_tab).style.display = "none"
   document.getElementById("anchor_"+last_active_tab).style.textDecoration = "none"
   document.getElementById("layer_"+num).style.display = ""
   document.getElementById("anchor_"+num).style.textDecoration = "underline"
   document.getElementById("sublayer_"+nnum).style.display = ""
   document.getElementById("menu_"+nnum).style.backgroundColor = sub_act_bgcolor

   last_active_tab = num
}

function activate_subtab(tnum, snum) {
   if (tabs_active && last_active_tab == tnum && last_active_subtabs[tnum] == snum) {
      return
   }
   activate_tab(tnum)
   var lnum = tnum+"_"+last_active_subtabs[tnum]
   tabs_active = 1
   document.getElementById("sublayer_"+lnum).style.display = "none"
   document.getElementById("menu_"+lnum).style.backgroundColor = sub_ina_bgcolor
   document.getElementById("sublayer_"+tnum+"_"+snum).style.display = ""
   document.getElementById("menu_"+tnum+"_"+snum).style.backgroundColor = sub_act_bgcolor
   last_active_subtabs[tnum] = snum
   setCookie("tabnum", tnum)
   setCookie("subtabnum", snum)
}

function buttonDown(button) {
    button.className = 'myButtonDown'
}

function buttonUp(button) {
    button.className = 'myButton'
}

// cookie funcs taken from http://javascript.ru/unsorted/top-10-functions
// возвращает cookie если есть или undefined
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ))
  return matches ? decodeURIComponent(matches[1]) : undefined 
}
// уcтанавливает cookie
function setCookie(name, value, props) {
  props = props || {}
  var exp = props.expires
  if (typeof exp == "number" && exp) {
    var d = new Date()
    d.setTime(d.getTime() + exp*1000)
    exp = props.expires = d
  }
  if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

  value = encodeURIComponent(value)
  var updatedCookie = name + "=" + value
  for(var propName in props){
    updatedCookie += "; " + propName
    var propValue = props[propName]
    if(propValue !== true){ updatedCookie += "=" + propValue }
  }
  document.cookie = updatedCookie

}
// удаляет cookie
function deleteCookie(name) {
  setCookie(name, null, { expires: -1 })
}

