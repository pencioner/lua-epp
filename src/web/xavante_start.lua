require "xavante"
local tls_opts = {
  mode = "client",
  protocol = "tlsv1",
  key = "keys/clientAkey.pem",
  certificate = "keys/clientA.pem",
  cafile = "keys/rootA.pem",
  verify = {"peer", "fail_if_no_peer_cert"},
  options = {"all", "no_sslv2"},
}
local client = require'epp_client_al'
local query_tpl = require'epp_query_templates'

local posix=require'posix'

require "xavante.redirecthandler"
require "xavante.filehandler"

web_dir = "." -- posix.getcwd() -- TODO FIXME
TEMPLATEDIR = web_dir.."/"

-- no virtualhosts etc - it's intended mostly for helping manual testing, not as a real web-interface
config = {
    server = {host = "*", port = 8700},
    defaultHost = {},
    virtualhosts = {},
}

local session=require"xavante.session"

local function get_session(req, res)
  local s = session.open(req, res, "a3a331110")
  if not s.client then
    s.client = client.create_client_instance('127.0.0.1', 7700, tls_opts)
    local is_ok, err = pcall(s.client.connect, s.client, true)
    if not is_ok then
      s.client = nil
      error(err)
    end
  end
  return s
end

local url = require'url'
local parse_query = url.parseQuery

require'tirtemplates'

function pre(str)
  return (str or '?'):gsub('[&<>]', { ['>'] = '&gt;', ['<'] = '&lt;', ['&'] = '&amp;' })
end


function render_input_generic(itype, name, val, additional)
  return ([[<input type='%s' name='%s' value='%s' %s />]]):format(itype, name, 'string'==type(val) and val or '', additional or '')
end
function render_input(name, val, extra)
  return render_input_generic('text', name, val, extra)
end
function render_passwd(name, val)
  return render_input_generic('password', name, val)
end
function render_submit(name,val)
  return render_input_generic('submit', name, val)
end
function render_checkbox(name, val, checked)
  return render_input_generic('checkbox', name, val, checked and "checked='checked'")
end
function render_select(name, vals, extra)
  local options = {}
  for _,v in pairs(vals) do
    options[1+#options] = ([[<option value='%s'>%s</option>]]):format(v,v)
  end
  return ([[<select name='%s' %s>%s</select>]]):format(
    name, extra or '', table.concat(options)
  )
end

local function do_command(s, cmd)
  local resp, req = s.client:process_cmd(cmd)
--  print">>>>" print(req)
--  print"<<<<" print(resp)
  resp, req = pre(resp), pre(req)
  s.query.resp, s.query.req = resp, req
  local history = s.history or {}
  table.insert(history, 1, { resp=resp, req=req })
  history[21] = nil -- no more than 20 entries. FIXME magic number
  s.history = s.history or history
  s.query.history = history
end

local function gen_general_handler(template, before)
  return function(s)
    s.query[1] = template
    if before then before(s.query) end
    do_command(s, s.query)
  end
end

local function purge_empty_fields(tbl)
  for k,v in pairs(tbl) do
    if type(v) == 'table' then
      purge_empty_fields(v)
      local cnt =0
      for _ in pairs(v) do cnt=cnt+1 end
      if 0 == cnt then tbl[k]=nil end
    elseif ''==tostring(v) then
      tbl[k]=nil
    end
  end
end

local function combine_disclose_info(query)
  if query.disclose_what and #query.disclose_what > 0 then
    query.disclose = '1:'..table.concat(query.disclose_what, ',')
  end
end
local function prepare_domain_op_nsi(query, idx_src, idx_dst)
  local nsi = query[idx_src]
  if nsi then
    local domain__ns = {}
    for _, ns in pairs(nsi) do
      local ns_parts = { ns.host }
      if ns_parts[1] and ns.create then
        if ns.ips then
          for _,ip in pairs(ns.ips) do
            ns_parts[1+#ns_parts] = ip
          end
        else
          ns_parts[2] = ''
        end
      end
      domain__ns[1+#domain__ns] = table.concat(ns_parts, '/')
    end
    query[idx_dst] = domain__ns
  end
end
local function prepare_domain_op_contacts(query, idx_adm, idx_tech, idx_dst)
  local domain__contact = {}
  for ctype,contact in pairs{ admin=query[idx_adm], tech = query[idx_tech] } do
    for _,c in pairs(contact) do
      domain__contact[1+#domain__contact] = ctype..'='..c
    end
  end
  if domain__contact[1] then
    query[idx_dst] = domain__contact
  end
end
local before_handlers = {
  contact_create = combine_disclose_info,
  contact_update = combine_disclose_info,
  domain_create = function(query)
    prepare_domain_op_contacts(query, 'admin', 'tech', 'domain__contact')
    prepare_domain_op_nsi(query, 'ns', 'domain__ns')
  end,
  domain_update = function(query)
    prepare_domain_op_contacts(query, 'admin_rem', 'tech_rem', 'contact_rem')
    prepare_domain_op_nsi(query, 'ns_rem', 'ns_rem')
    prepare_domain_op_contacts(query, 'admin_add', 'tech_add', 'contact_add')
    prepare_domain_op_nsi(query, 'ns_add', 'ns_add')
  end,
  domain_transfer = function(query)
    if 'request' ~= query.transfer_op then
      query.domain__period = nil
      if 'query' ~= query.transfer_op then
        query.domain__authInfo = nil
      end
    end
  end,
}

local handlers = {
  index  = function() end,
  logout = function(s)
    do_command(s, { query_tpl.logout })
    if s.query.disconnect then
      s.client:shutdown()
      s.client = nil
    end
  end,
  login = gen_general_handler(query_tpl.login),
  hello = gen_general_handler(query_tpl.hello),
  poll = gen_general_handler(query_tpl.poll),
}

for prefix,names in pairs {
      domain  = {
       'check', 'create', 'delete', 'info',
       'renew', 'restore', 'transfer', 'update'
      },
      host    = { 'check', 'create', 'delete', 'info', 'update' },
      contact = { 'check', 'create', 'delete', 'info', 'update' },
    } do
  for _, name in pairs(names) do
    local name = prefix.."_"..name
    handlers[name] = gen_general_handler(
      query_tpl[name], before_handlers[name]
    )
  end
end

local simplerules = {
  {
    match = "^[^%./]*/$",
    with = xavante.redirecthandler,
    params = {"/epp"},
  },
  
  {
    match = "/epp:?(.*)",
    with = function(req, res, match)
      local s = get_session(req, res)
  
      local query = req.parsed_url.query
      s.query = query and parse_query(query) or s.query or {}

      local action = match[1]
      if not action or ''==action then
        for k,v in pairs(s.query) do
          if 'string'==type(k) then
            local _action = k:match'action_(.*)'
            if _action then action = _action break end
          end
        end

        if not action or ''==action then action = 'index' end
      end
      local subquery = s.query[action]
      if subquery then
        setmetatable(subquery, getmetatable(s.query))
        s.query = subquery
      end
      purge_empty_fields(s.query)
      handlers[action](s)
  
      res:add_header("Content-Type", "text/html")
      res:send_data(tirtemplate.tload('index.tir')(s.query))
      s.query = nil
    end,
  },
  
  -- it's the trick 'transitional' handler which parse GET for request (fill session variable) and redirects
  -- it so the URL in browser not get pollute. this is not mandatory, but my laziness and time outage considered
  -- it to be simpler than looking for adding POST support
  -- ATTENTION: remember to clean up s.query from final destination handler because bug does not sleep
  {
    match = "/req:([a-z_]+)",
    with = function(req, res, match)
      local s = get_session(req, res)
      local hdlr = match[1] or "epp"
      res:add_header("Content-Type", "text/html")
      local len = tonumber(req.headers["content-length"] or 0)
      local query = 0 < len and req.socket:receive(len) or req.parsed_url.query
      s.query = parse_query(query or '')
      xavante.httpd.redirect(res, "/"..hdlr)
    end,
  },

  { -- filehandler
    match = ".",
    with = xavante.filehandler,
    params = {baseDir = web_dir}
  },
}

config.defaultHost = { rules = simplerules, }

-- load the configuration table
xavante.HTTP(config)

-- run xavante
xavante.start()

