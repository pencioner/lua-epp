local partial = require 'partial'
local partial_one = partial.make_partial_1

require 'snippets'

require 'epp_settings'
local XML_HEADER = XML_HEADER

local XMLNS_EPP_HOST    = XMLNS_EPP_HOST
local XMLNS_EPP_DOMAIN  = XMLNS_EPP_DOMAIN
local XMLNS_EPP_CONTACT = XMLNS_EPP_CONTACT

local VERSION = EPP_CLIENT_VERSION

local SCARY_FENV_META = setmetatable({}, { __index=function(_,k) return _G[k] or k end})

local xml = require 'luaxml'
local gsub_tag = function(tag)
  if 'string'==type(tag) then tag = tag:gsub('__', ':') end
  return tag
end
local call_func_chain = function(item)
  if 'function'==type(item) then item = item() end
  return item
end
xml.set_tag_filters {
  gsub_tag,
  call_func_chain,
} 
xml.set_val_filters {
  call_func_chain,
}

-- so table constructor { spawn_tag(...) } will create table with tag name and value
local function spawn_tag(env, idx, mutator, other_tag_name)
  local val = env[idx]

  if val then
    local name = gsub_tag(other_tag_name or idx)
    if mutator then
      return name, mutator(val)
    end
    return name, val
  end
end

local function spawn_tags_arr(env, idx, mutator, other_tag_name)
  local tags = {}
  local tag_name = gsub_tag(other_tag_name or idx)
  for _, val in pairs(env[idx] or {}) do
    local tag = { tag_name, val }
    if mutator then mutator(tag) end -- mutate whole tag
    tags[1+#tags] = tag
  end
  return tags
end

local function spawn_tags_unpack(env, idx, mutator, other_tag_name)
  return unpack(spawn_tags_arr(env, idx, mutator, other_tag_name))
end

local function spawn_tag_authinfo(env, item_type)
  local tag_authinfo = item_type..'__authInfo'
  local tag_pw       = item_type..'__pw'
  return spawn_tag(env, tag_authinfo,
    function(authinfo)
      return { spawn_tag(env, tag_authinfo, nil, tag_pw) }
    end
  )
end

local function spawn_tags_statusi(env, idx, other_tag_name)
  local tag_name = gsub_tag(other_tag_name or idx)

  return spawn_tags_arr(env, idx,
           function(tag)
             tag.s = tag[2]
             -- the hostmaster example shows
             --   <contact:status s="..."></contact:status>
             -- instead of
             --   <contact:status s="..." />
             -- in case of incompatibilities assign empty str, not nil
             tag[2] = nil --''
           end,
           tag_name
         )
end
local function spawn_tags_statusi_unpack(env, idx, other_tag_name)
  return unpack(spawn_tags_statusi(env, idx, other_tag_name))
end

local function make_command(env, ...)
  local cmd_ent = {...}
  return XML_HEADER ..
    xml.new { epp, xmlns = XMLNS_EPP_ROOT,
      { command,
        unpack(table.array_merge(
          cmd_ent,
          {{ spawn_tag(env, 'clTRID') }}
        ))
      }
    }:str()
end
setfenv(make_command, SCARY_FENV_META)

local host_info_tpl = function(env)
  return make_command(env, { info,
    { host__info, xmlns__host = XMLNS_EPP_HOST,
      { spawn_tag(env, host__name) },
    }
  })
end
setfenv(host_info_tpl, SCARY_FENV_META)

local domain_info_tpl = function(env)
  return make_command(env, { info,
    { domain__info, xmlns__domain = XMLNS_EPP_DOMAIN,
      { spawn_tag(env, domain__name) },
      { spawn_tag_authinfo(env, 'domain') },
    }
  })
end
setfenv(domain_info_tpl, SCARY_FENV_META)

local contact_info_tpl = function(env)
  return make_command(env, { info,
    { contact__info, xmlns__contact = XMLNS_EPP_CONTACT,
      { spawn_tag(env, contact__id) },
      { spawn_tag_authinfo(env, 'contact') },
    }
  })
end
setfenv(contact_info_tpl, SCARY_FENV_META)

local ip_mutator = function(tag)
  tag.ip = tag[2]:find':' and 'v6' or 'v4'
end

local host_create_tpl = function(env)
  return make_command(env, { create,
    { host__create, xmlns__host = XMLNS_EPP_HOST,
      { spawn_tag(env, host__name) },
      spawn_tags_unpack(env, host__ip, ip_mutator)
    }
  })
end
setfenv(host_create_tpl, SCARY_FENV_META)

local host_update_tpl = function(env)
  return make_command(env, { update,
    { host__update, xmlns__host = XMLNS_EPP_HOST,
      { spawn_tag(env, host__name) },
      { (env.status_add or env.addrs_add) and host__add,
        unpack(table.array_merge(
          spawn_tags_arr(env, 'addrs_add', ip_mutator, host__addr),
          spawn_tags_statusi(env, 'status_add', host__status)
        ))
      },
      { (env.status_rem or env.addrs_rem) and host__rem,
        unpack(table.array_merge(
          spawn_tags_arr(env, 'addrs_rem', ip_mutator, host__addr),
          spawn_tags_statusi(env, 'status_rem', host__status)
        ))
      },
    }
  })
end
setfenv(host_update_tpl, SCARY_FENV_META)

local host_check_tpl = function(env)
  return make_command(env, { check,
    { host__check, xmlns__host = XMLNS_EPP_HOST,
      spawn_tags_unpack(env, host__name)
    }
  })
end
setfenv(host_check_tpl, SCARY_FENV_META)

local domain_check_tpl = function(env)
  return make_command(env, { check,
    { domain__check, xmlns__domain = XMLNS_EPP_DOMAIN,
      spawn_tags_unpack(env, domain__name)
    }
  })
end
setfenv(domain_check_tpl, SCARY_FENV_META)

local contact_check_tpl = function(env)
  return make_command(env, { check,
    { contact__check, xmlns__contact = XMLNS_EPP_CONTACT,
      spawn_tags_unpack(env, contact__id)
    }
  })
end
setfenv(contact_check_tpl, SCARY_FENV_META)

local domain_transfer_tpl = function(env)
  return make_command(env, { transfer, op = env.transfer_op or 'query',
    { domain__transfer, xmlns__domain = XMLNS_EPP_DOMAIN,
      { spawn_tag(env, domain__name) },
      { unit = 'y', spawn_tag(env, domain__period) },
      { spawn_tag_authinfo(env, 'domain') },
    }
  })
end
setfenv(domain_transfer_tpl, SCARY_FENV_META)

local domain_renew_tpl = function(env)
  return make_command(env, { renew,
    { domain__renew, xmlns__domain = XMLNS_EPP_DOMAIN,
      { spawn_tag(env, domain__name) },
      { spawn_tag(env, domain__curExpDate) },
      { unit = 'y', spawn_tag(env, domain__period) },
    }
  })
end
setfenv(domain_renew_tpl, SCARY_FENV_META)

local generate_disclose_tag = function(d_str)
  if d_str then
    local dtag = { 'contact:disclose', flag=d_str:sub(1,1) }
    d_str = d_str:sub(3)
    d_str:gsub("[^,]+", function(match)
      local equal_sgn = match:find'='
      if not equal_sgn then
        dtag[1+#dtag] = { 'contact:'..match }
      else
        local what, type = match:sub(1, -1+equal_sgn), match:sub(1+equal_sgn)
        dtag[1+#dtag] = { 'contact:'..what, type=type }
      end
    end)
    return dtag
  end
end
local postal_info_tpl = function(pi_env) -- partial for contact_create_tpl, contact_update_tpl
  return { spawn_tag(pi_env, contact__name) },
         { spawn_tag(pi_env, contact__org) },
         { contact__addr,
           { spawn_tag(pi_env, contact__street) },
           { spawn_tag(pi_env, contact__city) },
           { spawn_tag(pi_env, contact__sp) },
           { spawn_tag(pi_env, contact__pc) },
           { spawn_tag(pi_env, contact__cc) },
         }
end
setfenv(postal_info_tpl, SCARY_FENV_META)
local contact_create_tpl = function(env)
  return make_command(env, { create,
    { contact__create, xmlns__contact = XMLNS_EPP_CONTACT,
      { spawn_tag(env, contact__id) },
      { type='int',
        spawn_tag(env, postalinfo_int, postal_info_tpl, contact__postalInfo)
      },
      { type='loc',
        spawn_tag(env, postalinfo_loc, postal_info_tpl, contact__postalInfo)
      },
      { spawn_tag(env, contact__voice) },
      { spawn_tag(env, contact__fax) },
      { spawn_tag(env, contact__email) },
      { spawn_tag_authinfo(env, 'contact') },
      generate_disclose_tag(env.disclose)
    },
  })
end
setfenv(contact_create_tpl, SCARY_FENV_META)

local got_postalinfo_updates = function(env)
  return env.postalinfo_int or env.postalinfo_loc or env.disclose
          or env.contact__voice or env.contact__fax or env.contact__email
          or env.contact__authInfo
end
local contact_update_tpl = function(env)
  return make_command(env, { update,
    { contact__update, xmlns__contact = XMLNS_EPP_CONTACT,
      { spawn_tag(env, contact__id) },
      { env.add and contact__add, -- this is legacy obsolete one, TODO FIXME remove it in favor of status_add
        spawn_tags_statusi_unpack(env, 'add', contact__status)
      },
      { env.status_add and contact__add,
        spawn_tags_statusi_unpack(env, 'status_add', contact__status)
      },
      { env.rem and contact__rem, -- legace obsolete one like `add` one
        spawn_tags_statusi_unpack(env, 'rem', contact__status)
      },
      { env.status_rem and contact__rem,
        spawn_tags_statusi_unpack(env, 'status_rem', contact__status)
      },
      { got_postalinfo_updates(env) and contact__chg,
        { type='int',
          spawn_tag(env, postalinfo_int, postal_info_tpl, contact__postalInfo)
        },
        { type='loc',
          spawn_tag(env, postalinfo_loc, postal_info_tpl, contact__postalInfo)
        },
        { spawn_tag(env, contact__voice) },
        { spawn_tag(env, contact__fax) },
        { spawn_tag(env, contact__email) },
        { spawn_tag_authinfo(env, 'contact') },
        generate_disclose_tag(env.disclose)
      }
    }
  })
end
setfenv(contact_update_tpl, SCARY_FENV_META)

local contact_mutator = function(c)
  -- [2]='admin=cnt' ==> [2]='cnt',type='admin'
  local c_val = c[2]
  local equal_pos = c_val:find'='
  c[2], c.type = c_val:sub(1+equal_pos), c_val:sub(1, -1+equal_pos)
  return c
end
local domain_ns_tpl = function(ns) -- partial for domain:ns
  local nsi = {}
  for _, host in pairs(ns) do
    local slash_pos = host:find'/'
    if not slash_pos then
      nsi[1+#nsi] = { domain__hostObj, host }
    else
      local host_attr = { domain__hostAttr }
      local hostname, ips = host:sub(1, -1+slash_pos), host:sub(1+slash_pos)
      host_attr[2] = { domain__hostName, hostname }
      ips:gsub('[^/]+', function(ip)
        local ip_v = ip:find":" and 'v6' or 'v4'
        host_attr[1+#host_attr] = { domain__hostAddr, ip=ip_v, ip }
      end)
      nsi[1+#nsi] = host_attr
    end
  end
  return unpack(nsi)
end
setfenv(domain_ns_tpl, SCARY_FENV_META)
local domain_create_tpl = function(env)
  return make_command(env, { create,
    { domain__create, xmlns__domain = XMLNS_EPP_DOMAIN,
      { spawn_tag(env, domain__name) },
      { unit='y', spawn_tag(env, domain__period) },
      { spawn_tag(env, domain__ns, domain_ns_tpl) },
      { spawn_tag(env, domain__registrant) },
      spawn_tags_unpack(env, domain__contact, contact_mutator)
    }
  })
end
setfenv(domain_create_tpl, SCARY_FENV_META)
local domain_update_tpl = function(env)
  return make_command(env, { update,
    { domain__update, xmlns__domain = XMLNS_EPP_DOMAIN,
      { spawn_tag(env, domain__name) },
      { (env.domain__registrant or env.domain__authInfo or env.domain__null)
          and domain__chg,
        { spawn_tag(env, domain__registrant) },
        { spawn_tag_authinfo(env, 'domain') },
        { env.domain__null and domain__authInfo,
          { domain__null }
        },
      },
      { (env.status_add or env.ns_add or env.contact_add) and domain__add,
        { spawn_tag(env, ns_add, domain_ns_tpl, domain__ns) },
        unpack(table.array_merge( 
          spawn_tags_arr(env, contact_add, contact_mutator, domain__contact),
          spawn_tags_statusi(env, 'status_add', domain__status)
        ))
      },
      { (env.status_rem or env.ns_rem or env.contact_rem) and domain__rem,
        { spawn_tag(env, ns_rem, domain_ns_tpl, domain__ns) },
        unpack(table.array_merge(
          spawn_tags_arr(env, contact_rem, contact_mutator, domain__contact),
          spawn_tags_statusi(env, 'status_rem', domain__status)
        ))
      },
    },
  })
end
setfenv(domain_update_tpl, SCARY_FENV_META)
local domain_restore_tpl = function(env)
  return make_command(env,
    { update,
      { domain__update, xmlns__domain = XMLNS_EPP_DOMAIN,
        { spawn_tag(env, domain__name) },
      }
    },
    { extension,
      { rgp__update, xmlns__rgp = XMLNS_EPP_RGP,
        { rgp__restore, op='request' }
      }
    }
  )
end
setfenv(domain_restore_tpl, SCARY_FENV_META)

local contact_delete_tpl = function(env)
  return make_command(env, { delete,
    { contact__delete, xmlns__contact = XMLNS_EPP_CONTACT,
      { spawn_tag(env, contact__id) },
    }
  })
end
setfenv(contact_delete_tpl, SCARY_FENV_META)

local host_delete_tpl = function(env)
  return make_command(env, { delete, force = env.force,
    { host__delete, xmlns__host = XMLNS_EPP_HOST,
      { spawn_tag(env, host__name) },
    }
  })
end
setfenv(host_delete_tpl, SCARY_FENV_META)

local domain_delete_tpl = function(env)
  return make_command(env, { delete, force = env.force,
    { domain__delete, xmlns__domain = XMLNS_EPP_DOMAIN,
      { spawn_tag(env, domain__name) },
    }
  })
end
setfenv(domain_delete_tpl, SCARY_FENV_META)


local poll_tpl = function(env)
  return make_command(env, { 'poll',
    op = env.poll_op or 'req',
    msgID = env.msgID,
  })
end


local login_tpl = function(env)
  return make_command(env, { 'login',
    { spawn_tag(env, 'clID') },
    { spawn_tag(env, 'pw') },
    { spawn_tag(env, 'newPW') },
    { 'options',
      { 'version', VERSION },
      { 'lang', env.lang or 'en' },
      { 'svcs',
        { 'objURI', XMLNS_EPP_CONTACT },
        { 'objURI', XMLNS_EPP_DOMAIN },
        { 'objURI', XMLNS_EPP_HOST },
      },
    },
  })
end
local logout_tpl = function(env)
  return make_command(env, { 'logout' })
end
local hello_tpl = function()
  return XML_HEADER .. 
    tostring(xml.new { 'epp', xmlns = XMLNS_EPP_ROOT, { 'hello' }})
end

return {
  login = login_tpl,
  logout = logout_tpl,
  hello = hello_tpl,

  poll = poll_tpl,

  host_info = host_info_tpl,
  host_check = host_check_tpl,
  host_create = host_create_tpl,
  host_delete = host_delete_tpl,
  host_update = host_update_tpl,

  domain_check = domain_check_tpl,
  domain_info  = domain_info_tpl,
  domain_transfer = domain_transfer_tpl,
  domain_create = domain_create_tpl,
  domain_update = domain_update_tpl,
  domain_delete = domain_delete_tpl,
  domain_restore = domain_restore_tpl,
  domain_renew = domain_renew_tpl,

  contact_check = contact_check_tpl,
  contact_info = contact_info_tpl,
  contact_create = contact_create_tpl,
  contact_update = contact_update_tpl,
  contact_delete = contact_delete_tpl,
}

