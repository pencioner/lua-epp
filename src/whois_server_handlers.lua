require 'epp_settings'

local yield = coroutine.yield

local pcre = require 'rex_pcre'
local pcre_match = pcre.match
local DOMAIN_RE = EPP_DEFAULT_DOMAIN_SUFFIX_REX
local DOMAIN_SEGMENT_RE = '([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])'
local DOMAIN_MATCHER = '^'..DOMAIN_SEGMENT_RE..DOMAIN_RE..'$'

local WHOIS_HEADER = WHOIS_HEADER
local WHOIS_SOURCE = WHOIS_SOURCE_NAME

local suffix_lambda = require'snippets'.suffix_lambda

local function memoizer_maker(db_tbl, idx_fld)
  local memo_objs_tbl, memo_ids_tbl = {}, {}

  local function memo_put(obj, id_obj)
    local id_obj = id_obj or obj[idx_fld]
    memo_objs_tbl[id_obj] = obj
    memo_ids_tbl[1+#memo_ids_tbl] = id_obj
  end
  local function memo_get(id_obj)
    local obj = memo_objs_tbl[id_obj]
    if not obj then
      obj = db_tbl:select{ [idx_fld] = id_obj }
      memo_put(obj, id_obj)
    end
    return obj
  end

  return memo_get, memo_put, memo_objs_tbl, memo_ids_tbl
end

local function prefixer_gen_gen(width, delim)
  return function(prefix)
    local rest_width = width - #delim - #prefix
    local spaces = ("%%%ds"):format(rest_width):format''
    local formatted_prefix = prefix..delim..spaces
    return function(str)
      if str then
        return formatted_prefix .. str
      end
    end
  end
end


local _width = 19
local _delim = ':'
local prefixer_gen = prefixer_gen_gen(_width, _delim)

-- convenient way of auto-generating prefixers for shorter code
-- for future optimizations: localize by
--   local _P_domain   = _P_.domain 
-- etc
local _P_ = setmetatable({}, {
  __index = function(_p_, idx)
    rawset(_p_, idx, prefixer_gen(idx:gsub('_', '-')))
    return _p_[idx]
  end,
})

local WHOIS_CONTACT_DISLOSED_TEXT = WHOIS_CONTACT_DISLOSED_TEXT
local enclose = function(is_disclosed, text, disclose_text)
  disclose_text = disclose_text or WHOIS_CONTACT_DISLOSED_TEXT
  return not is_disclosed and disclose_text or text
end
local function add_contact_response_ex(env, id_c)
  local output = env.output
  local c = env.c_get(id_c)
  if not c then
    output.raw = '% ERROR'
    return
  end
  local arr_split = env.dbm.helpers.db_array_split
  local dgen = table.topsy_turvy(arr_split(c.c_disclose))
  local dloc = table.topsy_turvy(arr_split(c.c_disclose_loc))
  local dint = table.topsy_turvy(arr_split(c.c_disclose_int))

  local pint = env.epp_cpi:select{cpi_contact_c = id_c, cpi_type = 'int'}
  local ploc = env.epp_cpi:select{cpi_contact_c = id_c, cpi_type = 'loc'}

  output.contact_id = c.c_id

  local dinfos = { [''] = dint, _loc = dloc }
  for suffix, pinfo in pairs{ [''] = pint, _loc = ploc } do
    local dinfo = dinfos[suffix]
    local with_suffix = suffix_lambda(suffix)

    output[with_suffix'person']        = enclose(dinfo.name, pinfo.cpi_name)
    output[with_suffix'organization']  = enclose(dinfo.org, pinfo.cpi_org)

    if not dinfo.addr then
      output[with_suffix'address']     = WHOIS_CONTACT_DISLOSED_TEXT
    else
      pinfo.cpi_street:gsub('[^\r\n]+', function(line)
        output[with_suffix'address']   = line
      end)
      output[with_suffix'address']     = pinfo.cpi_city
      output[with_suffix'address']     = pinfo.cpi_sp
      output[with_suffix'postal_code'] = pinfo.cpi_pc
      output[with_suffix'country']     = pinfo.cpi_cc
    end
  end

  output.email    = enclose(dgen.email, c.c_email)
  output.phone    = enclose(dgen.voice, c.c_voice)
  output.fax      = enclose(dgen.fax,   c.c_fax)

  output.mnt_by   = env.m_get(c.c_clid_m).m_id
  output.status   = c.c_statusi:gsub('[{}]', '')
  output.created  = env.format_time(c.c_crdate)
  output.modified = c.c_upid and env.format_time(c.c_update)
  output.source   = WHOIS_SOURCE
  output.raw      = ''
end

local function add_contact_response(env, c_id)
  local c = env.epp_contact:select{ c_id = c_id }
  if not c then
    env.output.raw = '% Object not found: '..c_id
    return
  end
  local id_c = c.id_c
  env.c_put(c, id_c)
  add_contact_response_ex(env, id_c)
end

local c_suffix = suffix_lambda('-c')
local function add_domain_response(env, d_name)
  local output = env.output
  local d = env.epp_domain:select{ d_name = d_name }
  if not d then
    output.raw = '% Domain not found: '..d_name
    return
  end
  local id_d, registrant_c = d.id_d, d.d_registrant_c
  local registrant = env.c_get(registrant_c)
  local registrar  = env.m_get(d.d_clid_m)
  local dealer     = env.m_get(d.d_dlid_m)
  local contact_lnk = env.epp_domain_contact:find{ dc_domain_d = id_d }
  local contacts = { admin = {}, tech = {} }
  for _, lnk in pairs(contact_lnk) do
    local c_tbl = contacts[lnk.dc_contact_type]
    c_tbl[1+#c_tbl] = env.c_get(lnk.dc_contact_c)
  end
  output.domain = d.d_name
--  output.dom_public = 'NO' -- TODO need support in RDB and business-logic
  output.registrant = registrant.c_id
  for _, c_type in pairs{ 'admin', 'tech' } do
    for _, who in pairs(contacts[c_type]) do
      output[c_suffix(c_type)] = c_id
    end
  end
  output.mnt_by = registrar.m_id
  output.mnt_lower = dealer and dealer.m_id
  local ns_lnk = env.epp_domain_ns:find{ dn_domain_d = id_d }
  for _, lnk in pairs(ns_lnk) do
    local h = env.epp_host:select{ id_h = lnk.dn_host_h }
    output.nserver = h.h_name
  end
  output.status           = d.d_statusi:gsub('[{}]', '')
  output.created          = env.format_time(d.d_crdate)
  output.modified         = d.d_upid and env.format_time(d.d_update)
  output.expires          = env.format_time(d.d_exdate)
  output.source           = WHOIS_SOURCE
  output.raw              = ''

  output.registrar        = registrar.m_id
  output.organization     = registrar.m_organization
  output.organization_loc = registrar.m_organization_loc
  output.city             = registrar.m_city
  output.country          = registrar.m_country
  output.source           = WHOIS_SOURCE
  output.raw              = ''

  -- registrant is first one from contacts to show
  add_contact_response_ex(env, registrant_c)

  for _,id_c in pairs(env.c_ids) do
    if id_c ~= registrant_c then
      add_contact_response_ex(env, id_c)
    end
  end
end

local accept_connect = function(env, sock, ip, port)
  env.ip, env.port = ip, port
  env.connected_at = posix.time()
  env.ip, env.port = sock:getsockname()
  return env
end

local init_env = function(env)
  if not env.listening then
    env.output = setmetatable({}, {
      __newindex = function(_, prefix, val)
        if val then
          local line = prefix == 'raw' and val or _P_[prefix](val)
          env.response = env.response .. line .. '\r\n'
        end
      end
    })
    env.format_time = require'snippets'.format_datetime_whois
    env.dbm = require 'epp_db_management'
    setmetatable(env, { __index = env.dbm.tables }) -- shortcut
    env.c_get, env.c_put, env.c_objs, env.c_ids
      = memoizer_maker(env.epp_contact, 'id_c')
    env.m_get, env.m_put, env.m_objs, env.m_ids
      = memoizer_maker(env.epp_maintainer, 'id_m')
  end
  return env
end

local WHOIS_MAX_REQ_LEN = WHOIS_MAX_REQ_LEN
local recv_data = function(env)
  local data
  local pcre_match = require'rex_pcre'.match

  while true do
    data = (data or '') .. yield(true)
    local crlf_pos = data:find'[\r\n]'
    if crlf_pos then
      local obj_id = data:sub(1, -1 + crlf_pos)
      env.response = WHOIS_HEADER:format(obj_id)
      if pcre_match(obj_id, DOMAIN_MATCHER) then
        pcall(add_domain_response, env, obj_id)
      else
        pcall(add_contact_response, env, obj_id)
      end
      env.response_ready = true
      break
    elseif data and #data > WHOIS_MAX_REQ_LEN then
      break
    end
  end
end

local data_ready = function(env)
  while true do
    local response = env.response
    yield(env.response_ready and 0 < #response and response)
  end
end

local data_sent = function(env)
  while true do
    local sent_bytes = yield(true) or 0
    env.response = env.response:sub(1+sent_bytes)
    if 0 == #env.response then
      env.response_ready = false
      env.sock:shutdown() -- just shutdown, will be closed by server code
      break
    end
  end
end

local simple_shutdown = function(env)
  yield(true)
end

local listener_error = function(env)
  while true do
    print("WHOIS LISTENING socket ERROR:", yield(true)) -- FIXME TODO HERE
  end
end

local acceptor_error = function(env)
  while true do
    print("WHOIS CONNECTED socket ERROR:", yield(true)) --- FIXME
  end
end

return {
  init_env  = init_env,
  recv_data = recv_data,
  shutdown_close = simple_shutdown,
  accept_connect = accept_connect,
  data_ready = data_ready,
  data_prepare = data_ready,
  data_sent = data_sent,
  acceptor_error = acceptor_error,
  listener_error = listener_error,
}

