--[=[--

-- example server data:
local servers = {
  default = {
  }
  EPP = {
    port = 700, ip = "*", family = 'inet', type = 'stream'
    timeout = 0.5,
    idle_timeout = 24*3200, -- 24h
    listener_callbacks = {
      on_connect = <callable which returns on bind callback method for listening socket>,
      on_data_in = <callable for on accept>,
      on_error = <etc...>,
      on_shutdown = 
      on_tick = ...
    },
    connection_callbacks = {
      on_connect = <>,
      on_data_recv = <callable which returns on data receive callback for client socket>,
      on_data_prepare = <callable fo sending bytes, see below comments for details>,
      is_data_ready = < should return true if need to send data >
      on_error = <etc...>,
      on_shutdown = 
    },
  },

  whois = {
  },
}
--]=]--

local function try_coroutine_resume(coroutine, ...)
  -- TODO FIXME some dead coroutine logging or so...
end

local ERR = {
  SOCK_POLL    = "socket polling HUP or ERR error",
  SOCK_ACCEPT  = "socket accept error",
  SOCK_SETUP   = "setup_socket method error",
  SOCK_RECV    = "socket recv error",
  SOCK_SEND    = "socket send error",
  ON_ACCEPT    = "on_accept callback error",
  ON_DATA_RECV = "on_data_recv callback error",
  ON_DATA_PREP = "on_data_prepare callback error",
}
setmetatable(ERR, { -- protect to avoid debugging of typos
  __index = function(t, idx) error('ERR no such index'..idx) end
})

local nixio = require 'nixio'
local ssl   = require 'ssl'
local posix = require 'posix'

local snippets = require 'snippets'
local generate_uid = snippets.generate_uid
local is_integer = snippets.is_integer
local type = type
local table = table
local table_remove, table_insert = table.remove, table.insert
local unpack = unpack
local rawset, rawget, setmetatable = rawset, rawget, setmetatable
local getfenv, setfenv = getfenv, setfenv
local coroutine = coroutine
local coroutine_resume, coroutine_create, coroutine_yield = coroutine.resume, coroutine.create, coroutine.yield
local coroutine_resume = function(...) local s, a, b, c, d, e, f = coroutine_resume(...) if not s then print("++++", s, a, b, c, d, e, f) os.exit() end return s, a, b, c, d, e, f end
local pairs, select = pairs, select
local print = print local pcall = pcall -- DEBUG

module(..., package.seeall)

local BACKLOG_DEFAULT = 42
local MAX_RECV_BUF_DEFAULT = 8192
local POLLING_TIMEOUT_DEFAULT = 1000 -- milliseconds
-- functional-styled (closures)
function make_servers_pool(opts)
  local pool = {}
 
  local servers, sockets = {}, {}
  local poll_sockets, socket_coroutines = {}, {}
  local polling_timeout = opts and opts.polling_timeout or POLLING_TIMEOUT_DEFAULT

  local socket_get_fd = function(sock)
    local sock_fd_method = sock.fileno or sock.getfd
    return sock_fd_method(sock)
  end

  local __index_socket_fd = function(tbl, sock) -- another shorthand for convenience
    local fd = 'userdata' == type(sock) and socket_get_fd(sock) or sock
    return rawget(tbl, fd)
  end

  local __newindex_socket_fd_gen = function(action_if_userdata)
    local rawset_action = rawset

    action_if_userdata = action_if_userdata or rawset_action
    return function(tbl, sock_or_fd, what)
      if 'userdata' == type(sock_or_fd) then
        local fd = socket_get_fd(sock_or_fd)
        if fd then
          action_if_userdata(tbl, fd, what)
        else
          -- TODO internal log err (shall not happen) OR TODO action_if_no_fd ??
        end
      else
        rawset(tbl, sock_or_fd, what)
      end
    end
  end

  local simple_sock_fd_mt = { __index = __index_socket_fd, __newindex = __newindex_socket_fd_gen() }
  setmetatable(socket_coroutines, simple_sock_fd_mt)
  setmetatable(sockets, simple_sock_fd_mt)

  --[[
   this metatable to allow syntactically neat set/get polling flags
   which lives in different tables events and revents (which is also
   error prone to typos/inadvertence btw), i.e.:
       poll_sockets[sock].events.IN = true
   becomes
       poll_sockets[sock].IN = true
   and
       if poll_sockets[sock].revents.IN then ... end
   becomes
       if poll_sockets[sock].IN then ... end
   so that getters will read from table revents (returned) flags
   and setters set in table events (requested) flags

   also the index will recognize sockets and automatically get the
   file descriptor index to be even shorter, so
       poll_sockets[sock:fileno()]
   becomes
       poll_sockets[sock]
   (as written above in getters/setters examples)
   
  -- (c) Captain NonObvious :) --]]
  setmetatable(poll_sockets, {
    __newindex = __newindex_socket_fd_gen(
      function(tbl, fd, to_poll_or_not_to_poll)
        if to_poll_or_not_to_poll then
          local events, revents = {}, {}
          local short_accessor = setmetatable(
            {     events = events, revents = revents },
            { __newindex = events, __index = revents }
          )
          rawset(tbl, fd, short_accessor)
        else
          rawset(tbl, fd, nil)
        end
      end
    ),
    __index = __index_socket_fd,
  })

  local reset_poll_revents = function()
    for _,polling_data in pairs(poll_sockets) do
      local revents = polling_data.revents
      for key,_ in pairs(revents) do
        revents[key] = nil
      end
    end
  end

  local try_wrap_ssl = function(sock, listening, tls_options)
    local msg
    if not listening and tls_options then
      sock, msg = ssl.wrap(sock, tls_options)
      sock:settimeout(0)
    end
    return sock, msg
  end

  local wipe_socket = function(sock)
    local sock_env = sockets[sock]
    if sock_env then
      sock = sock_env.sock -- HERE
      coroutine_resume(sock_env.on_shutdown)
      poll_sockets[sock] = false
      socket_coroutines[sock], sockets[sock] = nil, nil
      return sock.shutdown and sock:shutdown(), sock:close()
    end
  end

  local setup_socket -- declared as used from inside of create_socket_coroutine()
  local create_socket_coroutine = function(sock, env)
    local listening, tls_options, name = env.listening, env.options.tls_options, env.name
    local on_accept, is_data_ready, on_data_prepare, on_data_send, on_data_recv =
          env.on_accept, env.is_data_ready, env.on_data_prepare, env.on_data_send, env.on_data_recv
    local poll = poll_sockets[sock]

    local current_operation
    local check_socket_error = function(status)
      return 'closed' == status or
              poll.HUP or poll.ERR or poll.NVAL or
              (sock.getopt and 0 ~= sock:getopt('socket', 'error'))
    end

    local process_error = function()
      wipe_socket(sock)
      return false, 'closed'
    end

    if listening then
      return coroutine_create(function()
        poll.IN, poll.OUT = true, false
        while true do
          local status, hint = coroutine_yield(true, 'ok')
          if status and poll.IN then
            local status
            local csock, ip, port = sock:accept()
            if csock then
              status, csock = coroutine_resume(on_accept, csock, ip, port)
              if status and csock then
                csock, status = setup_socket(name, csock, generate_uid(ip))
                -- TODO FIXME error check and logging
              end
            end
          end
          if check_socket_error(hint) then
            return process_error()
          end
        end
      end)
    else
      local max_recv_buf = env.options.max_recv_buf or MAX_RECV_BUF_DEFAULT
      local function set_poll_by_want(want)
        local IN, OUT = 'wantread' == want, 'wantwrite' == want
        if IN or OUT then
          poll.IN, poll.OUT = IN, OUT
          return true
        end
        return 'timeout' == want -- error if not want* and not timeout
      end
      local function set_poll_by_current_op(op, want)
        if want then
          poll.IN, poll.OUT = true, true
          return true
        end
        local need_write = 'send' == op
        poll.IN, poll.OUT = not need_write, need_write
        return true
      end
      local function check_data_ready()
        local status, is_ready = coroutine_resume(is_data_ready)
        return status and is_ready and 'send' or 'recv'
      end
      local function send_data()
        local status_c, data, size = coroutine_resume(on_data_prepare)
        local want, sent, sent2 -- want is all about SSL 'wantread/wantwrite'
        size = size or 0
        if status_c and data then
          sent, want, sent2 = sock:send(data)
          sent = sent or sent2 or 0
          if sent > 0 then
            status_c = coroutine_resume(on_data_send, sent) -- TODO FIXME check coroutine error
          end
        end
        return status_c, want, sent
      end
      local recv_method = sock.recv or sock.receive
      local function recv_data()
        local data, want, data2 = recv_method(sock, max_recv_buf)
        data = data or data2 or ""
        local recvd = #data
        if 0 ~= recvd then
          status_c = coroutine_resume(on_data_recv, data) -- TODO FIXME check coroutine errors
        end
        return status_c, want, recvd
      end
      local function tls_handshake()
        local rv = not tls_options -- handshake will be always true for non TLS/SSL
        poll.OUT = true
        while tls_options do
          local status, hint = coroutine_yield(true, 'ok')
          if not status then break end -- signalled for exit

          local done, want = sock:dohandshake()
          if done then rv = true break end
          if not set_poll_by_want(want) then break end -- error
        end

        return rv
      end


      return coroutine_create(function()
        if not tls_handshake() then
          return process_error()
        end

        local want, current_op, status_c
        while true do
          current_op = check_data_ready()
          set_poll_by_current_op(current_op, want)
          local status, hint = coroutine_yield(true, 'ok')
          if status then
            if poll.IN or poll.OUT then
              if 'send' == current_op then
                status_c, want = send_data() -- todo fixme check status_c
              else
                status_c, want = recv_data() -- todo fixme check status_c
              end
        
              if want ~= 'closed' and ('string' ~= type(want) or 'want' ~= want:sub(1,4)) then -- only wantread wantwrite SSL stuff need retry
                current_op, want = nil, nil
              end
            end
          end
          if check_socket_error(want) then
            return process_error()
          end
        end
      end)
    end
  end

  -- local setup_socket declared above
  setup_socket = function(name, sock, uuid, listening)
    sock:setblocking(false) -- we need it before call to try_wrap_ssl

    local srv_options = servers[name]
    local sock, msg = try_wrap_ssl(sock, listening, srv_options.tls_options)

    local options = setmetatable({}, { __index = srv_options }) -- protect from modify
    local env

    if sock then
      env = {
        name       = name,
        sock       = sock,
        session_id = uuid,
        options    = options,
        listening  = listening,
      }
  
      srv_options.generic_callbacks.on_init_environment(env)
  
      local callbacks = listening and srv_options.listener_callbacks
                                  or srv_options.connection_callbacks
  
      for coroutine_name, coroutine_func in pairs(callbacks) do
        local corout = coroutine_create(coroutine_func)
        env[coroutine_name] = corout
        coroutine_resume(corout, env) -- shared env passed to all coroutines
      end

      poll_sockets[sock] = true
      sockets[sock] = env
      socket_coroutines[sock] = create_socket_coroutine(sock, env)
    else -- SSL not wrapped ok
      env.options.generic_callbacks.on_general_failure(msg)
      sock:close()
    end

    return sock, msg or 'ok' or 'good' or 'fine' or 'nice' -- anyway...
  end

  local function pool_poll_sockets()
    reset_poll_revents()
    posix.poll(poll_sockets, polling_timeout) 
  end

  pool.cycle_once = function()
    pool_poll_sockets()
    for sock, corout in pairs(socket_coroutines) do
      local c_status, status, hint = coroutine_resume(corout, true)
      if hint == 'closed' or not c_status or not status then -- closed, error or dead coroutine
        local sock_obj = sockets[sock]
        if sock_obj then
          wipe_socket(sock_obj.sock)
        end
      end
    end
  end

  pool.cycle_forever = function(do_need_break)
    while coroutine_resume(do_need_break, pool) do
      pool.cycle_once()
    end
  end

  pool.spawn_server = function(name, options)
    options.name = name
    servers[name] = options

    local sock, err = nixio.socket(
      options.family or 'inet', options.type or 'stream'
    )
    local _status

    if sock then -- no default for port because UDS sockets doesn't have it
      _status, err = sock:bind(options.ip or '0.0.0.0', options.port)
      sock:setopt('socket', 'reuseaddr', 1)
      -- TODO FIXME UDS anybody?
      if _status then
        _status, err = sock:listen(options.backlog or BACKLOG_DEFAULT)
        if _status then
          setup_socket(name, sock, name, true)
        end
      end
    end
    return sock, err, err and options.generic_callbacks.on_general_failure(err)
  end

  return pool
end

function on_accept_gen(callback)
  return function(env)
    local sock, ip, port = true -- sock == just true for first yield
    
    repeat
      sock, ip, port = coroutine_yield(sock)
      if callback and not callback(env, sock, ip, port) then
        sock:close()
        sock = nil
      end
    until not sock
  end
end

