require 'epp_settings'

local lanes = require'lanes'.configure(EPP_LANES_CONFIG)
local linda = lanes.linda

local uuid = require 'uuid'

local posix = require 'posix'

local yield = coroutine.yield
--local logger = require 'logger'
--local logger_failure = logger'FAILURE' -- etc...

local EPP_PORT     = os.getenv"EPP_PORT" or EPP_DEFAULT_BIND_PORT
local EPP_BINDADDR = os.getenv"EPP_BINDADDR" or "*" -- 0.0.0.0
local WHOIS_PORT   = os.getenv"WHOIS_PORT" or WHOIS_DEFAULT_BIND_PORT
local WHOIS_BINDADDR = os.getenv"WHOIS_BINDADDR" or "*" -- 0.0.0.0

--local on_general_failure = function(...)
--  logger_failure(...)
--end

logger_failure = print

local sm = require 'servers_management_tls'

local esh = require 'epp_server_handlers'
local ts_xpr_mq, SRV_XPR_ADD_SESSION, SRV_XPR_DEL_SESSION =
  esh.ts_xpr_mq, esh.TS_XPR_ADD_SESSION, esh.TS_XPR_DEL_SESSION
local epp_srv = {
  port = EPP_PORT, ip=EPP_BINDADDR,
    family='inet', type='stream',
    polling_timeout = 80, -- msecs
    max_recv_buf = 65000,

  tls_options = esh.tls_options,

  generic_callbacks = {
    on_general_failure = logger_failure,
    on_init_environment = esh.init_environment,
  },

  listener_callbacks = {
    on_accept = sm.on_accept_gen(esh.ip_throttling),
    on_error = esh.listener_error,
    on_shutdown = esh.shutdown_close,
  },

  connection_callbacks = {
    on_data_recv = esh.recv_data,
    on_shutdown = esh.shutdown_close,
    is_data_ready = esh.data_ready,
    on_data_prepare = esh.data_prepare,
    on_data_send = esh.data_sent,
    on_error = esh.acceptor_error,
  },
}

local wsh = require'whois_server_handlers'
local whois_srv = {
  port = WHOIS_PORT, ip=WHOIS_BINDADDR,
    family='inet', type='stream',
    polling_timeout = 20, -- msecs
    max_recv_buf = 1024,

  generic_callbacks = {
    on_general_failure = logger_failure,
    on_init_environment = wsh.init_env,
  },

  listener_callbacks = {
    on_error = wsh.listener_error,
    on_accept = sm.on_accept_gen(wsh.accept_connect),
    on_shutdown = wsh.shutdown_close,
  },

  connection_callbacks = {
    on_data_recv = wsh.recv_data,
    on_shutdown = wsh.shutdown_close,
    is_data_ready = wsh.data_ready,
    on_data_prepare = wsh.data_prepare,
    on_data_send = wsh.data_sent,
    on_error = wsh.acceptor_error,
  },
}

local function server_tick(lnd)
  while true do yield(true) end -- TODO some linda:receive and look for the external commands to exit or so
end

function server_thread(srv_name, srv_options)
  local sm = require 'servers_management_tls'
  local pool = sm.make_servers_pool{ polling_timeout = srv_options.polling_timeout }
  pool.spawn_server(srv_name, srv_options)
  pool.cycle_forever(coroutine.create(server_tick, server_linda))
end

function xml_processor_thread(lnd)
  local rp = require 'requests_processor'
  local process_epp_xml = rp.process_epp_xml
  local listen_keys = { SRV_XPR_ADD_SESSION, SRV_XPR_DEL_SESSION }

  while true do
    local key, data = lnd:receive(nil, unpack(listen_keys))
    if key == SRV_XPR_ADD_SESSION then
      local idx = 1 + #listen_keys
      listen_keys[idx] = data
      listen_keys[data] = idx
    elseif key == SRV_XPR_DEL_SESSION then
      local idx = listen_keys[data]
      if idx ~= #listen_keys then -- not on top, so keep ungaped
        listen_keys[idx] = listen_keys[#listen_keys]
        listen_keys[listen_keys[idx]] = idx
      end
      listen_keys[#listen_keys], listen_keys[data] = nil, nil
    else
      local response = process_epp_xml(key, data)
      ts_xpr_mq:send(nil, key.."-R", response.res_xml)
    end
  end
end

function main_thread()
  local SRV = lanes.gen("*", server_thread)
  local XPR = lanes.gen("*", xml_processor_thread)

  local EPP_h = SRV("EPP_INUA", epp_srv)
  local WHOIS_h = SRV("WHOIS", whois_srv)
  local XPR_h = XPR(ts_xpr_mq)

  while posix.sleep(3) do
    print(EPP_h.status, XPR_h.status, WHOIS_h.status)
 
    if EPP_h.status == 'error' then print(EPP_h[1]) end
    if XPR_h.status == 'error' then print(XPR_h[1]) end
    if WHOIS_h.status == 'error' then print(WHOIS_h[1]) end
  end
end

main_thread()

