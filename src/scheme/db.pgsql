--CREATE DATABASE lua_epp;

-- enum types for efficiency and readability
CREATE TYPE T_INFOTYPE AS ENUM('int', 'loc', 'ext');

CREATE TYPE T_C_STATUS AS ENUM(
   'clientDeleteProhibited', 'serverDeleteProhibited',
   'clientUpdateProhibited', 'serverUpdateProhibited',
   'clientTransferProhibited', 'serverTransferProhibited',
   'linked', 'ok',
   'pendingCreate', 'pendingDelete', 'pendingTransfer', 'pendingUpdate'
);

CREATE TYPE T_C_DISCLOSE AS ENUM(
   'email', 'voice', 'fax'
);

CREATE TYPE T_CPI_DISCLOSE AS ENUM(
   'name', 'org', 'addr'
);

CREATE TYPE T_D_STATUS AS ENUM(
   'clientDeleteProhibited', 'serverDeleteProhibited',
   'clientUpdateProhibited', 'serverUpdateProhibited',
   'clientRenewProhibited', 'serverRenewProhibited',
   'clientTransferProhibited', 'serverTransferProhibited',
   'clientHold', 'serverHold', 'inactive',
   'pendingCreate', 'pendingDelete', 'pendingTransfer', 'pendingUpdate',
   'autoRenewGracePeriod', 'redemptionPeriod',
   'ok'
);

CREATE TYPE T_DT_STATUS AS ENUM(
   'pending', 'serverCancelled', 'serverApproved',
   'clientCancelled', 'clientApproved', 'clientRejected'
);

CREATE TYPE T_DC_REL_TYPE AS ENUM('admin', 'tech'); -- domain-to-contact relation type

CREATE TYPE T_H_STATUS AS ENUM(
   'clientDeleteProhibited', 'serverDeleteProhibited',
   'clientUpdateProhibited', 'serverUpdateProhibited',
   'linked', 'ok',
   'pendingCreate', 'pendingDelete', 'pendingTransfer', 'pendingUpdate'
);

CREATE TYPE T_P_OPTYPE AS ENUM(
  'create', 'renew', 'restore', 'transfer_renew'
);

CREATE TYPE T_M_STATUS AS ENUM(
   'active', 'inactive',
   'blocked_paid_ops', 'blocked_all_ops', 'open_redemption'
);

CREATE TABLE epp_maintainer(
   id_m                   SERIAL PRIMARY KEY,

   m_id                   CHARACTER VARYING(40) NOT NULL,
   m_organization         CHARACTER VARYING(255) NOT NULL DEFAULT 'FIXME',
   m_organization_loc     CHARACTER VARYING(255),
   m_url                  CHARACTER VARYING(255),
   m_city                 CHARACTER VARYING(255),
   m_country              CHARACTER (2) NOT NULL DEFAULT 'UA',
   m_authInfo             CHARACTER VARYING (160) NOT NULL,
   m_clID_m               INTEGER REFERENCES epp_maintainer(id_m) ON DELETE RESTRICT,
   m_crID_m               INTEGER REFERENCES epp_maintainer(id_m) ON DELETE SET NULL,
   m_upID_m               INTEGER REFERENCES epp_maintainer(id_m) ON DELETE SET NULL,
   m_crDate               TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   m_upDate               TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   m_trDate               TIMESTAMP DEFAULT NULL,
   m_statusi              T_M_STATUS[7] NOT NULL DEFAULT '{active}',
--   m_epp_settings         TEXT NOT NULL DEFAULT '', -- NOT YET IMPLEMENTED
--   m_other_settings       TEXT NOT NULL DEFAULT '', -- NOT YET IMPLEMENTED

   UNIQUE(m_id),
   CHECK (m_id SIMILAR TO '[a-z][a-z0-9\-\.]{1,39}')
);
CREATE INDEX m_clid_m_idx ON epp_maintainer(m_clid_m);
CREATE INDEX m_crid_m_idx ON epp_maintainer(m_crid_m);
CREATE INDEX m_upid_m_idx ON epp_maintainer(m_upid_m);

CREATE TABLE epp_contact(
   id_c              SERIAL PRIMARY KEY,
   c_roid            CHARACTER VARYING (90) NOT NULL,
   c_id              CHARACTER VARYING (40) NOT NULL,  -- contact-id:
   c_authInfo        CHARACTER VARYING (80) NOT NULL,
   c_statusi         T_C_STATUS[11] NOT NULL DEFAULT '{ok}',
   c_disclose        T_C_DISCLOSE[3] NOT NULL DEFAULT '{}',
   c_disclose_int    T_CPI_DISCLOSE[3] NOT NULL DEFAULT '{name}',
   c_disclose_loc    T_CPI_DISCLOSE[3] NOT NULL DEFAULT '{name}',
   c_email           CHARACTER VARYING (240) NOT NULL,
   c_voice           CHARACTER VARYING (18), -- +380.441234567
   c_fax             CHARACTER VARYING (18),
   c_clID_m          INTEGER NOT NULL REFERENCES epp_maintainer(id_m) ON DELETE RESTRICT,
   c_crID_m          INTEGER NOT NULL DEFAULT 1 REFERENCES epp_maintainer(id_m) ON DELETE SET DEFAULT,
   c_upID_m          INTEGER REFERENCES epp_maintainer(id_m) ON DELETE SET NULL,
   c_crDate          TIMESTAMP NOT NULL,
   c_upDate          TIMESTAMP DEFAULT NULL,
   c_trDate          TIMESTAMP DEFAULT NULL,

   UNIQUE (c_id),
   UNIQUE (c_roid),
   CHECK (c_id SIMILAR TO '[a-z][a-z0-9\-\.]{2,39}'),
   CHECK (c_roid SIMILAR TO '(\w|_){1,80}\-\w{1,8}'),
   CHECK (c_authInfo SIMILAR TO '[A-Za-z0-9~!@#$%_=:;?,.\-+]{1,80}'),
   CHECK (c_email SIMILAR TO
     '[a-z0-9_.\-!#$%*+=~]+@([a-z0-9]+([.\-][a-z0-9]+)*)+.[a-z]{2,6}'),
   CHECK (c_voice SIMILAR TO '\+[\d\.]+')
);
CREATE INDEX c_clid_m_idx ON epp_contact(c_clid_m);
CREATE INDEX c_crid_m_idx ON epp_contact(c_crid_m);
CREATE INDEX c_upid_m_idx ON epp_contact(c_upid_m);

CREATE TABLE epp_contact_postal_info(
   id_cpi               SERIAL PRIMARY KEY,
   cpi_contact_c        INTEGER REFERENCES epp_contact(id_c) ON DELETE CASCADE,
   cpi_type             T_INFOTYPE NOT NULL DEFAULT 'int',
   cpi_name             CHARACTER VARYING (512) NOT NULL, -- person:
   cpi_org              CHARACTER VARYING (512),  -- organization:
   cpi_street           CHARACTER VARYING (1024), -- address:
   cpi_sp               CHARACTER VARYING (255),  -- address:
   cpi_city             CHARACTER VARYING (255) NOT NULL,  -- address:
   cpi_pc               CHARACTER VARYING (16),   -- postal-code:
   cpi_cc               CHARACTER (2) NOT NULL DEFAULT 'UA',    -- country:

   CHECK (cpi_cc SIMILAR TO '\w{2}'),
   CHECK (cpi_pc SIMILAR TO '\d+'),
   UNIQUE (cpi_contact_c, cpi_type)
);

CREATE TABLE epp_domain(
   id_d             SERIAL PRIMARY KEY,
   d_roid           CHARACTER VARYING (90)  NOT NULL,
   d_name           CHARACTER VARYING (255) NOT NULL,
   d_authInfo       CHARACTER VARYING (80), --  NOT NULL,
   d_statusi        T_D_STATUS[11] NOT NULL DEFAULT '{ok}',
   d_registrant_c   INTEGER NOT NULL REFERENCES epp_contact(id_c) ON DELETE RESTRICT,
   d_clID_m         INTEGER NOT NULL REFERENCES epp_maintainer(id_m) ON DELETE RESTRICT,
   d_crID_m         INTEGER NOT NULL DEFAULT 1 REFERENCES epp_maintainer(id_m) ON DELETE SET DEFAULT,
   d_dlID_m         INTEGER REFERENCES epp_maintainer(id_m) ON DELETE SET NULL,
   d_upID_m         INTEGER REFERENCES epp_maintainer(id_m) ON DELETE SET NULL,
   d_crDate         TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   d_exDate         TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP + INTERVAL '1 YEAR',
   d_upDate         TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   d_trDate         TIMESTAMP DEFAULT NULL,

   UNIQUE (d_roid),
   UNIQUE (d_name),
   CHECK (d_exDate > d_crDate),
--   CHECK (d_name SIMILAR TO '[\w][\w\-]*[\w]*.in.ua'),
   CHECK (d_name NOT SIMILAR TO '%[.-].%'),
   CHECK (d_name NOT LIKE '%\_%')
);
CREATE INDEX d_clid_m_idx ON epp_domain(d_clid_m);
CREATE INDEX d_crid_m_idx ON epp_domain(d_crid_m);
CREATE INDEX d_upid_m_idx ON epp_domain(d_upid_m);
CREATE INDEX d_exdate_idx ON epp_domain(d_exdate);

CREATE TABLE epp_domain_contact_lnk(
   dc_domain_d       INTEGER NOT NULL REFERENCES epp_domain(id_d) ON DELETE CASCADE,
   dc_contact_c      INTEGER NOT NULL REFERENCES epp_contact(id_c) ON DELETE CASCADE,
   dc_contact_type   T_DC_REL_TYPE NOT NULL DEFAULT 'admin',
   PRIMARY KEY (dc_domain_d, dc_contact_c, dc_contact_type)
);
CREATE INDEX dc_contact_c_idx ON epp_domain_contact_lnk(dc_contact_c);

CREATE TABLE epp_host(
   id_h            SERIAL PRIMARY KEY,
   h_roid          CHARACTER VARYING (90)  NOT NULL,
   h_name          CHARACTER VARYING (255) NOT NULL, -- ded.in.ua
   h_domain_d      INTEGER NULL REFERENCES epp_domain(id_d) ON DELETE CASCADE,
   h_addressi      INET[17],
   h_statusi       T_H_STATUS[11] NOT NULL DEFAULT '{ok}',
   h_clID_m        INTEGER NOT NULL REFERENCES epp_maintainer(id_m) ON DELETE RESTRICT,
   h_crID_m        INTEGER NOT NULL DEFAULT 1 REFERENCES epp_maintainer(id_m) ON DELETE SET DEFAULT,
   h_upID_m        INTEGER REFERENCES epp_maintainer(id_m) ON DELETE SET NULL,
   h_crDate        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   h_upDate        TIMESTAMP DEFAULT NULL,
   h_trDate        TIMESTAMP DEFAULT NULL,

   UNIQUE (h_roid),
   UNIQUE (h_name),

   CHECK (h_name SIMILAR TO '([\w][\w\-]*[\w]*\.)*[\w][\w\-]*[\w]+'),
   CHECK (h_name NOT SIMILAR TO '%[.-].%'),
   CHECK (h_name NOT LIKE '%\_%')
);
CREATE INDEX h_clid_m_idx ON epp_host(h_clid_m);
CREATE INDEX h_crid_m_idx ON epp_host(h_crid_m);
CREATE INDEX h_upid_m_idx ON epp_host(h_upid_m);

CREATE TABLE epp_domain_ns_lnk(
   dn_domain_d       INTEGER NOT NULL REFERENCES epp_domain(id_d) ON DELETE CASCADE,
   dn_host_h         INTEGER NOT NULL REFERENCES epp_host(id_h) ON DELETE CASCADE,
   PRIMARY KEY (dn_domain_d, dn_host_h)
);
CREATE INDEX dn_host_h_idx ON epp_domain_ns_lnk(dn_host_h);

CREATE TABLE epp_domain_transfer(
   id_dt             SERIAL PRIMARY KEY,
   dt_domain_d       INTEGER NOT NULL REFERENCES epp_domain(id_d) ON DELETE CASCADE,
   dt_reID_m         INTEGER NOT NULL REFERENCES epp_maintainer(id_m) ON DELETE CASCADE,
   dt_reDate         TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   dt_acID_m         INTEGER NOT NULL REFERENCES epp_maintainer(id_m) ON DELETE CASCADE,
   dt_acDate         TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP + INTERVAL '1 WEEK',
   dt_exDate         TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP + INTERVAL '1 YEAR',
   dt_status         T_DT_STATUS NOT NULL DEFAULT 'pending'
);
CREATE INDEX dt_domain_d_idx ON epp_domain_transfer(dt_domain_d);
CREATE INDEX dt_reid_m_idx ON epp_domain_transfer(dt_reid_m);
CREATE INDEX dt_acid_m_idx ON epp_domain_transfer(dt_acid_m);
CREATE INDEX dt_acdate_idx ON epp_domain_transfer(dt_acdate);
CREATE INDEX dt_status_idx ON epp_domain_transfer(dt_status);

CREATE TABLE epp_message_queue(
   id_q             BIGSERIAL PRIMARY KEY, -- if we get million per second - ~300000 years to consume
   q_addressee_m    INTEGER NOT NULL REFERENCES epp_maintainer(id_m) ON DELETE CASCADE,
   q_msg            CHARACTER VARYING (255) NOT NULL DEFAULT 'UNKNOWN MESSAGE',
   q_queued_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   q_remove_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP + INTERVAL '1 MONTH',
   q_resData        TEXT -- here reside pretty-printed xml-as-tables Lua objects
);
CREATE INDEX q_addressee_m_idx ON epp_message_queue(q_addressee_m);
CREATE INDEX q_remove_at_idx ON epp_message_queue(q_remove_at);

CREATE TABLE epp_payments(
  id_p              SERIAL PRIMARY KEY,
  p_clid_m          INTEGER NOT NULL REFERENCES epp_maintainer(id_m) ON DELETE CASCADE,
  p_dlid_m          INTEGER NULL REFERENCES epp_maintainer(id_m) ON DELETE SET NULL,
  p_opdate          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  p_optype          T_P_OPTYPE NOT NULL DEFAULT 'renew',
  p_amount          NUMERIC(3,1) NOT NULL DEFAULT 1, -- billing made in units, actual value of unit is out of this scope, i assume that precision of 3 with decimal fraction should be enough
  p_object          CHARACTER VARYING (255) NOT NULL -- domain name will be here, don't use foreign table references so no ON DELETE stuff to deal with
);
CREATE INDEX p_clid_m_dlid_m_isx ON epp_payments(p_clid_m, p_dlid_m);
CREATE INDEX p_opdate_idx ON epp_payments(p_opdate);


