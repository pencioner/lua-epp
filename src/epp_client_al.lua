local sn = require 'snippets'
local make_epp_message = sn.make_epp_message

EPP_CLIENT_TIMEO = 300 -- more than enough...

local insert_query = function(env, tpl)
  tpl = tpl or env.tpl
  if tpl then
    env.outbox[1+#env.outbox] = tpl(env)
  end
end


local function receive_epp_chunk(clnt)
  local sock = clnt.sock
  local recv_method = sock.receive or sock.recv
  local data, msg, data2
  data, msg = sock:receive(4)
  if data then
    local length = bitstring.unpack("32:int", data)
    data, msg, data2 = sock:receive(length)
  elseif clnt.raise_error then
    error(msg)
  end
  return data, msg
end

local function send_xml_recv_response(clnt, xml)
  local message = make_epp_message(tostring(xml))
  local sock = clnt.sock

  local sent, msg, sent2 = sock:send(message)
  if sent then
    return receive_epp_chunk(clnt)
  elseif clnt.raise_error then
    error(msg)
  end
  return nil, msg
end

local function connect(self)
  local nixio, posix, ssl = require'nixio', require'posix', require'ssl'
  local ctx, status, msg

  if self.sock then self:shutdown() end
  self.sock, msg = nixio.socket('inet', 'stream')
  if self.sock then
    status, msg = self.sock:connect(self.ip, self.port)
    if status then
      self.sock:setblocking(true) -- TODO think about non-blocking code, not simplify
      if self.tls_opts then
        self.sock, msg = ssl.wrap(self.sock, self.tls_opts)
        if self.sock then
          self.sock:settimeout(EPP_CLIENT_TIMEO)
        end
      else
        rawset(getmetatable(self.sock).__index, 'receive', self.sock.recv) -- alias
      end
      print(receive_epp_chunk(self))
    else
      self:shutdown()
    end
  end
if msg then print('msg', msg) end
  if self.raise_errors and not self.sock then error(msg) end
  return self.sock, msg
end

local function shutdown(self)
  self.sock:shutdown()
  self.sock:close()
  self.sock = nil
end

local function process_cmd(self, cmd)
  cmd.clTRID = os.time() -- TODO FIXME real one HERE
  local cmd_xml = cmd[1](cmd)
  local response = send_xml_recv_response(self, cmd_xml)
  if response then
  else
    -- TODO client transport error? life like that...
  end
  return response, cmd_xml
end

local tpl = require'epp_query_templates'
local client_instance_mt = {
  connect = connect,
  shutdown = shutdown,
  process_cmd = process_cmd,
  __gc = shutdown,
  __call = process_cmd,
  __index = function(clnt, idx)
    if "do_" == idx:sub(1,3) then
      local tpl = tpl[idx:sub(4)]
      return function(clnt, args)
        return clnt:process_cmd(table.array_merge(args, { tpl }))
      end
    end
  end,
}
client_instance_mt.__index = client_instance_mt

function create_client_instance(ip, port, tls_opts, raise_errors)
  return setmetatable({
    ip = ip, port = port, tls_opts = tls_opts, raise_errors = raise_errors
  }, client_instance_mt)
end

return {
  create_client_instance = create_client_instance,
}

