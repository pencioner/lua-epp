local pairs, ipairs, unpack = pairs, ipairs, unpack
local type, pcall = type, pcall
local setmetatable = setmetatable -- used in domain:create hostAttr code
local tostring = tostring

local table_remove = table.remove

local RES = require 'epp_response_codes'
local OK                      = RES.OK
local LOGGED_OUT              = RES.LOGGED_OUT
local PENDING                 = RES.PENDING
local EMPTY_Q                 = RES.EMPTY_Q
local E_USE                   = RES.E_USE
local E_SYNTAX                = RES.E_SYNTAX
local E_FAILED                = RES.E_FAILED
local E_EXISTS                = RES.E_EXISTS
local E_NOT_EXISTS            = RES.E_NOT_EXISTS
local E_PENDING               = RES.E_PENDING
local E_NOT_PENDING           = RES.E_NOT_PENDING
local E_REJECTED              = RES.E_REJECTED
local E_PARAM_RANGE           = RES.E_PARAM_RANGE
local E_PARAM_VALUE           = RES.E_PARAM_VALUE
local E_PARAM_POLICY          = RES.E_PARAM_POLICY
local E_PARAM_MISSING         = RES.E_PARAM_MISSING
local E_ASSOC_PROHIBITE       = RES.E_ASSOC_PROHIBITE
local E_STATUS_PROHIBITE      = RES.E_STATUS_PROHIBITE
local E_AUTHORIZATION         = RES.E_AUTHORIZATION
local E_AUTHENTICATION        = RES.E_AUTHENTICATION
local E_AUTHINFO              = RES.E_AUTHINFO
local E_TRANSF_NOT_ELIGIBLE   = RES.E_TRANSF_NOT_ELIGIBLE
local E_RENEW_NOT_ELIGIBLE    = RES.E_RENEW_NOT_ELIGIBLE
local E_SERVICE_UNIMPLEMENTED = RES.E_SERVICE_UNIMPLEMENTED

require 'epp_settings'

local DB = require 'db_management'
local db_connection       = DB.db_connection
local epp_maintainer      = DB.tables.epp_maintainer
local epp_domain_contact  = DB.tables.epp_domain_contact
local epp_mnt_contact     = DB.tables.epp_mnt_contact
local epp_contact         = DB.tables.epp_contact
local epp_cpi             = DB.tables.epp_cpi
local epp_host            = DB.tables.epp_host
local epp_domain          = DB.tables.epp_domain
local epp_domain_ns       = DB.tables.epp_domain_ns
local epp_domain_transfer = DB.tables.epp_domain_transfer
local epp_message_queue   = DB.tables.epp_message_queue
-- helpers
local dhlp = DB.helpers
local db_array_split        = dhlp.db_array_split
local db_array_merge        = dhlp.db_array_merge
local db_array_add_remove   = dhlp.db_array_add_remove
local mnt_name              = dhlp.get_maintainer_name
local get_admin_id_m        = dhlp.get_admin_id_m
local remove_host_status    = dhlp.remove_host_status
local remove_domain_status  = dhlp.remove_domain_status
local remove_contact_status = dhlp.remove_contact_status
local add_domain_status     = dhlp.add_domain_status
local add_host_status       = dhlp.add_host_status
local add_contact_status    = dhlp.add_contact_status
local remove_str_status     = dhlp.remove_str_status
local add_str_status        = dhlp.add_str_status
local check_set_ok_status   = dhlp.check_set_ok_str_status
local check_status_present  = dhlp.check_status_present
local check_status_absent   = dhlp.check_status_absent
local update_host           = dhlp.update_host
local update_domain         = dhlp.update_domain
local update_contact        = dhlp.update_contact
local update_maintainer     = dhlp.update_maintainer
local update_domtransfer    = dhlp.update_domtransfer

local get_mntnr_ids = function(mnt_obj) -- TODO how'bout move it to helpers
  local owner_id = mnt_obj.id_m
  local registrar_id = mnt_obj.m_clid_m
  local dealer_id
  if registrar_id == get_admin_id_m() then -- not a dealer
    registrar_id = owner_id
  else
    dealer_id = owner_id
  end

  return owner_id, registrar_id, dealer_id
end

local db_push = function(db_ok)
  if db_ok and db_connection:commit() then
    return true
  end
  db_connection:rollback() -- TODO think about keepalive/ping
  return false
end

local bit = require 'bit'
local bit_or, bit_and, bit_rshift = bit.bor, bit.band, bit.rshift

local partial_one, partial_twix do
  local partial = require 'partial'
  partial_one, partial_twix = partial.make_partial_1, partial.make_partial_2
end

local typesng = require 'typesng'
local is_string = typesng.is_string
local isnot_string = typesng.isnot_string

local sn = require 'snippets'
local table_hash_slice     = table.hash_slice
local table_hash_merge     = table.hash_merge
local table_hash_wipe      = table.hash_wipe
local table_hash_len       = table.hash_len
local table_array_merge    = table.array_merge
local table_set_complement = table.set_complement
local table_topsy_turvy    = table.topsy_turvy
local is_integer = sn.is_integer -- heh lol - is_string in typesng and is_integer is in snippets. FIXME should be together?
local unpack_time, check_ip, pack_ip, unpack_ip = table_hash_slice(
  sn, 'unpack_time', 'check_ip', 'pack_ip', 'unpack_ip'
)
local fmt_time, date_add_years, date_add_days = table_hash_slice(
  sn, 'fmt_time', 'date_add_years', 'date_add_days'
)
local gen_h_roid, gen_d_roid, gen_c_roid = table_hash_slice(
  sn, 'generate_roid_host', 'generate_roid_domain', 'generate_roid_contact'
)
local cltrid_new, generate_auth = table_hash_slice(
  sn, 'generate_cltrid', 'generate_auth'
)

local date = require 'date'
local xml = require 'luaxml'
local xml_find = xml.find
local gsub_tag = function(tag) -- TODO FIXME code dupe w/epp_query_templates
  if is_string(tag) then tag = tag:gsub('__', ':') end
  return tag
end
xml.set_tag_filters { gsub_tag }

-- indexes in Lua table representation for XML tag name and value
local _T_N, _T_V = 1, 2

local xmliter = require 'xmliter'
local xmliter_switch = xmliter.switch

local pcre = require 'rex_pcre'

local getaddrinfo = require'nixio'.getaddrinfo

local idn_encode = require'idn'.encode

local serp = require'serpent'
local s_serialize = function(data)
  return serp.serialize(data, {})
end
local s_deserialize = function(serdata)
  local chunk = loadstring('return ' .. serdata)
  if chunk then
    local status, res_data = pcall(chunk)
    if status then return res_data end
  end
end

local function fill_entities_from_array(tag_parent, tag_name, arr, tag_mutator)
  if is_string(arr) then arr = db_array_split(arr) end
  if arr then
    for _, tag_val in pairs(arr) do
      local tag = { tag_name, tag_val }
      tag_parent[1+#tag_parent] = tag_mutator and tag_mutator(tag) or tag
    end
  end
end

local function get_subtag(tag, subtag_name, ...)
  local rv
  for i=2,#tag do
    local _tag = tag[i]
    rv = _tag and 'table' == type(_tag)
         and _tag[_T_N] == subtag_name and _tag[_T_V]
    if rv then break end
  end

  if 0 == #{...} then
    return rv
  end
 
  return get_subtag(tag, ...) -- tail call so recursion may be of any depth
end
local function get_subtag_filter(tag, filter, ...)
  local rv = get_subtag(tag, ...)
  return filter(rv)
end

local func_return_true = function() return true end
local func_empty = function() end
local func_passthru = function(v) return v end
-- filters are data verifying and mutating data filters, though
local function entity_gatherer_gen_ex(item_type, basket, name_or_arr, ...)
  local basket_multi, get_val_func, set_val_func
  local filters = {...}
  if item_type == 'array' then
    if 'table' == type(name_or_arr) then
      basket_multi = name_or_arr
    else
      basket_multi = basket[name_or_arr] or {}
      basket[name_or_arr] = basket_multi
    end
    set_val_func = function(val) basket_multi[1+#basket_multi] = val end
  elseif item_type == 'predicate' then
    get_val_func, set_val_func = func_return_true, func_empty
  elseif item_type == 'flag' then
    get_val_func = func_return_true
  elseif 'function' == type(item_type) then
    get_val_func = item_type
  elseif 'table' == type(item_type) then -- still not used, maybe remove if so
    get_val_func, set_val_func = unpack(item_type)
  end
  
  return function(tag)
    local tag_val = get_val_func and get_val_func(tag) or tag[_T_V]
    if not basket._error then
      for _,filter in pairs(filters) do
        local val, err, reason, err_tag = filter(
          tag_val, basket_multi or basket[name_or_arr], tag
        )
        if not val then
          basket._error, basket._reason, basket._value = 
            err, reason, err_tag or tag
          return
        end
        tag_val = val
      end
      if set_val_func then
        set_val_func(tag_val)
      else
        basket[name_or_arr] = tag_val
      end
    end
  end
end
local function entity_gatherer_gen(basket, name, ...)
  return entity_gatherer_gen_ex(nil, basket, name, ...)
end
local function entity_flag_gatherer_gen(basket, name, ...)
  return entity_gatherer_gen_ex('flag', basket, name, ...)
end
local function entity_array_gatherer_gen(basket, name, ...)
  return entity_gatherer_gen_ex('array', basket, name, ...)
end
local function entity_predicate_gatherer_gen(basket, ...)
  return entity_gatherer_gen_ex('predicate', basket, 'finally', ...)
end
local function gathered_unpack(basket, ...) -- {...} is names of entries
  local names, do_consume = {...}, false
  if 'boolean' == type(names[#names]) then
    do_consume = table_remove(names)
  end
  local rv, idx = {}, 0
  for _,name in pairs{...} do
    idx = 1 + idx
    rv[idx] = basket[name]
    if do_consume then basket[name] = nil end
  end

  return basket._error, unpack(rv, 1, idx)
end

local eg_filter_string = function(val)
  if 'table' == type(val) then
    return nil, E_SYNTAX, "XML tag found where character data expected"
  elseif not val then
    return nil, E_SYNTAX, "XML tag shouldn`t be empty"
  end
  return val
end
local eg_filter_tag = function(val)
  if 'string' == type(val) then
    return nil, E_SYNTAX, "Nested XML tag expected"
  end
  return val
end
local eg_filter_string_concat_gen = function(max_len)
  return function(val, prev_val)
    val = prev_val and val..'\n'..prev_val or val
    if #val > max_len then
      return nil, E_SYNTAX, "Data too long"
    end
    return val
  end
end
local eg_filter_single = function(val, prev_val)
  if prev_val then
    return nil, E_SYNTAX, "Duplicate XML entity"
  end
  return val
end
local eg_unexpected = function() -- used for unexpected xml tags
  return nil, E_SYNTAX, "XML inconsistency (unexpected tag)"
end
local entity_unexpected_gen = function(basket)
  return entity_gatherer_gen(basket, '_', eg_unexpected)
end
local eg_filter_empty = function(val, _, tag)
  if tag[_T_V] then
    return nil, E_SYNTAX, "XML unexpected character data"
  end
  return val
end
local eg_array_filter_finally_gen = function(array, ...)
  local min_max_chks = {...}
  local get_len_default_func = function(arr) return #arr end

  return function(finally)
    for _,chk_data in pairs(min_max_chks) do
      local min_chk, max_chk, ex_chk, len
        = chk_data.min, chk_data.max, chk_data.ex, chk_data[1]

      len = len or get_len_default_func

      local min, min_msg, min_code
      local max, max_msg, max_code
      local ex_msg, ex_code
    
      if min_chk then min, min_msg, min_code = unpack(min_chk) end
      if max_chk then max, max_msg, max_code = unpack(max_chk) end
      if ex_chk then ex_msg, ex_code = unpack(ex_chk) end

      local arr_len = len(array)

      if ex_chk and not arr_len then
        return nil, ex_code or E_SYNTAX, ex_msg
      elseif min and min > arr_len then
        return nil, min_code or E_SYNTAX, min_msg
      elseif max and max < arr_len then
        return nil, max_code or E_SYNTAX, max_msg
      end
    end
    return finally
  end
end
local eg_authinfo_filter_gen = function(what)
  local subtag_name = what .. ':pw'
  return function(val)
    if val[_T_N] == subtag_name then
      local pw = val[_T_V]
      if 'string' == type(pw) then
        return pw
      end
    end
    return nil, E_SYNTAX, "Malformed authInfo"
  end
end
local eg_update_status_gatherer_gen = function(basket, name, legal_statusi)
  return entity_array_gatherer_gen(basket, name,
    function(_,_,tag)
      local status = tag.s
      if not status then
        return nil, E_SYNTAX, "Status not given"
      elseif not legal_statusi[status] then
        return nil, E_PARAM_VALUE, "Bad status given"
      end
      return status
    end
  )
end

local check_domain_period do
  local EPP_MAX_FULL_YEARS_PERIOD = EPP_MAX_FULL_YEARS_PERIOD
  local EPP_MIN_FULL_YEARS_PERIOD = EPP_MIN_FULL_YEARS_PERIOD
  check_domain_period = function(period, _, tag)
    if tag.unit ~= 'y' then -- FIXME magic strings
      return nil, E_SYNTAX, "Period unit should be == `y` (year)"
    end
    local status, period = pcall(tonumber, period)
    if not status or not is_integer(period) then
      return nil, E_PARAM_VALUE, "Period should be integer"
    elseif period >= EPP_MAX_FULL_YEARS_PERIOD
        or period < EPP_MIN_FULL_YEARS_PERIOD then
      return nil, E_PARAM_RANGE, "Period wrong value"
    end
    return period
  end
end

local is_contact_id_correct do
  local EPP_CONTACT_MAX_ID_LEN = EPP_CONTACT_MAX_ID_LEN
  local EPP_CONTACT_MIN_ID_LEN = EPP_CONTACT_MIN_ID_LEN
  local EPP_CONTACT_REGEXP     = EPP_CONTACT_REGEXP

  is_contact_id_correct = function(cid)
    if type(cid) ~= 'string' then
      return nil, E_SYNTAX, "Malformed contact id"
    elseif EPP_CONTACT_MIN_ID_LEN < #cid
        or #cid < EPP_CONTACT_MAX_ID_LEN then
      return nil, E_PARAM_VALUE, "Contact id wrong length"
    elseif not cid:match(EPP_CONTACT_REGEXP) then
      return nil, E_PARAM_VALUE, "Contact id contains wrong characters"
    end
    return cid
  end
end
local check_contact_obj = function(checks, cid)
  local cid, msg, reason = is_contact_id_correct(cid)
  if not cid then
    return nil, msg, reason
  elseif checks then
    local contact = checks.contact or epp_contact:select{ c_id = cid }
    if checks.gimme then
      checks.contact = contact -- optimization, memoize, reducing selects
    end
    if checks.free and contact then
      return nil, E_EXISTS, "Object exists"
    elseif checks.exist and not contact then
      return nil, E_NOT_EXISTS, "Object not exists"
    elseif checks.owner and contact.c_crid_m ~= checks.owner then
      return nil, E_AUTHORIZATION, "Not owner of object"
    elseif checks.del and contact.c_statusi:find'DeleteProhibited' then -- BBW
      return nil, E_STATUS_PROHIBITE, "Delete prohibited"
    elseif checks.del and contact.c_statusi:find'linked' then
      return nil, E_ASSOC_PROHIBITE, "Contact is linked"
    -- let's not depend on status only at least until code matures
    elseif checks.del then
      local id_c = contact.id_c
      if epp_domain_contact:select{ dc_contact_c = id_c } or
         epp_domain:select{ d_registrant_c = id_c } then
        local msg
        if not add_contact_status(contact, 'linked') then
          db_push(false)
          msg = "Contact is linked, status update failed"
        else
          msg = "Contact is linked, status updated"
        end
        return nil, E_ASSOC_PROHIBITE, msg
      end
    end
  end

  return cid
end

local in_served_domain, take_domain_part do
  local EPP_DEFAULT_DOMAIN_SUFFIX = EPP_DEFAULT_DOMAIN_SUFFIX
  local len_of_domain_suffix = #EPP_DEFAULT_DOMAIN_SUFFIX
  local DOMAIN_PART_RE = '%.[^%.]+' .. EPP_DEFAULT_DOMAIN_SUFFIX_RE

  in_served_domain = function(name)
    return EPP_DEFAULT_DOMAIN_SUFFIX == name:sub(-len_of_domain_suffix)
  end
  take_domain_part = function(name)
    return name:sub(1 + (name:find(DOMAIN_PART_RE) or 0))
  end
end
local domain_is_parent_for_hosts = function(domain)
  return epp_host:select{ h_domain_d = domain.id_d }
end
local host_has_linked_domains = function(host, owner_id, is_dealer)
  return epp_domain_ns:join{
    { epp_domain, dn_domain_d = 'id_d' },
    { epp_host, dn_host_h = 'id_h' }
  }:where{
    'id_d <> h_domain_d',
    [is_dealer and 'd_dlid_m' or 'd_clid_m'] = owner_id
  }:select{ id_h = host.id_h }
end
local try_get_rid_linked = function(host, owner_id, force_delete)
  local id_h = host.id_h
  local cache = {}
  local db_ok = true
  local ns_domain_lnk = epp_domain_ns:join{{epp_domain, dn_domain_d = 'id_d'}}
                                     :find{ dn_host_h = id_h }
  if 1 > #ns_domain_lnk then return true end

  if not force_delete then
    for _, domain in pairs(ns_domain_lnk) do
      if (domain.d_dlid_m or domain.d_clid_m) == owner_id then
        return false
      end
    end
  end

  local h_name = host.h_name
  local own_domain_name = take_domain_part(h_name)
  local addressee = {}
  for _, domain in pairs(ns_domain_lnk) do
    if domain.d_name ~= own_domain_name then
      local id_m = domain.d_dlid_m or domain.d_clid_m
      local domains_broken = addressee[id_m] or {}
      domains_broken[1+#domains_broken] = domain.d_name
      addressee[id_m] = domains_broken
    end
  end
  for addressee_m, domains_broken in pairs(addressee) do
    local msg_data = { 'issue', description='lost object association',
      { 'nameserver', h_name }
    }
    for _,d_name in pairs(domains_broken) do
      msg_data[1+#msg_data] = { 'domain', d_name }
    end
    db_ok = db_ok and epp_message_queue:insert{
      q_addressee_m = addressee_m,
      q_msg         = "Host used as nameserver was deleted",
      q_resData     = s_serialize(msg_data),
    }
  end
  return db_ok and epp_domain_ns:delete{ dn_host_h = id_h }
end

local check_domains_hosts do
  local check_domain_regex = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$"
  local pcre_check_domain_re = pcre.new(check_domain_regex)

  check_domains_hosts = function(checks, name)
    local idn_name = idn_encode(name)
    if not idn_name or not pcre_check_domain_re:match(idn_name) then
      return nil, E_PARAM_VALUE, "Illegal host or domain name"
    end
  
    local obj
    local is_served = in_served_domain(idn_name)
    local free, exist, del, checks_addr, h_or_d, owner_id, dealer
      = table_hash_slice(
        checks, 'free', 'exist', 'del', 'addr', 'what', 'owner', 'dealer'
      )
    h_or_d = h_or_d or checks[1] or 'd'
  
    if free or exist then
      obj = (h_or_d=='d' and epp_domain or epp_host)
              :select{ [h_or_d=='d' and 'd_name' or 'h_name']=idn_name }
      if checks.gimme then
        checks[h_or_d=='d' and 'domain' or 'host'] = obj
      end
    end
  
    if exist and not obj then
      return nil, E_NOT_EXISTS, "Object not exists"
    elseif free and obj then
      return nil, E_EXISTS, "Object exists"
    elseif checks.served and not is_served then
      return nil, E_SERVICE_UNIMPLEMENTED, "Domain not in service"
    elseif checks.nested and idn_name ~= take_domain_part(idn_name) then
      return nil, E_PARAM_RANGE, "Domain should not be a nested subdomain"
    elseif owner_id and (obj.h_clid_m or obj.d_dlid_m or obj.d_clid_m) ~= owner_id then
      return nil, E_AUTHORIZATION, "Not owner"
    elseif del and h_or_d=='d' and domain_is_parent_for_hosts(obj) then
      return nil, E_ASSOC_PROHIBITE, "Domain is parent for hosts"
    elseif del and (obj.h_statusi or obj.d_statusi):find'DeleteProhibited' then
      return nil, E_STATUS_PROHIBITE, "Delete prohibited"
    elseif del and h_or_d=='h' and host_has_linked_domains(obj, owner_id, dealer) then
      return nil, E_ASSOC_PROHIBITE, "Host is linked with your domains"
    elseif checks.cre_host then
      if is_served then
        local domain_name = take_domain_part(idn_name)
        if idn_name == domain_name then
          return nil, E_PARAM_RANGE, "Parent domain can not be nameserver"
        end
        local domain = epp_domain:select{ d_name = domain_name }
        if checks.gimme then checks.domain = domain end
        if not domain then
          return nil, E_NOT_EXISTS, "Parent domain not exists"
        elseif (domain.d_dlid_m or domain.d_clid_m) ~= (dealer or checks.p_owner) then
          return nil, E_AUTHORIZATION, "Not owner of parent domain"
        end
      else
        checks_addr = 1
      end
    end
    if checks_addr and not is_served and not getaddrinfo(idn_name) then
      return nil, E_PARAM_POLICY, "Cannot resolve host domain name"
    end
  
    return idn_name
  end
end

-- DANGEROUS and WILL BE REMOVED IN FUTURE TODO FIXME
-- but now it's easier to write xml tags without quoting strings
local SCARY_FENV_META = setmetatable({}, { __index=function(_,k) return _G[k] or k end})

-- hello
local do_hello do
  local SERVER_NAME       = EPP_SRV_STRING
  local SERVER_VERSION    = EPP_SRV_VERSION
  local XMLNS_EPP_HOST    = XMLNS_EPP_HOST
  local XMLNS_EPP_DOMAIN  = XMLNS_EPP_DOMAIN
  local XMLNS_EPP_CONTACT = XMLNS_EPP_CONTACT

  do_hello = function(env, tag)
    -- hello uses special answer format so we directly assign to res_xml
    env.res_xml = xml.new { epp,
      { greeting,
        { svID, SERVER_NAME },
        { svDate, fmt_time() },
        { svcMenu,
          { version, SERVER_VERSION },
          { lang, en }, { lang, ru }, { lang, ua },
          { objURI, XMLNS_EPP_CONTACT },
          { objURI, XMLNS_EPP_DOMAIN },
          { objURI, XMLNS_EPP_HOST },
        },
        { dcp,
          { access, { all } },
          { statement,
            { purpose, { admin }, { prov } },
            { recipient, { public } },
            { retention, { stated } },
          },
        },
      },
    }:str()
    return nil, true -- skip out
  end
  setfenv(do_hello, SCARY_FENV_META)
end

-- domain:transfer
local do_domain_transfer do
  local XMLNS_EPP_DOMAIN         = XMLNS_EPP_DOMAIN
  local EPP_WAIT_DOMAIN_TRANSFER = EPP_WAIT_DOMAIN_TRANSFER
  local EPP_MAX_DOMAIN_RENEW     = EPP_MAX_DOMAIN_RENEW
  local m_s_tr_prohibite = {
    'inactive', 'blocked_paid_ops', 'blocked_all_ops', 'open_redemption',
  }
  local d_s_prohibite = {
    'TransferProhibited', 'pendingDelete', 'redemptionPeriod',
  }
  local d_s_pending = { 'pendingTransfer' }
  local filter_authinfo = eg_authinfo_filter_gen 'domain'
  do_domain_transfer = function(env, tag)
    -- Domain Transfer pt 1 : Parse
    local op = env.op
    local request, query, cancel = 'request'==op, 'query'==op, 'cancel'==op
    local reject, approve = 'reject'==op, 'approve'==op
    if not (request or query or reject or cancel or approve) then
      env.res_code = E_SYNTAX
      return
    end
    local my_id, registrar_id, dealer_id = get_mntnr_ids(env.authorized)

    local res_code, res_data
    local domain_name, period, authinfo
    local db_ok, operation_date, op_date_raw = true, fmt_time()
    local dom, dom_statusi, is_pending, is_owner
    local tr_status, dom_trans, dt_exdate, q_msg
    -- placeholders to methods dynamically generated later
    local select_domain_transfer_obj, make_domain_updater
    local check_is_contender, is_registrar_takeover
    -- placeholders to methods dynamically genetated by dynamically genetated methods
    -- (In order to understand recursion, you must first understand recursion)
    local dom_trans_updater, domain_updater
 
    local basket, checks = {}, {'d',gimme=1,exist=1,dealer=dealer_id}
    local ft_domain_transfer = {
      domain__name = entity_gatherer_gen(basket, 'domain_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks)
      ),
      domain__period = entity_gatherer_gen(basket, 'period',
        eg_filter_string, eg_filter_single,
        check_domain_period
      ),
      domain__authInfo = entity_gatherer_gen(basket, 'authinfo',
        eg_filter_single,
        filter_authinfo
      ),
      [-1] = entity_predicate_gatherer_gen(basket,
        function(finally) -- Domain Transfer pt 2 : Preparation
          res_code, domain_name, period, authinfo = gathered_unpack(
            basket, 'domain_name', 'period', 'authinfo', true
          )
          if not domain_name then
            return nil, E_SYNTAX, "No domain name specified"
          end
          -- res_code here cannot be set at this point
          -- if it is set this line is not reached (see predicate gatherer gen)
          dom = checks.domain
          dom_statusi = dom.d_statusi
          is_pending = check_status_present(d_s_pending, dom_statusi)
          is_owner = my_id == (dom.d_dlid_m or dom.d_clid_m)
          make_domain_updater = function(mod_status_method)
            domain_updater = function(upd_data)
              local db_ok = true
              if upd_data and 0 < table_hash_len(upd_data) then -- accepted
                local hosts = epp_host:find{ h_domain_d = dom.id_d }
                for _, host in ipairs(hosts) do
                  local h_statusi = host.h_statusi
                  h_statusi = mod_status_method(h_statusi, 'pendingTransfer')
                  h_statusi = check_set_ok_status(h_statusi)
                  db_ok = db_ok and update_host(host, {
                    h_clID_m  = my_id,
                    h_trDate  = operation_date,
                    h_statusi = h_statusi,
                  })
                end
              end
              upd_data = upd_data or {}
              local d_statusi = upd_data.d_statusi
              d_statusi = mod_status_method(dom_statusi, 'pendingTransfer')
              d_statusi = check_set_ok_status(d_statusi)
              upd_data.d_statusi = d_statusi
              
              -- BEWARE: dom.d_exdate should be set/updated before #CONVENIENT#
              upd_data.d_exDate = dom.d_exdate
              return db_ok and update_domain(dom, upd_data)
            end
          end
          select_domain_transfer_obj = function()
            dom_trans = epp_domain_transfer:order_by{ 'id_dt DESC' }
                                           :select{ dt_domain_d = dom.id_d }
            if dom_trans then
              dom_trans_updater = function()
                -- BEWARE: 
                -- `dom.d_exdate` should be set and
                -- local closure variable `tr_status` should be set
                -- BEFORE call to dom_trans_updater() #CONVENIENT#
                dom_trans.dt_acdate = operation_date
                return update_domtransfer(dom_trans, {
                  dt_status = tr_status,
                  dt_acdate = operation_date,
                  dt_exdate = dom.d_exdate,
                })
              end
              make_domain_updater(remove_str_status)
            end
            return dom_trans
          end
          check_is_contender = function()
            return dom_trans and dom_trans.dt_reid_m == my_id
          end
          -- registrar takeover means no approval direct transfer of domain
          -- from registrar's dealer to registrar itself. this is to avoid
          -- issues when dealer `disappears` and cannot be contacted etc
          is_registrar_takeover = not dealer_id
                                  and dom.d_dlid_m
                                  and dom.d_clid_m == my_id
          return finally
        end,
        function(finally) -- Domain Transfer pt 3 : Consistency
          if not domain_name then
            return nil, E_SYNTAX, "Missing domain name"
          elseif request and is_owner then
            return nil, E_TRANSF_NOT_ELIGIBLE, "Transfer to himself requested"
          elseif (reject or approve) and not is_owner then
            return nil, E_AUTHORIZATION, "Not owner"
          elseif (reject or approve or cancel) and not is_pending then
            return nil, E_TRANSF_NOT_ELIGIBLE, "Domain not in transfer"
          elseif request and not is_registrar_takeover and not authinfo then
            return nil, E_SYNTAX, "With authInfo please"
          elseif (request or query) and authinfo and authinfo ~= dom.d_authinfo then
            return nil, E_AUTHINFO, "Wrong authInfo"
          elseif request and check_status_present(d_s_prohibite, dom_statusi) then
            return nil, E_STATUS_PROHIBITE, "Transfer prohibited"
          elseif request and is_pending then
            return nil, E_PENDING, "Domain already in transfer"
          elseif check_status_present(m_s_tr_prohibite, env.authorized.m_statusi) then
            return nil, E_REJECTED, "Maintainer operations disabled"
          elseif not request and not select_domain_transfer_obj() then -- load dom_trans variable
            return nil, E_NOT_PENDING -- TODO FIXME warning - we shouldn't get here if the domain object status is ok, though - catch the DB inconsistency btw NEED THINK AND FIX or so
          elseif cancel and not check_is_contender() then
            return nil, E_AUTHORIZATION, "Not owner of transfer request"
          elseif query and not (is_owner or check_is_contender() or authinfo) then
            return nil, E_AUTHINFO, "No rights to query transfer info"
          end
          return finally
        end
      ),
      [ true ] = entity_unexpected_gen(basket),
    }

    local status, err = pcall(xmliter_switch, tag, ft_domain_transfer)
    res_code = gathered_unpack(basket)
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else -- Domain Transfer pt 4 : Acting
      -- @ this point all the sanity checks should be performed and the
      -- correctness of the objects' changes in DB should be fulfilled
      -- so the following code below is such the `pure action` :)
      if cancel then
        tr_status = 'clientCancelled'
        q_msg = "Transfer cancelled"
        db_ok = db_ok and dom_trans_updater()
        db_ok = db_ok and domain_updater()
      elseif reject then
        tr_status = 'clientRejected'
        q_msg = "Transfer rejected"
        db_ok = db_ok and dom_trans_updater()
        db_ok = db_ok and domain_updater()
      elseif approve then
        local requestor = epp_maintainer:select{ id_m = dom_trans.dt_reid_m }
        if requestor then
          local _, registrar_id, dealer_id = get_mntnr_ids(requestor)
          tr_status = 'clientApproved'
          q_msg = "Transfer approved"
          dom.d_exdate = dom_trans.dt_exdate
          db_ok = db_ok and dom_trans_updater()
          db_ok = db_ok and domain_updater{
            d_clid_m   = registrar_id,
            d_dlid_m   = dealer_id,
            d_authInfo = generate_auth(),
          }
        else -- direct return from inside. not a good one here, FIXME TODO
          env.res_code = E_FAILED
          return
        end
      elseif query then
        -- to show correct one in response
        dt_exdate = dom_trans.dt_exdate
      elseif request then
        local domain_updater_status_func
        if is_registrar_takeover then -- our dealer's domain
          tr_status = 'serverApproved'
          q_msg = "Transferred from dealer to registrator"
          domain_updater_status_func = func_passthru
        else
          tr_status = 'pending'
          q_msg = "Transfer requested"
          domain_updater_status_func = add_str_status
        end
        dt_exdate = date(dom.d_exdate)
        local maxdate = date_add_years(op_date_raw, EPP_MAX_DOMAIN_RENEW)
        local paid_op = { -- HERE FIXME HUH WTF some unfinished artifact :((((
        }
        local _period = period or 0 -- we don't force renew on transfer
        while _period > 0 do
          local new_exdate = date_add_years(dt_exdate, 1)
          if new_exdate <= maxdate then
            dt_exdate = new_exdate --TODO FIXME BILLING HERENOW +1 paid op
          end
          _period = -1 + _period
        end
        local acdate = date_add_days(op_date_raw, EPP_WAIT_DOMAIN_TRANSFER)
        dom_trans = {
          dt_domain_d = dom.id_d,
          dt_reid_m   = my_id,
          dt_acid_m   = dom.d_dlid_m or dom.d_clid_m,
          dt_acdate   = fmt_time(acdate),
          dt_exdate   = fmt_time(dt_exdate),
          dt_status   = tr_status,
        }
        db_ok = db_ok and epp_domain_transfer:insert(dom_trans)
        make_domain_updater(domain_updater_status_func)
        -- pass empty table: still need update hosts statusi, see domain_updater()
        db_ok = db_ok and domain_updater{}
      end
 
      dt_exdate = dt_exdate or dom.d_exdate
      res_data = dom_trans and {
        domain__trnData, xmlns__domain = XMLNS_EPP_DOMAIN,
        { domain__name, domain_name },
        { domain__trStatus, tr_status or dom_trans.dt_status },
        { domain__reID, mnt_name(dom_trans.dt_reid_m) },
        { domain__reDate, (fmt_time(dom_trans.dt_redate)) },
        { domain__acID, mnt_name(dom_trans.dt_acid_m) },
        { domain__acDate, (fmt_time(dom_trans.dt_acdate)) },
        { domain__exDate, (fmt_time(dt_exdate)) },
      }
      if not query then
        local addressee_m = (request or cancel) and dom_trans.dt_acid_m
                              or dom_trans.dt_reid_m
        db_ok = db_ok and epp_message_queue:insert{
          q_addressee_m = addressee_m,
          q_msg         = q_msg,
          q_queued_at   = fmt_time(),
          q_resData     = s_serialize(res_data),
        }
      end
    end -- if not status and not res_code after xmliter.switch

    if not db_push(db_ok) then res_code = E_FAILED end
    if not res_code then
      env.res_data = { resData, res_data }
      if request and tr_status == 'pending' then
        res_code = PENDING
      end
    end
    env.res_code = res_code
  end
  setfenv(do_domain_transfer, SCARY_FENV_META)
end

-- domain:info
local do_domain_info do
  local XMLNS_EPP_DOMAIN = XMLNS_EPP_DOMAIN
  do_domain_info = function(env, tag)
    local basket, checks = {}, {'d',exist=1,gimme=1}
    local res_code, authinfo, d_name, d
    local ft_domain_info = {
      domain__name = entity_gatherer_gen(basket, 'd_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks)
      ),
      domain__authInfo = entity_gatherer_gen(basket, 'authinfo',
        eg_filter_single,
        eg_authinfo_filter_gen'domain'
      ),
      [-1] = entity_predicate_gatherer_gen(basket,
        function(finally)
          d = checks.domain
          res_code, d_name, authinfo = gathered_unpack(
            basket, 'd_name', 'authinfo'
          )
          if not d_name then
            return nil, E_SYNTAX, "No domain name"
          elseif authinfo and authinfo ~= d.d_authinfo then
            return nil, E_AUTHINFO, "Wrong authInfo"
          end
          return finally
        end
      ),
      [true] = entity_unexpected_gen(basket),
    }

    local status, err = pcall(xmliter_switch, tag, ft_domain_info)
    res_code = gathered_unpack(basket)
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      local is_authorized = authinfo
              -- auth by dealer or by registrator
              or env.authorized.id_m == d.d_dlid_m
              or env.authorized.id_m == d.d_clid_m
      local registrant = epp_contact:select{ id_c = d.d_registrant_c }.c_id
      local rd = { domain__infData, xmlns__domain = XMLNS_EPP_DOMAIN,
        { domain__name, d_name },
        { domain__roid, d.d_roid },
        { domain__registrant, registrant },
      }
      fill_entities_from_array(rd, domain__status, d.d_statusi,
        function(tag)
          tag.s =  tag[_T_V]
          tag[_T_V] = nil
        end
      )
      fill_entities_from_array(rd, domain__contact,
        epp_domain_contact:find{ dc_domain_d = d.id_d }, -- iddqd
        function(tag)
          tag.type = tag[_T_V].dc_contact_type
          tag[_T_V] = epp_contact:select{ id_c = tag[_T_V].dc_contact_c }.c_id
        end
      )
      local nsi = { domain__ns }
      fill_entities_from_array(nsi, domain__hostObj,
        epp_domain_ns:find{ dn_domain_d = d.id_d },
        function(tag)
          tag[_T_V] = epp_host:select{ id_h = tag[_T_V].dn_host_h }.h_name
        end
      )
      if 0 < #nsi then rd[1+#rd] = nsi end
      if is_authorized then
        fill_entities_from_array(rd, domain__host,
          epp_host:find{ h_domain_d = d.id_d }, -- linked hosts
          function(tag)
            tag[_T_V] = tag[_T_V].h_name
          end
        )
      end
      rd[1+#rd] = { domain__clID, mnt_name(d.d_clid_m) }
      rd[1+#rd] = d.d_dlid_m and { domain__dlID, mnt_name(d.d_dlid_m) }
      rd[1+#rd] = { domain__crID, mnt_name(d.d_crid_m) }
      rd[1+#rd] = { domain__crDate, fmt_time(d.d_crdate) }
      rd[1+#rd] = { domain__exDate, fmt_time(d.d_exdate) }
      if is_authorized then
        local mnt_up = mnt_name(d.d_upid_m)
        if mnt_up then
          rd[1+#rd] = { domain__upID, mnt_up }
          rd[1+#rd] = { domain__upDate, fmt_time(d.d_update) }
        end                                                 
        rd[1+#rd] = d.d_trdate and { domain__trDate, fmt_time(d.d_trdate) }
        rd[1+#rd] = d.d_authinfo and { domain__authInfo,
          { domain__pw, d.d_authinfo }
        }
      end
  
      env.res_data = { resData, rd }
    end
    env.res_code = res_code
  end
  setfenv(do_domain_info, SCARY_FENV_META)
end

local do_contact_info do
  local XMLNS_EPP_CONTACT = XMLNS_EPP_CONTACT
  local EPP_CONTACT_DISLOSED_TEXT = EPP_CONTACT_DISLOSED_TEXT
  local function str_contains(str, ...)
    local rv = {}
    for _,v in pairs{...} do
      rv[1+#rv] = str:find(v)
    end
    return unpack(rv)
  end
  local function enclosure(need_show, what)
    return not need_show and EPP_CONTACT_DISLOSED_TEXT or what
  end
  do_contact_info = function(env, tag)
    local basket, checks = {}, {exist=1,gimme=1}
    local res_code, authinfo, contact_id, c
    local owner_id = env.authorized.id_m
    local ft_contact_info = {
      contact__id = entity_gatherer_gen(basket, 'c_id',
        eg_filter_string, eg_filter_single,
        partial_one(check_contact_obj, checks)
      ),
      contact__authInfo = entity_gatherer_gen(basket, 'authinfo',
        eg_filter_single,
        eg_authinfo_filter_gen'contact'
      ),
      [-1] = entity_predicate_gatherer_gen(basket,
        function(finally)
          c = checks.contact
          res_code, contact_id, authinfo = gathered_unpack(
            basket, 'c_id', 'authinfo'
          )
          if not contact_id then
            return nil, E_SYNTAX
          elseif c.c_clid_m ~= owner_id
              and authinfo and authinfo ~= c.c_authinfo then
            return nil, E_AUTHINFO
          end
          return finally
        end
      ),
      [true] = entity_unexpected_gen(basket),
    }

    local status, err = pcall(xmliter_switch, tag, ft_contact_info)
    res_code = gathered_unpack(basket)
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      local cpi = epp_cpi:find{ cpi_contact_c = c.id_c }
      local is_authorized = authinfo
              or env.authorized.id_m == c.c_clid_m
      local rd = { contact__infData, xmlns__contact = XMLNS_EPP_CONTACT,
        { contact__id, contact_id },
        { contact__roid, c.c_roid },
      }
      fill_entities_from_array(rd, contact__status, c.c_statusi)
      local discl_flags = c.c_disclose
      local disclose = is_authorized and { contact__disclose, flag='1' }
      local disclosure_flags = {
        gen = table_topsy_turvy(db_array_split(c.c_disclose)),
        loc = table_topsy_turvy(db_array_split(c.c_disclose_loc)),
        int = table_topsy_turvy(db_array_split(c.c_disclose_int)),
      }
      -- process postal info entries
      fill_entities_from_array(rd, contact__postalInfo, cpi,
        function(tag)
          local cpinfo = tag[_T_V]
          tag[_T_V] = nil
          local _type = cpinfo.cpi_type
          local d = disclosure_flags[_type]
          local _name, _org = cpinfo.cpi_name, cpinfo.cpi_org
          local _street, _city = cpinfo.cpi_street, cpinfo.cpi_city
          local _sp, _pc, _cc = cpinfo.cpi_sp, cpinfo.cpi_pc, cpinfo.cpi_cc
          if not is_authorized then
            _name, _org = enclosure(d.name, _name), enclosure(d.org, _org)
            _street, _sp = enclosure(d.addr, _street), enclosure(d.addr, _sp)
            _city, _pc = enclosure(d.addr, _city), enclosure(d.addr, _pc)
            _cc = enclosure(d.addr, _cc)
          end
          tag[1+#tag] = _name and { contact__name, _name }
          tag[1+#tag] = _org  and { contact__org,  _org }
          local addr = { contact__addr }
          do
            addr[1+#addr] = _street and { contact__street, _street }
            addr[1+#addr] = _city   and { contact__city, _city }
            addr[1+#addr] = _sp     and { contact__sp, _sp }
            addr[1+#addr] = _pc     and { contact__pc, _pc }
            addr[1+#addr] = _cc     and { contact__cc, _cc }
          end
          tag[1+#tag] = addr
          tag.type = _type
          return tag
        end
      )

      local c_email , c_voice, c_fax = c.c_email, c.c_voice, c.c_fax
      local d = disclosure_flags.gen
      if not is_authorized then
        c_email = enclosure(d.email, c_email)
        c_voice = enclosure(d.voice, c_voice)
        c_fax   = enclosure(d.fax,   c_fax)
      end
      rd[1+#rd] = c_voice and { contact__voice, c_voice }
      rd[1+#rd] = c_fax   and { contact__fax,   c_fax }
      rd[1+#rd] = c_email and { contact__email, c_email }
      rd[1+#rd] = { domain__clID, mnt_name(c.c_clid_m) }
      rd[1+#rd] = { domain__crID, mnt_name(c.c_crid_m) }
      rd[1+#rd] = { domain__crDate, fmt_time(c.c_crdate) }
      if is_authorized then
        local mnt_up = mnt_name(c.c_upid_m)
        if mnt_up then
          rd[1+#rd] = { contact__upID, mnt_up }
          rd[1+#rd] = { contact__upDate, fmt_time(c.c_update) }
        end
        rd[1+#rd] = c.c_trdate and { contact__trDate, fmt_time(d.d_trdate) } -- TODO not implemented now (like in hostmaster's one now)

        rd[1+#rd] = { contact__authInfo,
          { contact__pw, c.c_authinfo }
        }
        for _type, _tag_type in pairs{gen=false, int=true, loc=true} do
          local _tag_type = _tag_type and _type or nil
          for d_flag in pairs(disclosure_flags[_type]) do
            disclose[1+#disclose] = { contact__ .. d_flag, type=_tag_type }
          end
        end
        rd[1+#rd] = disclose
      end
      env.res_data = { resData, rd }
    end
    env.res_code = res_code
  end
  setfenv(do_contact_info, SCARY_FENV_META)
end

-- host:info
local do_host_info do
  local XMLNS_EPP_HOST = XMLNS_EPP_HOST
  do_host_info = function(env, tag)
    local basket, checks = {}, {'h',exist=1,gimme=1}
    local res_code, host_name, h
    local ft_host_info = {
      host__name = entity_gatherer_gen(basket, 'h_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks)
      ),
      [-1] = entity_predicate_gatherer_gen(basket,
        function(finally)
          h = checks.host
          res_code, host_name = gathered_unpack(basket, 'h_name')
          if not host_name then
            return nil, E_SYNTAX
          end
          return finally
        end
      ),
      [true] = entity_unexpected_gen(basket),
    }
    local status, err = pcall(xmliter_switch, tag, ft_host_info)
    local res_code = gathered_unpack(basket)

    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      local rd = { host__infData, xmlns__host = XMLNS_EPP_HOST,
        { host__name, host_name },
        { host__roid, h.h_roid },
      }
      fill_entities_from_array(rd, host__status, h.h_statusi)
      fill_entities_from_array(rd, host__addr, h.h_addressi,
        function(tag) tag.ip = tag[_T_V]:find":" and 'v6' or 'v4' end
      )
      rd[1+#rd] = { host__clID, mnt_name(h.h_clid_m) }
      rd[1+#rd] = { host__crID, mnt_name(h.h_crid_m) }
      rd[1+#rd] = { host__crDate, fmt_time(h.h_crdate) }
      local mnt_up = mnt_name(h.h_upid_m)
      if mnt_up then
        rd[1+#rd] = { host__upID, mnt_up }
        rd[1+#rd] = { host__upDate, fmt_time(h.h_update) }
      end
      rd[1+#rd] = h.h_trdate and { host__trDate, fmt_time(h.h_trdate) }
  
      env.res_data = { resData, rd }
    end
    env.res_code = res_code
  end
  setfenv(do_host_info, SCARY_FENV_META)
end

local add_remove_statusi = function(status_add, status_rem, statusi_topsy)
  local new_statusi = db_array_add_remove(status_add, status_rem, statusi_topsy)
  local len = #new_statusi
  if 0 == len or (1 == len and new_statusi[1] == 'linked') then
    new_statusi[1+len] = 'ok'
  end
  return new_statusi
end

-- host:create
local do_host_create, do_host_update do
  local XMLNS_EPP_HOST = XMLNS_EPP_HOST
  local EPP_MAX_HOST_IP = EPP_MAX_HOST_IP
  -- TODO FIXME provide <extValue> with failed ip addr on this checks
  local check_ip_in_nets = function(nets, ip) -- TODO FIXME move to snippets?
    for net,check in pairs(nets) do
      if check(ip) then return true end
    end
  end
  local ip4_reserved_nets = { -- http://www.ietf.org/rfc/rfc5735.txt
    ['0.0.0.0/8']       = function(ip) return ip[1] == 0 end,
    ['10.0.0.0/8']      = function(ip) return ip[1] == 10 end,
    ['127.0.0.0/8']     = function(ip) return ip[1] == 127 end,
    ['169.254.0.0/16']  = function(ip) return ip[1] == 169 and ip[2] == 254 end,
    ['172.16.0.0/12']   = function(ip) return ip[1] == 172 and ip[2] > 15 end,
    ['192.0.0.0/24']    = function(ip) return ip[1] == 192 and ip[2]+ip[3] == 0 end,
    ['192.0.2.0/24']    = function(ip) return ip[1] == 192 and ip[2] == 0 and ip[3] == 2 end,
    ['192.88.99.0/24']  = function(ip) return ip[1] == 192 and ip[2] == 88 and ip[3] == 99 end,
    ['192.168.0.0/16']  = function(ip) return ip[1] == 192 and ip[2] == 168 end,
    ['198.18.0.0/15']   = function(ip) return ip[1] == 198 and (ip[2] == 18 or ip[2] == 19) end,
    ['198.51.100.0/24'] = function(ip) return ip[1] == 198 and ip[2] == 51 and ip[3] == 100 end,
    ['203.0.113.0/24']  = function(ip) return ip[1] == 203 and ip[2] == 0 and ip[3] == 113 end,
    ['224.0.0.0/4']     = function(ip) return ip[1] >= 224 and ip[1] < 240 end,
    ['240.0.0.0/4']     = function(ip) return ip[1] >= 240 end,
    ['255.255.255.255'] = function(ip) return ip[1]+ip[2]+ip[3]+ip[4] == 1020 end,
  }
  local ip6_reserved_nets = { -- http://www.ietf.org/rfc/rfc5156.txt
    -- ['::/128 ::1/128'] = function(ip) return ip[1]+ip[2]+ip[3]+ip[4]+ip[5]+ip[6]+ip[7] == 0 and ip[8] < 2 end, -- those fell into below check...
    ['::FFFF:0:0/96 ::/96'] = function(ip) return ip[1]+ip[2]+ip[3]+ip[4]+ip[5] == 0 and (ip[6] == 0xFFFF or ip[6] == 0) end,
    ['fe80::/10']           = function(ip) return bit_and(ip[1], 0xffc0) == 0xfe80 end,
    ['fc00::/7']            = function(ip) return bit_and(ip[1], 0xfe00) == 0xfc00 end,
    ['2001:db8::/32']       = function(ip) return ip[1] == 0x2001 and ip[2] == 0xdb8 end,
    ['2001::/32']           = function(ip) return ip[1] == 0x2001 and ip[2] == 0 end,
    ['5f00::/8']            = function(ip) return bit_and(ip[1], 0xff00) == 0x5f00 end,
    ['3ffe::/16']           = function(ip) return ip[1] == 0x3ffe end,
    ['2001:10::/28']        = function(ip) return ip[1] == 0x2001 and bit_and(ip[2], 0xfff0) == 16 end,
    ['ff00::/8']            = function(ip) return bit_and(ip[1], 0xff00) == 0xff00 end,
    ['!2002::/16!ipv4!']    = function(ip)
      return ip[1] == 0x2002 and check_ip_in_nets(ip4_reserved_nets, {
        bit_rshift(ip[7], 8), bit_and(ip[7], 0xff), bit_rshift(ip[8], 8), bit_and(ip[8], 0xff)
      }) -- check ipv4 part
    end,
  }
  local check_ip_correct = function(ip, _ips, ip_tag)
    local ip_family, ip_data = check_ip(ip, true)
    if not ip_family then
      return nil, E_PARAM_VALUE, "Malformed IP address"
    elseif ip_family ~= ip_tag.ip then
      return nil, E_PARAM_VALUE, "IP address doesn't match ip family"
    elseif check_ip_in_nets(
             ip_family=='v6' and ip6_reserved_nets or ip4_reserved_nets,
             ip_data
           ) then
      return nil, E_PARAM_RANGE, "IP is in reserved networks, forbidden"
    end

    return ip
  end
  local h_d_s_prohibite = {'Hold','inactive','redemptionPeriod'}
  do_host_create = function(env, tag)
    local new_host = {}
    local ip_addrs = {}
    local owner_id, registrar_id, dealer_id = get_mntnr_ids(env.authorized)
    local checks = {'h',free=1,cre_host=1,gimme=1,p_owner=registrar_id,dealer=dealer_id}
    local ft_host_create = {
      host__name = entity_gatherer_gen(new_host, 'h_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks),
        function(val)
          local parent = checks.domain
          if parent then
            if check_status_present(h_d_s_prohibite, parent.d_statusi) then
              return nil, E_STATUS_PROHIBITE, "Parent domain status prohibits operation"
            end
          end
          return val
        end
      ),
      host__ip = entity_array_gatherer_gen(new_host, ip_addrs,
        eg_filter_string, check_ip_correct
      ),
      -- this entry to set host owned by inua flag and check integrity
      [-1] = entity_predicate_gatherer_gen(new_host,
        eg_array_filter_finally_gen(ip_addrs,
          {
            min = { 1, "No IP addresses found", E_PARAM_MISSING },
            max = { EPP_MAX_HOST_IP, "Too many IP addresses" },
            ex = { "Unexpected IP addresses of external host" },
            function(ip_addrs)
              local cnt = #ip_addrs
              if in_served_domain(new_host.h_name) then
                return cnt
              end
              if 0 == cnt then
                return 1 -- like, "OK, pass"
              end
            end
          },
          {
            max = { 0, "Duplicate IP addresses" },
            function(ip_addrs)
              local dupe = {}
              for _,ip in pairs(ip_addrs) do
                if dupe[ip] then return 1 end
                dupe[ip] = true
              end
              return 0
            end,
          }
        )
      ),
      [true] = entity_unexpected_gen(new_host),
    }

    -- microhack for call for automatic host create from domain:create
    if env.__called_via_domain_create then
      ft_host_create.domain__hostName = ft_host_create.host__name
      ft_host_create.domain__hostAddr = ft_host_create.host__ip
    end
  
    local status, err = pcall(xmliter_switch, tag, ft_host_create)
    local res_code = gathered_unpack(new_host)
    local db_ok, crdate = true, fmt_time()

    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = new_host._value, new_host._reason
    else
      local parent = checks.domain
      table_hash_merge(new_host, {
        h_roid = gen_h_roid(),
        h_domain_d = parent and parent.id_d,
        h_addressi = db_array_merge(ip_addrs),
        h_statusi = '{ok}',
        h_clID_m = owner_id,
        h_crID_m = owner_id,
        h_crDate = crdate,
      }, true)
      db_ok = db_ok and epp_host:insert(new_host)
    end -- if not status and not res_code after xmliter.switch

    -- if we called from domain:create do not commit transaction, as any error on domain:create appeared later will not rollback created hosts
    if not (env.__called_via_domain_create and db_ok or db_push(db_ok)) then
      res_code = E_FAILED
    end

    if not res_code then
      local rd = { host__creData, xmlns__host=XMLNS_EPP_HOST,
        { host__name, new_host.h_name },
        { host__crDate, crdate },
      }
      env.res_data = { resData, rd }
    end
    env.res_code = res_code
  end
  setfenv(do_host_create, SCARY_FENV_META)

  local host_update_partial_prohibited_status = 'clientUpdateProhibited'
  local host_update_statusi = {
    [host_update_partial_prohibited_status] = true,
    clientDeleteProhibited = true
  }
  local host_update_prohibited_statusi = {
    'serverUpdateProhibited', 'pending'
  }
  do_host_update = function(env, tag)
    local addrs_rem, addrs_add = {}, {}
    local status_rem, status_add = {}, {}
    local upd_host = { }
    local update_weight, ip_addr_count, ip_addr_current
    local owner_id = env.authorized.id_m
    local checks = {'h',exist=1,owner=owner_id,gimme=1}

    local dummy = entity_unexpected_gen(upd_host)
    local ft_host_update = {
      host__name = entity_gatherer_gen(upd_host, 'h_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks)
      ),
      host__add = {
        host__addr = entity_array_gatherer_gen(
          upd_host, addrs_add,
          eg_filter_string, check_ip_correct
        ),
        host__status = eg_update_status_gatherer_gen(
          upd_host, status_add, host_update_statusi
        ),
        [true] = dummy,
      },
      host__rem = {
        host__addr = entity_array_gatherer_gen(upd_host, addrs_rem,
          eg_filter_string, check_ip_correct
        ),
        host__status = eg_update_status_gatherer_gen(
          upd_host, status_rem, host_update_statusi
        ),
        [true] = dummy,
      },
      [-1] = entity_predicate_gatherer_gen(upd_host,
        function(finally)
          ip_addr_current = db_array_split(checks.host.h_addressi or '{}')
          ip_addr_count = #ip_addr_current + #addrs_add - #addrs_rem
          update_weight = -1 + table_hash_len(upd_host) -- -1 for h_name
              + #status_add + #status_rem + #addrs_add + #addrs_rem
          if 0 == update_weight then
            return nil, E_SYNTAX, "Empty update"
          end
          local h_name = upd_host.h_name
          if not h_name then
            return nil, E_SYNTAX, "Host name not present"
          elseif check_status_present(
                   host_update_prohibited_statusi, checks.host.h_statusi
                 ) then
            return nil, E_STATUS_PROHIBITE
          end
      
          return finally
        end,
        eg_array_filter_finally_gen(upd_host,
          {
            max = { 0, "Duplicate IP addresses" },
            function()
              local dupe = {}
              for _,ip in pairs(table_array_merge(addrs_rem, addrs_add)) do
                if dupe[ip] then return 1 end
                dupe[ip] = true
              end
              return 0
            end,
          },
          {
            max = { EPP_MAX_HOST_IP, "Too many IP addresses" },
            ex = { "Unexpected IP addresses of external host" },
            function()
              if in_served_domain(upd_host.h_name) then
                return ip_addr_count
              end
              if 0 == ip_addr_count then
                return 1 -- like, "OK, pass"
              end
            end
          },
          {
            min = { 1, "Add IP already in use", E_PARAM_POLICY },
            max = { 2, "Remove IP not in use", E_PARAM_POLICY },
            function()
              local ip_topsy = table_topsy_turvy(ip_addr_current)
              for _,ip in pairs(addrs_rem) do
                if not ip_topsy[ip] then return 3 end
              end
              for _,ip in pairs(addrs_add) do
                if ip_topsy[ip] then return 0 end
              end
              return 1
            end
          }
        )
      ),
      [true] = dummy,
    }

    local status, err = pcall(xmliter_switch, tag, ft_host_update)
    local db_ok, update = true, fmt_time()
    local res_code, h_name = gathered_unpack(upd_host, 'h_name', true)

    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = upd_host._value, upd_host._reason
    else
      local host = checks.host
      local statusi = db_array_split(host.h_statusi)
      local statusi_topsy = table_topsy_turvy(statusi)
      if not statusi_topsy[host_update_partial_prohibited_status]
          or (status_rem[1] == host_update_partial_prohibited_status
              and 1 == update_weight) then
        local id_h = host.id_h
        local h_statusi = db_array_merge(add_remove_statusi(
          status_add, status_rem, statusi_topsy
        ))
        local h_addressi = db_array_merge(db_array_add_remove(
          addrs_add, addrs_rem, host.h_addressi
        ))
        table_hash_merge(upd_host, {
          h_statusi  = h_statusi,
          h_addressi = h_addressi,
          h_upID_m   = owner_id,
          h_upDate   = update,
        }, true)
        db_ok = db_ok and update_host(host, upd_host)
      else
        res_code = E_STATUS_PROHIBITE
      end

      if not db_push(db_ok and not res_code) then
        res_code = res_code or E_FAILED
      end
    end
    env.res_code = res_code
  end
  setfenv(do_host_update, SCARY_FENV_META)
end

local do_domain_create, do_domain_update do
  local XMLNS_EPP_DOMAIN = XMLNS_EPP_DOMAIN
  local EPP_MAX_DOMAIN_NS = EPP_MAX_DOMAIN_NS
  local EPP_MAX_DOMAIN_CONTACTS = EPP_MAX_DOMAIN_CONTACTS
  local EPP_MAX_DOMAIN_CONTACTS_OF_TYPE = EPP_MAX_DOMAIN_CONTACTS_OF_TYPE
  local contact_by_type_counter_gen = function(type_name)
    return function(c_arr)
      local cnt=0
      for _,c in pairs(c_arr) do
        if c[1] == type_name then cnt = 1+cnt end
      end
      return cnt
    end
  end
  contact_admin_counter = contact_by_type_counter_gen'admin'
  contact_tech_counter  = contact_by_type_counter_gen'tech'
  local function create_scheduled_hosts(env, host_tags)
    for _, host_tag in pairs(host_tags) do
      -- use native host creation with our tag
      -- see do_host_create() for details
      local h_env = setmetatable(
        { __called_via_domain_create = true },
        { __index = env } -- read access to things like auth info
      )
      do_host_create(h_env, host_tag)
      if h_env.res_code and h_env.res_code ~= OK then
        env.res_code, env.res_reason, env.res_value =
          h_env.res_code, h_env.res_reason, h_env.res_value
        return
      end
    end
    return true
  end

  local function contacts_gatherer_gen(basket, idx_name, contacts_cache)
    return entity_array_gatherer_gen(basket, idx_name,
      eg_filter_string,
      function(val,_,tag)
        local t = tag.type
        if t~='admin' and t~='tech' then -- FIXME magic strings
          return nil, E_SYNTAX, "Contact have wrong `type` attribute"
        end
        local checks = {exist=1,gimme=1}
        local cid, code, msg, extra = check_contact_obj(checks, val)
        if not cid then return cid, code, msg, extra end
        if contacts_cache then contacts_cache[val] = checks.contact.id_c end
        return { t, val }
      end
    )
  end
  local function domain_ns_hostobj_gatherer_gen(basket, idx_name)
    return entity_array_gatherer_gen(basket, idx_name,
      eg_filter_string, -- eg_filter_unique ?? FIXME TODO
      partial_one(check_domains_hosts, {'h',exist=1})
    )
  end
  local function domain_ns_hostattr_gatherer_gen(basket, idx_name, sched_arr)
    return entity_array_gatherer_gen(basket, idx_name,
      eg_filter_tag,
      function(_,_,tag)
        -- fill up scheduled hosts array for further processing
        -- we delay host creation because glue hosts for current domain
        -- cannot be created before domain itself get created
        sched_arr[1+#sched_arr] = tag
        -- determine and collect host name for dupe checks with hostObj's
        for i=2,#tag do
          if tag[i][_T_N] == 'domain:hostName' then
            return tag[i][_T_V]
          end
        end
        return nil, E_SYNTAX, "Missing domain:hostName"
      end
    )
  end
  local function domain_registrant_gatherer_gen(basket)
    return entity_gatherer_gen(basket, 'd_registrant_c',
      eg_filter_string, eg_filter_single,
      function(registrant)
        -- registrators can use foreign contacts on their risk, no owner check
        local checks = {gimme=1,exist=1}
        local cid, err, reason = check_contact_obj(checks, registrant)
        return cid and checks.contact.id_c, err, reason
      end
    )
  end
  local function domain_process_ns_lnk(id_d, hosts, do_delete) -- TODO think about some caching like in contacts lnk rocessing too. for hosts is more complex as we not only check existing hosts but may create them on the fly
    local db_ok = true
    local op = do_delete and epp_domain_ns.delete or epp_domain_ns.insert
    for _, host in pairs(hosts) do
      local h = db_ok and epp_host:select{ h_name = host }
      -- on insert ignore existing links between same objs
      if do_delete or not epp_domain_ns:select{
                            dn_domain_d = id_d, dn_host_h = h.id_h
                          } then
        db_ok = db_ok and h and op(epp_domain_ns, {
          dn_domain_d = id_d, dn_host_h = h.id_h
        })
      end
    end
    return db_ok
  end
  local function domain_process_contacts_lnk(
                   id_d, contacts, contacts_cache, do_delete
                 )
    local db_ok = true
    local op = do_delete and epp_domain_contact.delete
                          or epp_domain_contact.insert
    for _, contact in pairs(contacts) do
      local c_type, c_id = unpack(contact)
      local id_c = contacts_cache[c_id]
      -- on insert ignore existing links between same objs
      if do_delete or not epp_domain_contact:select {
                            dc_domain_d = id_d, dc_contact_c = id_c,
                            dc_contact_type = c_type,
                          } then
        db_ok = db_ok and op(epp_domain_contact, {
          dc_domain_d = id_d, dc_contact_c = id_c, dc_contact_type = c_type
        })
      end
    end
    return db_ok
  end

  do_domain_create = function(env, tag)
    local hosts, contacts = {}, {}
    local scheduled_hosts = {}
    local contacts_cache = {}
    local new_domain = { }
    local dummy = entity_unexpected_gen(new_domain)

    local ft_domain_create = {
      domain__name = entity_gatherer_gen(new_domain, 'd_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, {'d',served=1,free=1,nested=1})
      ),
      domain__registrant = domain_registrant_gatherer_gen(new_domain),
      domain__period = entity_gatherer_gen(new_domain, 'period',
        eg_filter_string, eg_filter_single,
        check_domain_period
      ),
      domain__contact = contacts_gatherer_gen(new_domain, contacts, contacts_cache),
      domain__ns = {
        domain__hostObj = domain_ns_hostobj_gatherer_gen(new_domain, hosts),
        domain__hostAttr = domain_ns_hostattr_gatherer_gen(
          new_domain, hosts, scheduled_hosts
        ),
        [true] = dummy,
      },
      [-1] = entity_predicate_gatherer_gen(new_domain,
        function(finally)
          if not new_domain.d_name then
            return nil, E_SYNTAX, "No domain name"
          elseif not new_domain.d_registrant_c then
            return nil, E_SYNTAX, "No registrant"
          end
          return finally
        end,
        eg_array_filter_finally_gen(contacts,
          {
            max = { EPP_MAX_DOMAIN_CONTACTS, "Too many contacts" },
          },
          {
            min = { 1, "Need at least one admin contact" },
            max = { EPP_MAX_DOMAIN_CONTACTS_OF_TYPE,
                    "Too many admin contacts",
                  },
            contact_admin_counter,
          },
          {
            max = { EPP_MAX_DOMAIN_CONTACTS_OF_TYPE,
                    "Too many tech contacts"
                  },
            contact_tech_counter,
          },
          {
            max = { 0, "Duplicate contacts" },
            function()
              local dupe = {}
              for _,c in pairs(contacts) do
                local key = c[1]..c[2]
                if dupe[key] then return 1 end
                dupe[key] = true
              end
              return 0
            end,
          }
        ),
        eg_array_filter_finally_gen(hosts,
          {
            max = { EPP_MAX_DOMAIN_NS, "Too many nameservers" }
          },
          {
            max = { 0, "Duplicate nameservers" },
            function()
              local dupe = {}
              for _,h in pairs(hosts) do
                if dupe[h] then return 1 end
                dupe[h] = true
              end
              return 0
            end,
          }
        )
      ),
      domain__authInfo = func_empty, -- sliently ignore. TODO: warning, sir?
      [true] = dummy,
    }

    local status, err = pcall(xmliter_switch, tag, ft_domain_create)
    local db_ok, crdate, exdate = true, fmt_time()
    local res_code, period = gathered_unpack(new_domain, 'period', true)
    local domain_name = new_domain.d_name
    period = period or EPP_MIN_FULL_YEARS_PERIOD
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = new_domain._value, new_domain._reason
    else
      local owner_id, registrar_id, dealer_id = get_mntnr_ids(env.authorized)
      exdate = fmt_time(date_add_years(crdate, period))
      table_hash_merge(new_domain, {
        d_roid    = gen_d_roid(),
        d_clID_m  = registrar_id,
        d_dlID_m  = dealer_id,
        d_crID_m  = owner_id,
        d_crDate  = crdate,
        d_exDate  = exdate,
        d_statusi = '{ok}',
      }, true)

      db_ok = db_ok and epp_domain:insert(new_domain)
      local d = epp_domain:select{ d_name = domain_name }
      db_ok = db_ok and d and create_scheduled_hosts(env, scheduled_hosts)
      if db_ok then
        local id_d = d.id_d -- iddqd
        db_ok = db_ok and domain_process_ns_lnk(id_d, hosts)
        db_ok = db_ok and domain_process_contacts_lnk(
          id_d, contacts, contacts_cache
        )
      else
        -- create_scheduled_hosts() may have been set it on errors
        res_code = env.res_code or E_FAILED
      end
    end

    if not db_push(db_ok and not res_code) then
      res_code = res_code or E_FAILED
    end

    if not res_code then
      local rd = { domain__creData, xmlns__domain = XMLNS_EPP_DOMAIN,
        { domain__name, domain_name },
        { domain__crDate, crdate },
        { domain__exDate, exdate },
      }
      env.res_data = { resData, rd }
    end
    env.res_code = res_code
  end
  setfenv(do_domain_create, SCARY_FENV_META)

  local domain_update_partial_prohibited_status = 'clientUpdateProhibited'
  local domain_update_statusi = {
    [domain_update_partial_prohibited_status] = true,
    clientDeleteProhibited   = true,
    clientTransferProhibited = true,
    clientRenewProhibited    = true,
    clientHold               = true,
  }
  local domain_update_prohibited_statusi = {
    'serverUpdateProhibited',
    'pendingDelete','pendingTransfer', 'redemptionPeriod',
  }
  do_domain_update = function(env, tag)
    local hosts_rem, contacts_rem, status_rem = {}, {}, {}
    local hosts_add, contacts_add, status_add = {}, {}, {}
    local scheduled_hosts, contacts_cache = {}, {}
    local update_weight
    local extension = env.extension -- rgp:restore
    local owner_id = env.authorized.id_m
    local checks = {'d',exist=1,nested=1,gimme=1,owner=owner_id}
    local upd_domain = {}
    local dummy = entity_unexpected_gen(upd_domain)

    local ft_domain_update = {
      domain__name = entity_gatherer_gen(upd_domain, 'd_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks)
      ),
      domain__chg = {
        domain__registrant = domain_registrant_gatherer_gen(upd_domain),
        domain__authInfo = {
          domain__pw = entity_gatherer_gen(upd_domain, 'd_authInfo',
            eg_filter_string, eg_filter_single
          ),
          domain__null = entity_gatherer_gen_ex('flag', upd_domain, 'null_authinfo',
            eg_filter_empty
          ),
          [-1] = entity_predicate_gatherer_gen(upd_domain,
            function(finally)
              if upd_domain.d_authInfo and upd_domain.null_authinfo then
                return nil, E_SYNTAX, "What do you want for domain:authInfo?"
              end
              return finally
            end
          ),
          [true] = dummy,
        }
      },
      domain__add = {
        domain__status = eg_update_status_gatherer_gen(
          upd_domain, status_add, domain_update_statusi
        ),
        domain__contact = contacts_gatherer_gen(upd_domain, contacts_add, contacts_cache),
        domain__ns = {
          domain__hostObj = domain_ns_hostobj_gatherer_gen(
            upd_domain, hosts_add
          ),
          domain__hostAttr = domain_ns_hostattr_gatherer_gen(
            upd_domain, hosts_add, scheduled_hosts
          ),
          [true] = dummy,
        },
      },
      domain__rem = {
        domain__status = eg_update_status_gatherer_gen(
          upd_domain, status_rem, domain_update_statusi
        ),
        domain__contact = contacts_gatherer_gen(upd_domain, contacts_rem, contacts_cache),
        domain__ns = {
          domain__hostObj = domain_ns_hostobj_gatherer_gen(
            upd_domain, hosts_rem
          ),
          [true] = dummy,
        },
      },
      [-1] = entity_predicate_gatherer_gen(upd_domain,
        function(finally) -- init and general checks
          update_weight = -1 + table_hash_len(upd_domain) -- -1 for d_name
            + #status_add + #status_rem
            + #contacts_rem + #contacts_add
            + #hosts_rem + #hosts_add + (upd_domain.null_authinfo and 1 or 0)

          if not upd_domain.d_name then
            return nil, E_SYNTAX, "No domain name"
          end
          return finally
        end,
        function(finally) -- domain:update checks
          if extension then return finally end
          if 0 == update_weight then
            return nil, E_SYNTAX, "Empty update"
          elseif check_status_present(
                   domain_update_prohibited_statusi, checks.domain.d_statusi
                 ) then
            return nil, E_STATUS_PROHIBITE
          end
          return finally
        end,
        function(finally) -- rgp:restore checks
          if not extension then return finally end
          if 0 ~= update_weight then
            return nil, E_PARAM_POLICY, "Non-empty update with rgp:restore"
          elseif not checks.domain.d_statusi:find'redemptionPeriod' then
            return nil, E_STATUS_PROHIBITE, "Not in redemption period"
          end
          return finally
        end,
        function(finally)
          -- TODO FIXME HERE HERENOW all the final checks for contacts and ns dupes and counters should be @.

          return finally
        end
      ),
      [true] = dummy,
    }

    local status, err = pcall(xmliter_switch, tag, ft_domain_update)
    local db_ok, update = true, fmt_time()
    local res_code, domain_name, null_authinfo
            = gathered_unpack(upd_domain, 'd_name', 'null_authinfo', true)

    local domain = checks.domain
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = upd_domain._value, upd_domain._reason
    else
      if extension then
        -- microhack - domain restore is just syntactic sugar
        -- for 'redemptionPeriod' status removal
        status_rem[1] = 'redemptionPeriod'
      end
      local statusi = db_array_split(domain.d_statusi)
      local statusi_topsy = table_topsy_turvy(statusi)
      -- only removal of clientUpdateProhibited allowed if it is set
      if not statusi_topsy[domain_update_partial_prohibited_status]
           or (status_rem[1] == domain_update_partial_prohibited_status
               and 1 == update_weight)
           or (status_rem[1] == 'redemptionPeriod' -- rgp:restore case
               and 0 == update_weight) then
        local id_d = domain.id_d
        local d_statusi = db_array_merge(add_remove_statusi(
          status_add, status_rem, statusi_topsy
        ))
        table_hash_merge(upd_domain, {
          d_statusi = d_statusi,
          d_upID_m  = owner_id,
          d_upDate  = update,
        }, true)
        local domain_updater = update_domain(domain)
        db_ok = db_ok and domain_updater(upd_domain)
                      and create_scheduled_hosts(env, scheduled_hosts)
        if db_ok then
          db_ok = db_ok and domain_process_ns_lnk(id_d, hosts_rem, true)
                        and domain_process_ns_lnk(id_d, hosts_add)
                        and domain_process_contacts_lnk(
                              id_d, contacts_rem, contacts_cache, true
                            )
                        and domain_process_contacts_lnk(
                              id_d, contacts_add, contacts_cache
                            )
          if null_authinfo then
            db_ok = db_ok and domain_updater({ 'd_authInfo' }, true)
          end
        else
          -- create_scheduled_hosts() may have been set it on errors
          res_code = env.res_code or E_FAILED
        end
      else
        res_code = E_STATUS_PROHIBITE
      end

      if not db_push(db_ok and not res_code) then
        res_code = res_code or E_FAILED
      end
    end
    env.res_code = res_code
  end
  setfenv(do_domain_update, SCARY_FENV_META)
end

local check_email = function(email)
  if email:match"^[A-Za-z0-9%.%%%+%-]+@[A-Za-z0-9%.%%%+%-]+%.%w%w%w?%w?$" then
    return email
  end
  return nil, E_PARAM_VALUE, "Malformed email address"
end
local do_contact_create, do_contact_update do
  local XMLNS_EPP_CONTACT = XMLNS_EPP_CONTACT
  local EPP_PHONE_MAX_LEN = EPP_PHONE_MAX_LEN
  local COUNTRY_CC_CODES = COUNTRY_CC_CODES
  local check_phone_number = function(phone) -- not completely correct, need check for country code be fine (see ITU.E164.2005)
    if not phone:match'^%+%d+%.%d+$' or 
        EPP_PHONE_MAX_LEN < #phone then
      return nil, E_PARAM_VALUE, "Wrong phone number"
    end
    return phone
  end
  local check_and_concat_512 = eg_filter_string_concat_gen(512)
  local check_name   = check_and_concat_512
  local check_org    = check_and_concat_512
  local check_street = eg_filter_string_concat_gen(1024)
  -- below will be no concatenation as the eg_filter_single set before
  local check_255    = eg_filter_string_concat_gen(255)
  local check_sp     = check_255
  local check_city   = check_255
  local check_16     = eg_filter_string_concat_gen(16)
  local check_pc = function(val)
    if not val:match"^%d+$" then
      return nil, E_PARAM_VALUE, "Postal code should contain digits only"
    end
    return check_16(val)
  end
  local check_cc = function(val)
    val = val:upper()
    if not COUNTRY_CC_CODES[val] then
      return nil, E_PARAM_RANGE, "No country with this code (ISO 3166-1)"
    end
    return val
  end
  local disclose_attr_gen = function(disclose_info, basket, name)
    return entity_gatherer_gen_ex(
      function(tag) return tag and tag.type end, -- value is 'type' attr
      basket, name,
      eg_filter_empty,
      function(int_or_loc)
        local info = disclose_info[int_or_loc]
        if not info then
          return nil, E_SYNTAX, "Bad `type` attribute"
        end
        
        info[1+#info] = name
        return int_or_loc
      end
    )
  end
  local postalinfo_mandatory = {
    cpi_name = "Missing contact name in postalInfo",
    cpi_cc = "Missing country code in postalInfo",
    cpi_city = "Missing city in postalInfo",
  }
  local disclose_general_names = {'voice','fax','email'}
  local disclose_postalinfo_names = {'name','org','addr'}
  local disclose_names = {
    c_disclose = disclose_general_names,
    c_disclose_int = disclose_postalinfo_names,
    c_disclose_loc = disclose_postalinfo_names,
  }
  local filter_authinfo = eg_authinfo_filter_gen 'contact'

  local function postal_info_gatherer_gen(contact_basket)
    return entity_array_gatherer_gen(contact_basket, 'postal_info',
      eg_filter_tag,
      function(tag, _, parent)
        local _type = parent.type
        local cpi = { cpi_type = _type }
        if 'int' ~= _type and 'loc' ~= _type then
          return nil, E_SYNTAX, "Wrong postalInfo `type` attr"
        end
        local ft_postalinfo = {
          contact__name = entity_gatherer_gen(cpi, 'cpi_name',
            eg_filter_string,
            check_name
          ),
          contact__org = entity_gatherer_gen(cpi, 'cpi_org',
            eg_filter_string,
            check_org
          ),
          contact__addr = {
            contact__street = entity_gatherer_gen(cpi, 'cpi_street',
              eg_filter_string,
              check_street
            ),
            contact__city = entity_gatherer_gen(cpi, 'cpi_city',
              eg_filter_string,
              check_city
            ),
            contact__sp = entity_gatherer_gen(cpi, 'cpi_sp',
              eg_filter_string, eg_filter_single,
              check_sp
            ),
            contact__pc = entity_gatherer_gen(cpi, 'cpi_pc',
              eg_filter_string, eg_filter_single,
              check_pc
            ),
            contact__cc = entity_gatherer_gen(cpi, 'cpi_cc',
              eg_filter_string, eg_filter_single,
              check_cc
            ),
          },
          [true] = entity_unexpected_gen(contact_basket),
        }

        local status, err = pcall(xmliter_switch, parent, ft_postalinfo)
        if not status then
          return nil, E_FAILED
        end
        local res_code = gathered_unpack(cpi)
        if res_code then
          return nil, res_code, cpi._reason, cpi._value
        end

        for k,msg in pairs(postalinfo_mandatory) do
          if not cpi[k] then
            return nil, E_PARAM_VALUE, msg
          end
        end

        return cpi
      end
    )
  end

  local function disclose_info_gatherer_gen(contact_basket)
    return entity_gatherer_gen_ex(
      func_passthru, -- value getter just take tag
      contact_basket, 'disclose_info',
      eg_filter_single, eg_filter_tag,
      function(tag) -- will get full <contact:disclose>, look, func_passthru
        local disclose_info = { flag = tag.flag,
          int = {}, loc = {}, general = {}
        }
        local flag = tag.flag
        if not flag then
          return nil, E_SYNTAX, "Missing `flag` attribute"
        elseif '0' ~= flag and '1' ~= flag then
          return nil, E_SYNTAX, "Wrong `flag` attribute value"
        end
        local b = {} -- intermittent temporary basket
        local ft_disclose = {
          contact__voice = entity_flag_gatherer_gen(b, 'voice',
            eg_filter_single, eg_filter_empty
          ),
          contact__email = entity_flag_gatherer_gen(b, 'email',
            eg_filter_single, eg_filter_empty
          ),
          contact__fax   = entity_flag_gatherer_gen(b, 'fax',
            eg_filter_single, eg_filter_empty
          ),
          contact__name = disclose_attr_gen(disclose_info, b, 'name'),
          contact__org  = disclose_attr_gen(disclose_info, b, 'org'),
          contact__addr = disclose_attr_gen(disclose_info, b, 'addr'),
          [true] = entity_gatherer_gen(b, 'dummy', { eg_unexpected }),
        }
        local status, err = pcall(xmliter_switch, tag, ft_disclose)
        if not status then
          return nil, E_FAILED
        end
        local res_code = gathered_unpack(b)
        if res_code then
          return nil, res_code, b._reason, b._value
        end
        local general = disclose_info.general
        for _, what in pairs{'voice', 'email', 'fax'} do
          general[1+#general] = b[what] and what
        end
        disclose_info.type = flag
        return disclose_info
      end
    )
  end

  local check_contact_finally = function(checks, contact)
    local p_info = contact.postal_info
    if checks.postal and (not p_info or 0 == #p_info) then
      return nil, E_PARAM_MISSING, "Missing postalInfo"
    elseif p_info and #p_info > 2 then
      return nil, E_PARAM_VALUE, "Too many postalInfo tags"
    elseif p_info and p_info[2] and
           p_info[1].cpi_type == p_info[2].cpi_type then
      return nil, E_PARAM_VALUE, "Equal types of postalInfo"
    elseif checks.email and not contact.c_email then
      return nil, E_PARAM_MISSING, "Missing email"
    elseif not contact.c_id then
      return nil, E_PARAM_MISSING, "Missing contact id"
    elseif checks.gen_auth and not contact.c_authinfo then
      contact.c_authinfo = generate_auth()
    end
    return true
  end

  local setup_contact_disclose_fields = function(contact, disclose_info)
    if disclose_info then
      local do_enclose = "0" == disclose_info.type -- "0" for 'enclose'
      for fld, di_names in pairs{
                           c_disclose     = disclose_info.general,
                           c_disclose_loc = disclose_info.loc,
                           c_disclose_int = disclose_info.int,
                         } do
        contact[fld] = db_array_merge(
          do_enclose
             and table_set_complement(disclose_names[fld], di_names)
             or di_names
        )
      end
    end
  end

  local function ft_contact_gatherer_gen(
      contact_basket, finally_func, include_contact_id
    )
    local ft_contact = {
      contact__voice = entity_gatherer_gen(contact_basket, 'c_voice',
        eg_filter_string, eg_filter_single,
        check_phone_number
      ),
      contact__fax = entity_gatherer_gen(contact_basket, 'c_fax',
        eg_filter_string, eg_filter_single,
        check_phone_number
      ),
      contact__email = entity_gatherer_gen(contact_basket, 'c_email',
        eg_filter_string, eg_filter_single,
        check_email
      ),
      contact__authInfo = entity_gatherer_gen(contact_basket, 'c_authinfo',
        eg_filter_single,
        filter_authinfo
      ),
 
      contact__disclose = disclose_info_gatherer_gen(contact_basket),
      contact__postalInfo = postal_info_gatherer_gen(contact_basket),
 
      [-1] = entity_predicate_gatherer_gen(contact_basket, finally_func),
      [true] = entity_unexpected_gen(contact_basket),
    }

    if include_contact_id then
      ft_contact.contact__id = entity_gatherer_gen(contact_basket, 'c_id',
        eg_filter_string, eg_filter_single,
        partial_one(check_contact_obj, {free=1})
      )
    end

    return ft_contact
  end

  do_contact_create = function(env, tag)
    local new_contact = {}
    local ft_contact_create = ft_contact_gatherer_gen(
      new_contact,
      partial_twix(
        check_contact_finally,
        {gen_auth=1,email=1,postal=1},
        new_contact
      ),
      true
    )

    local status, err = pcall(xmliter_switch, tag, ft_contact_create)
    local res_code, postal_info, disclose_info = gathered_unpack(
      new_contact, 'postal_info', 'disclose_info', true
    )

    local db_ok, crdate = true, fmt_time()
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = new_contact._value, new_contact._reason
    else
      local owner_id = env.authorized.id_m
      table_hash_merge(new_contact, {
        c_roid = gen_c_roid(),
        c_clID_m = owner_id,
        c_crID_m = owner_id,
        c_crDate = crdate,
      }, true)

      setup_contact_disclose_fields(new_contact, disclose_info)
      db_ok = db_ok and epp_contact:insert(new_contact)
      if db_ok then
        local id_c = epp_contact:select{ c_id = new_contact.c_id }.id_c
        for _,cpi in pairs(postal_info) do
          cpi.cpi_contact_c = id_c
          db_ok = db_ok and epp_cpi:insert(cpi)
        end
      end
    end

    if not db_push(db_ok) then res_code = E_FAILED end
    if not res_code then
      local rd = { contact__creData, xmlns__contact=XMLNS_EPP_CONTACT,
        { contact__id, new_contact.c_id },
        { contact__crDate, crdate },
      }
      env.res_data = { resData, rd }
    end
    env.res_code = res_code
  end
  setfenv(do_contact_create, SCARY_FENV_META)

  local contact_update_partial_prohibited_status = 'clientUpdateProhibited'
  local contact_update_statusi = {
    [contact_update_partial_prohibited_status] = true,
    clientDeleteProhibited = true,
    clientTransferProhibited = true,
  }
  do_contact_update = function(env, tag)
    local status_add, status_rem = {}, {}
    local upd_contact = {}
    local dummy = entity_unexpected_gen(upd_contact)
    local owner_id = env.authorized.id_m
    local checks = {gimme=1,exist=1,owner=owner_id}
    local res_code, postal_info, disclose_info, contact_id, update_weight
    local ft_contact_update = {
      contact__id = entity_gatherer_gen(upd_contact, 'c_id',
        eg_filter_string, eg_filter_single,
        partial_one(check_contact_obj, checks)
      ),
      contact__add = {
        contact__status = eg_update_status_gatherer_gen(
          upd_contact, status_add, contact_update_statusi
        ),
        [true] = dummy,
      },
      contact__rem = {
        contact__status = eg_update_status_gatherer_gen(
          upd_contact, status_rem, contact_update_statusi
        ),
        [true] = dummy,
      },
      contact__chg = ft_contact_gatherer_gen(
        upd_contact,
        partial_twix(check_contact_finally, {}, upd_contact)
      ),
      [-1] = entity_predicate_gatherer_gen(upd_contact,
        function(finally)
          res_code, postal_info, disclose_info, contact_id = gathered_unpack(
            upd_contact, 'postal_info', 'disclose_info', 'c_id', true
          )

          if not contact_id then
            return nil, E_PARAM_MISSING, "Missing contact id"
          end
          update_weight = table_hash_len(upd_contact)
            + #status_add + #status_rem
            + (postal_info and #postal_info or 0)
            + (disclose_info and 1 or 0)
          if 0 == update_weight then
            return nil, E_SYNTAX, "Empty update"
          end

          return finally
        end
      ),
      [true] = dummy,
    }

    local status, err = pcall(xmliter_switch, tag, ft_contact_update)
    res_code = gathered_unpack(upd_contact)
    local db_ok, update = true, fmt_time()
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = upd_contact._value, upd_contact._reason
    else
      local contact = checks.contact
      local statusi = db_array_split(contact.c_statusi)
      local statusi_topsy = table_topsy_turvy(statusi)

      if not statusi_topsy.serverUpdateProhibited and -- HERE FIXME this server prohibited statusi may be checked in [-1] handler though
         (not statusi_topsy[contact_update_partial_prohibited_status]
          or (status_rem[1] == contact_update_partial_prohibited_status
              and 1 == update_weight)) then
        local c_statusi = db_array_merge(
          add_remove_statusi(status_add, status_rem, statusi_topsy)
        )
        table_hash_merge(upd_contact, {
          c_statusi = c_statusi,
          c_upID_m  = owner_id,
          c_upDate  = update,
        }, true)
        setup_contact_disclose_fields(upd_contact, disclose_info)

        db_ok = db_ok and update_contact(contact, upd_contact)
        if db_ok then
          local id_c = contact.id_c
          for _,cpi in pairs(postal_info) do
            db_ok = db_ok and epp_cpi:delete {
              cpi_contact_c = id_c, cpi_type = cpi.cpi_type
            }
            cpi.cpi_contact_c = id_c
            db_ok = db_ok and epp_cpi:insert(cpi)
          end
        end
      else
        res_code = E_STATUS_PROHIBITE
      end
    end

    if not db_push(db_ok) then res_code = E_FAILED end
    env.res_code = res_code
  end
  setfenv(do_contact_update, SCARY_FENV_META)
end

-- domain:check
local do_domain_check do
  local XMLNS_EPP_DOMAIN = XMLNS_EPP_DOMAIN
  local EPP_MAX_CHECK_DOMAINS = EPP_MAX_CHECK_DOMAINS
  do_domain_check = function(env, tag)
    local domains = {}
    local basket = { domains = domains }
    local ft_domain_name = {
      domain__name = entity_array_gatherer_gen(basket, 'domains',
        eg_filter_string
      ),
      [-1] = entity_predicate_gatherer_gen(basket,
        eg_array_filter_finally_gen(domains, {
          min = { 1, "Nothing to check"},
          max = { EPP_MAX_CHECK_DOMAINS, "Too many domains specified" },
        })
      ),
      [true] = entity_unexpected_gen(basket, 'dummy', { eg_unexpected }),
    }
    local status, err = pcall(xmliter_switch, tag, ft_domain_name)
    local res_code = gathered_unpack(basket)
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      local checks = {}
      for _, domain_name in pairs(domains) do
        local idn_name, _, reason = check_domains_hosts(
          {'d',served=1,nested=1,free=1}, domain_name
        )
        local check = { domain__cd,
          { domain__name, avail = idn_name and "1" or "0", domain_name }
        }
        check[1+#check] = reason and {
          domain__reason, lang = env.lang, reason
        }
        checks[1+#checks] = check
      end
      
      env.res_data = { resData,
        { domain__chkData, xmlns__domain = XMLNS_EPP_DOMAIN, unpack(checks) },
      }
    end
    env.res_code = res_code
  end
  setfenv(do_domain_check, SCARY_FENV_META)
end

-- host:check
local do_host_check do
  local XMLNS_EPP_HOST = XMLNS_EPP_HOST
  local EPP_MAX_CHECK_HOSTS = EPP_MAX_CHECK_HOSTS
  do_host_check = function(env, tag)
    local hosts = {}
    local basket = { hosts = hosts }
    local ft_host_name = {
      host__name = entity_array_gatherer_gen(basket, 'hosts',
        eg_filter_string
      ),
      [-1] = entity_predicate_gatherer_gen(basket,
        eg_array_filter_finally_gen(hosts, {
          min = { 1, "Nothing to check"},
          max = { EPP_MAX_CHECK_HOSTS, "Too many hosts specified" },
        })
      ),
      [true] = entity_unexpected_gen(basket),
    }
    local status, err = pcall(xmliter_switch, tag, ft_host_name)
    local res_code = gathered_unpack(basket)
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
       local checks = {}
       for _, host_name in pairs(hosts) do
         local owner_id = env.authorized.id_m
         local idn_name, _, reason = check_domains_hosts(
           {'h',cre_host=1,free=1,p_owner=owner_id}, host_name
         )
         local lang = reason and env.lang -- HERENOW TODO FIXME in all the places with reasons - take the language in account
         local check = { host__cd,
           { host__name, avail = idn_name and "1" or "0", host_name }
         }
         check[3] = reason and { host__reason, lang = lang, reason }
         checks[1+#checks] = check
       end
       
       env.res_data = { resData,
         { host__chkData, xmlns__host = XMLNS_EPP_HOST, unpack(checks) },
       }
    end
    env.res_code = res_code
  end
  setfenv(do_host_check, SCARY_FENV_META)
end

-- contact:check
local do_contact_check do
  local XMLNS_EPP_CONTACT = XMLNS_EPP_CONTACT
  local EPP_MAX_CHECK_CONTACTS = EPP_MAX_CHECK_CONTACTS
  do_contact_check = function(env, tag)
    local contacts = {}
    local basket = { contacts = contacts }
    local ft_contact_id = {
      contact__id = entity_array_gatherer_gen(basket, 'contacts',
        eg_filter_string
      ),
      [-1] = entity_predicate_gatherer_gen(basket,
        eg_array_filter_finally_gen(contacts, {
          min = { 1, "Nothing to check"},
          max = { EPP_MAX_CHECK_CONTACTS, "Too many contacts specified" },
        })
      ),
      [true] = entity_unexpected_gen(basket),
    }
    local status, err = pcall(xmliter_switch, tag, ft_contact_id)
    local res_code = gathered_unpack(basket)
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      local checks = {}

      for _, contact_id in pairs(contacts) do
        local avail, _, reason = check_contact_obj({free=1}, contact_id)
        local check = { contact__cd,
          { contact__id, avail = avail and 1 or 0, contact_id }
        }
        check[3] = reason and { contact__reason, lang = env.lang, reason }
        checks[1+#checks] = check
      end
      env.res_data = { resData,
        { contact__chkData, xmlns__contact = XMLNS_EPP_CONTACT,
          unpack(checks)
        },
      }
    end
    env.res_code = res_code
  end
  setfenv(do_contact_check, SCARY_FENV_META)
end

local do_contact_delete do
  do_contact_delete = function(env, tag)
    local basket = {}
    local owner_id = env.authorized.id_m
    local checks = {exist=1,gimme=1,owner=owner_id,del=1}
    local ft_contact_id = {
      contact__id = entity_gatherer_gen(basket, 'c_id',
        eg_filter_string, eg_filter_single,
        partial_one(check_contact_obj, checks)
      ),
      [true] = entity_unexpected_gen(basket),
    }

    local status, err = pcall(xmliter_switch, tag, ft_contact_id)
    local res_code = gathered_unpack(basket)
    local db_ok = true
 
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      db_ok = db_ok and epp_contact:delete{ id_c = checks.contact.id_c }
    end

    if not db_push(db_ok) then res_code = E_FAILED end
    env.res_code = res_code
  end
  setfenv(do_contact_delete, SCARY_FENV_META)
end

local do_host_delete do
  do_host_delete = function(env, tag)
    local basket = {}
    local owner_id, registrar_id, dealer_id = get_mntnr_ids(env.authorized)
    local is_force = '1' == env.force
    local checks = {'h',gimme=1,owner=owner_id,exist=1,dealer=dealer_id}
    -- enable the object associations checks if not force delete
    if not is_force then checks.del=1 end
    local ft_host_name = {
      host__name = entity_gatherer_gen(basket, 'host_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks)
      ),
      [-1] = entity_predicate_gatherer_gen(basket,
        function(finally)
          if not basket.host_name then
            return nil, E_SYNTAX, "No host name given"
          end
          return finally
        end
      ),
      [true] = entity_unexpected_gen(basket),
    }

    local status, err = pcall(xmliter_switch, tag, ft_host_name)
    local res_code, host_name = gathered_unpack(basket, 'host_name')
    local db_ok = true

    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      db_ok = db_ok and try_get_rid_linked(checks.host, owner_id, is_force)
      if not db_ok then
        res_code = E_ASSOC_PROHIBITE
      end
      db_ok = db_ok and epp_host:delete{ id_h = checks.host.id_h }
    end

    if not db_push(db_ok) then res_code = res_code or E_FAILED end
    env.res_code = res_code
  end
  setfenv(do_host_delete, SCARY_FENV_META)
end

local do_domain_delete do
  do_domain_delete = function(env, tag)
    local basket = {}
    local owner_id, registrar_id, dealer_id = get_mntnr_ids(env.authorized)
    local is_force = '1' == env.force
    local checks = {'d',gimme=1,owner=owner_id,exist=1,dealer=dealer_id}
    -- enable the object associations checks if not force delete
    if not is_force then checks.del=1 end
    local ft_domain_name = {
      domain__name = entity_gatherer_gen(basket, 'domain_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks)
      ),
      [true] = entity_unexpected_gen(basket),
    }

    local status, err = pcall(xmliter_switch, tag, ft_domain_name)
    local res_code, domain_name = gathered_unpack(basket, 'domain_name')
    local db_ok = true

    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      local domain = checks.domain
      local id_d = domain.id_d
      if is_force then -- ok ok, you're angry or in hurry
        local glue_hosts = epp_host:find{ h_domain_d = id_d }
        for _,host in pairs(glue_hosts) do
          if host.h_statusi:find'DeleteProhibited' then
            db_ok = false
            res_code = E_ASSOC_PROHIBITE
            env.res_value = { host__name, host.h_name }
            env.res_reason = "Status prohibites deletion of associated host"
            break
          else
            db_ok = db_ok and epp_host:delete{ id_h = host.id_h }
          end
        end
      end
      db_ok = db_ok and epp_domain:delete{ id_d = id_d }
    end

    if not db_push(db_ok) then res_code = res_code or E_FAILED end
    env.res_code = res_code
  end
  setfenv(do_host_delete, SCARY_FENV_META)
end

local do_domain_renew do
  local EPP_MAX_FULL_YEARS_PERIOD = EPP_MAX_FULL_YEARS_PERIOD
  local check_date_ok = function(date_val)
    local status, d = pcall(date, date_val)
    if not status or 0 ~= d.dayfrc then
      return nil, E_PARAM_VALUE, "Wrong date"
    end
    return d
  end
  local check_date_same_day = function(date1, date2)
    local diff = date(date1) - date(date2)
    return 0 == diff.daynum -- the date part of datetime is the same
  end

  do_domain_renew = function(env, tag)
    local basket = {}
    local checks = {'d',exist=1,gimme=1}
    local domain, new_exdate
    local ft_domain_renew = {
      domain__name = entity_gatherer_gen(basket, 'd_name',
        eg_filter_string, eg_filter_single,
        partial_one(check_domains_hosts, checks)
      ),
      domain__curExpDate = entity_gatherer_gen(basket, 'current_exp_date',
        eg_filter_single, check_date_ok
      ),
      domain__period = entity_gatherer_gen(basket, 'period',
        eg_filter_string, eg_filter_single,
        check_domain_period
      ),
      [ -1 ] = entity_predicate_gatherer_gen(basket,
        function(finally)
          domain = checks.domain
          local _, period, current_exp_date = gathered_unpack(
            basket, 'period', 'current_exp_date'
          )
          local d_exDate = domain.d_exdate
          if not period or not current_exp_date then
            return nil, E_SYNTAX, "Specify both period and curExpDate"
          end
          new_exdate = date_add_years(d_exDate, basket.period)
          if not check_date_same_day(d_exDate, basket.current_exp_date) then
            return nil, E_RENEW_NOT_ELIGIBLE, "curExpDate mismatch"
          elseif new_exdate > date():addyears(EPP_MAX_FULL_YEARS_PERIOD) then
            return nil, E_PARAM_RANGE, "Domain renew exceed range"
          elseif check_status_present(
            { 'RenewProhibited', 'pendingDelete', 'pendingTransfer',
              'redemptionPeriod'
            }, domain.d_statusi) then
            return nil, E_STATUS_PROHIBITE
          end
          return finally
        end
      ),
      [ true ] = entity_unexpected_gen(basket),
    }
    local status, err = pcall(xmliter_switch, tag, ft_domain_renew)
    local db_ok = true
    local res_code = gathered_unpack(basket)
    if not status then
      res_code = E_FAILED
    elseif res_code then
      env.res_value, env.res_reason = basket._value, basket._reason
    else
      -- TODO FIXME BILLING HERE
      db_ok = db_ok and update_domain(domain, {d_exdate = fmt_time(new_exdate)})
    end
    if not db_push(db_ok) then res_code = E_FAILED end
    env.res_code = res_code
  end
  setfenv(do_domain_renew, SCARY_FENV_META)
end

local do_poll do
  do_poll = function(env, tag)
    local db_ok, res_code = true
    local my_id_m = env.authorized.id_m 

    local first_msg = epp_message_queue:order_by{'id_q'}
                        :select{ q_addressee_m = my_id_m }
    local cnt = epp_message_queue:select{ q_addressee_m = my_id_m,
                                          'count(*) as cnt' }.cnt
 
    local op = tag.op
    if not first_msg then
      res_code = EMPTY_Q
    elseif op == 'req' then
      env.res_msg_q = { 'msgQ', count=cnt, id=first_msg.id_q,
        { qDate, fmt_time(first_msg.q_queued_at) },
        { 'msg', first_msg.q_msg }
      }
      local res_data = first_msg.q_resData
      env.res_data = res_data and s_deserialize(res_data)
    elseif op == 'ack' then
      if tostring(first_msg.id_q) ~= tag.msgID then
        res_code = E_PARAM_RANGE
      else
        db_ok = db_ok and epp_message_queue:delete{ id_q = tag.msgID }
        first_msg = epp_message_queue:order_by{'id_q'}
                      :select{ q_addressee_m = my_id_m }
        if not first_msg then
          res_code = EMPTY_Q
        else
          env.res_msg_q = { 'msgQ', count=-1+cnt, id=first_msg.id_q }
        end
      end
    else
      res_code = E_SYNTAX
    end
  
    if not db_push(db_ok) then
      res_code = E_FAILED
      env.res_msg_q = nil
    end
    env.res_code = res_code
  end
end


-- login
local do_login do
  local crypt = sn.posix_crypt -- wrapped with pcall in snippets
  local salt_prefix = {
    ['MD5-PW']    = '$1$',
    ['SHA256-PW'] = '$5$',
    ['SHA512-PW'] = '$6$',
    ['DES']       = '',
  }
  local gen_crypt = function(pwd, salt, prefix)
    local rv = prefix
    if pwd then
      if salt then
        if not salt:match(prefix:gsub('%$', '%%$'), nil) then
          salt = prefix..salt
        end
        rv = crypt(pwd, salt)
      else -- no salt means RAW asswords in DB (FIXME buggy, do we need it?)
        rv = pwd
      end
    end
    return rv
  end
  local function change_password(env, tag)
    local pwd_type = tag.crypt or 'MD5-PW'
    local pwd = tag[_T_V] or ''

    local enc_pwd
    if #pwd > 2 then
      enc_pwd = gen_crypt(pwd, env.uuid, salt_prefix[pwd_type])
      if enc_pwd then
        enc_pwd = pwd_type..' '..enc_pwd
      end
    end

    if enc_pwd and update_maintainer(env.authorized, {m_authinfo = enc_pwd})
               and db_push(true) then
      return
    end
    env.res_code = E_FAILED
    return nil, true
  end

  local EPP_SRV_LANGUAGES = EPP_SRV_LANGUAGES
  do_login = function(env, tag)
    local clID_tag, pwd_tag = xml_find(tag, 'clID'), xml_find(tag, 'pw')
    if clID_tag and pwd_tag then
      local clID, pwd = clID_tag[_T_V], pwd_tag[_T_V]
      if clID and pwd then
        local maintainer = epp_maintainer:select{m_id = clID}
        if maintainer then
          local _type,_,_crypt = maintainer.m_authinfo:match"^(.*)(%s+)(.*)$"
          if _crypt == gen_crypt(pwd, _crypt, salt_prefix[_type]) then
            env.authorized = maintainer
            return {
              newPW = partial_one(change_password, env),
              options = {
                lang = function(tag)
                  local lang = tag[_T_V]
                  lang = EPP_SRV_LANGUAGES[lang] and lang
                  if not lang then
                    env.res_code = RES.E_UNIMPLEMENTED_OPT
                    env.authorized = nil
                    return nil, true
                  end
                  env.lang = lang
                end,
                version = function(tag) env.version = tag[_T_V] end,
              },
            }
          end
          env.res_code = E_AUTHENTICATION
        else
          env.res_code, env.res_value = E_NOT_EXISTS, clID_tag
          env.res_reason = "Maintainer not found"
        end
      end
    end
    env.res_code = env.res_code or E_SYNTAX
  end
  setfenv(do_login, SCARY_FENV_META)
end

local do_extra do
  do_extra = function(env, tag) -- EE
    pcall(_, tag, env)
    env.res_code = env.res_code or OK
  end
end

local environments = {} -- here's where keyed by session id tables reside

-- logout
local do_logout do
  do_logout = function(env, tag)
    if not env.authorized then
      env.res_code = E_USE
    else
      env.res_code = LOGGED_OUT
      environments[env.session_id] = nil
      env.authorized = nil
    end
    return nil, true
  end
end

-- routing
local function ft_info(env)
  return {
    host__info    = partial_one(do_host_info, env),
    domain__info  = partial_one(do_domain_info, env),
    contact__info = partial_one(do_contact_info, env),
  }
end
local function ft_check(env)
  return {
    host__check    = partial_one(do_host_check, env),
    domain__check  = partial_one(do_domain_check, env),
    contact__check = partial_one(do_contact_check, env),
  }
end
local function ft_create(env)
  return {
    host__create    = partial_one(do_host_create, env),
    domain__create  = partial_one(do_domain_create, env),
    contact__create = partial_one(do_contact_create, env),
  }
end
local function ft_transfer(env, tag)
  env.op = tag.op or 'query'
  return {
    domain__transfer = partial_one(do_domain_transfer, env),
  }
end
local function ft_update(env, tag)
  return {
    host__update    = partial_one(do_host_update, env),
    domain__update  = partial_one(do_domain_update, env),
    contact__update = partial_one(do_contact_update, env),
  }
end
local function ft_extension(env, tag)
end
local function ft_delete(env, tag)
  env.force = tag.force or '0' -- delete hosts/domains ignoring associations
  return {
    host__delete    = partial_one(do_host_delete, env),
    domain__delete  = partial_one(do_domain_delete, env),
    contact__delete = partial_one(do_contact_delete, env),
  }
end
local function ft_renew(env, tag)
  return {
    domain__renew = partial_one(do_domain_renew, env),
  }
end
local function ft_command(env, tag)
  return {
    [0]       = function(tag)
      local clTRID = xml_find(tag, 'clTRID')
      clTRID = clTRID and clTRID[_T_V] or cltrid_new().."-AUTO"
      env.clTRID = clTRID
      env.extension = xml_find(tag, 'extension') -- rgp:restore will use it
    end,
    info      = partial_one(ft_info, env),
    check     = partial_one(ft_check, env),
    create    = partial_one(ft_create, env),
    transfer  = partial_one(ft_transfer, env),
    update    = partial_one(ft_update, env),
    renew     = partial_one(ft_renew, env),
    extension = partial_one(ft_extension, env),
    delete    = partial_one(ft_delete, env),
    poll      = partial_one(do_poll, env),
    login     = partial_one(do_login, env),
    logout    = partial_one(do_logout, env),
    [-1]      = partial_one(do_extra, env),
    [true]    = function(tag)
      if tag[_T_N] ~= 'clTRID' then
        env.res_code = RES.E_UNKNOWN
      end
    end,
  }
end

local function do_check_auth(env, tag)
  if not env.authorized then
   -- look for either epp->hello or epp->command->log{in,out}
    tag = tag[_T_N] and tag[_T_V]
    local cmd = tag and tag[_T_N]
    if cmd ~= "hello" then
      cmd = tag[_T_V] and tag[_T_V][_T_N]
      if cmd ~= "login" and cmd ~= "logout" then
        env.res_code = E_AUTHENTICATION--_CLOSE
        return nil, true
      end
    end
  end
end

local make_response do
  local XML_HEADER = XML_HEADER
  local XMLNS_EPP_ROOT = XMLNS_EPP_ROOT

  make_response = function(env)
    if env.res_xml then return end -- got ready
    
    local _lang = env.lang or 'en' -- english of course
    local _code = env.res_code or OK
    local response_body = { response }
    local result_body = { result, code = _code,
      { msg, lang = _lang,
        RES[_code][_lang] or RES[_code].en, -- fallback to english FIXME add ua messages
      },
      (env.res_value or env.res_reason) and { extValue,
        { value,
          env.res_value,
        },
        { reason, lang = _lang,
          env.res_reason,
        }
      }
    }
  
    local trid = { trID,
      { clTRID, env.clTRID },
      { svTRID, env.uuid .. '/' .. env.svTRID_seq },
    }
  
    -- those are empty will not arrive in pairs()
    for _,v in pairs{ result_body, env.res_msg_q, env.res_data, trid } do
      response_body[1+#response_body] = v
    end
  
    env.res_xml = XML_HEADER .. xml.new{ epp, xmlns = XMLNS_EPP_ROOT,
                                  response_body
                                }:str()
  end
  setfenv(make_response, SCARY_FENV_META)
end

function process_epp_xml(session_id, xml_data)
  local xml_data = type(xml_data) == 'table' and xml_data or xml.eval(xml_data)

  if not xml_data then
    -- TODO log "MALFORMED XML"
    return
  end

  local env = environments[session_id]
  if not env then
    local matches = session_id:gmatch "[^^]+"
    local uuid, ip, time = matches(), matches(), matches()
    -- FIXME check how many connects from this IP etc
    env = {
      session_id = session_id, -- keep it too for logout
      uuid = uuid,
      ip = ip, -- unpack_ip(ip), -- HERE FIXME TODO well - maybe just use it packed and that's all? now we're not plan to do some subnet banning\access etc
      connect_time = unpack_time(time),
      processing_state = true, -- switch to false alongside with setting res_code in case of error -- HERE do we need it?
      svTRID_seq = 1,
    }
    environments[session_id] = env
  else
    -- FIXME check for session idle times etc? or do it from outer cycle? THINK THINK!
  end

  table_hash_merge(env, {
    raw = xml_data,
    request_time = fmt_time(),
    svTRID_seq = env.svTRID_seq + 1,
  }, true)
  env.res_code = nil
  -- cleanup current response data
  table_hash_wipe(env,
    'res_xml', 'res_value', 'res_reason', 'res_data', 'res_msg_q'
  )

  if xml_data[_T_N] == 'hello' then -- direct syntetic hello spawned by us
    do_hello(env, xml_data)
  else
    local ft_epp = {
      [0]     = partial_one(do_check_auth, env),
      command = partial_one(ft_command, env),
      hello   = partial_one(do_hello, env),
      [true]  = function(tag) env.res_code = RES.E_UNKNOWN end,
    }

    local status, err = pcall(xmliter_switch, xml_data, ft_epp)
    if not status then
      env.res_code = E_FAILED
    end

    make_response(env)
  end
  return env
end

module('requests_processor', package.seeall)

