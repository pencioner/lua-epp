local response_codes = {
  OK = 1000,
  [1000] = {
    en = "Command completed successfully",
    ru = "Команда выполнена успешно",
    ua = "Команду виконано успішно",
  },

  PENDING = 1001,
  [1001] = {
    en = "Command completed successfully; action pending",
    ru = "Команда выполнена успешно, действие отложено",
    ua = "Команду виконано успішно, дію відкладено",
  },

  EMPTY_Q = 1300,
  [1300] = {
    en = "Command completed successfully; no messages",
    ru = "Команда выполнена успешно, нет сообщений",
    ua = "Команду виконано успішно, повідомлень немає",
  },

  READ_Q = 1301,
  [1301] = {
    en = "Command completed successfully; ack to dequeue",
    ru = "Команда выполнена успешно, сообщение исключено из очереди",
    ua = "Команду виконано успішно, повідомлення видалене з черги",
  },

  LOGGED_OUT = 1500,
  [1500] = {
    en = "Command completed successfully; ending session",
    ru = "Команда выполнена успешно, завершение сессии",
    ua = "Команду виконано успішно, завершення сесії",
  },


  E_UNKNOWN = 2000,
  [2000] = {
    en = "Unknown command",
    ru = "Неизвестная команда",
    ua = "Невідома команда",
  },

  E_SYNTAX = 2001,
  [2001] = {
    en = "Command syntax error",
    ru = "Синтаксическая ошибка команды",
    ua = "Синтаксична помилка команди",
  },

  E_USE = 2002,
  [2002] = {
    en = "Command use error",
    ru = "Ошибка в использовании команды",
    ua = "Помилка у використанні команди",
  },

  E_PARAM_MISSING = 2003,
  [2003] = {
    en = "Required parameter missing",
    ru = "Отсутствует обязательный параметр",
    ua = "Відсутній обов'язковий параметр",
  },

  E_PARAM_RANGE = 2004,
  [2004] = {
    en = "Parameter value range error",
    ru = "Ошибка в значении параметра",
    ua = "Помилка в значенні параметра",
  },

  E_PARAM_VALUE = 2005,
  [2005] = {
    en = "Parameter value syntax error",
    ru = "Синтаксическая ошибка в значении параметра",
    ua = "Синтаксична помилка в значенні параметра",
  },

  E_UNIMPLEMENTED_OPT = 2102,
  [2102] = {
    en = "Unimplemented option",
    ru = "Недопустимая опция",
    ua = "Неприпустима опція",
  },

  E_RENEW_NOT_ELIGIBLE = 2105,
  [2105] = {
    en = "Object is not eligible for renewal",
    ru = "Объект не подлежит продлению",
    ua = "Об'єкт не підлягає подовженню",
  },

  E_TRANSF_NOT_ELIGIBLE = 2106,
  [2106] = {
    en = "Object is not eligible for transfer",
    ru = "Объект не подлежит перевод",
    ua = "Об'єкт не підлягає переведенню",
  },


  E_AUTHENTICATION = 2200,
  [2200] = {
    en = "Authentication error",
    ru = "Ошибка аутентификации",
    ua = "Помилка аутентифікації",
  },

  E_AUTHORIZATION = 2201,
  [2201] = {
    en = "Authorization error",
    ru = "Ошибка авторизации",
    ua = "Помилка авторизації",
  },
  
  E_AUTHINFO = 2202,
  [2202] = {
    en = "Invalid authorization information",
    ru = "Недействительные данные авторизации",
    ua = "Недійсні дані авторизації",
  },


  E_PENDING = 2300,
  [2300] = {
    en = "Object pending transfer",
    ru = "Объект в ожидании перевода",
    ua = "Об'єкт в очікуванні переведення",
  },

  E_NOT_PENDING = 2301,
  [2301] = {
    en = "Object not pending transfer",
    ru = "Отсутствие запроса на перевод объекта",
    ua = "Відсутній запит на переведення об'єкта",
  },

  E_EXISTS = 2302,
  [2302] = {
    en = "Object already exists",
    ru = "Объект уже существует",
    ua = "Об'єкт вже існує",
  },

  E_NOT_EXISTS = 2303,
  [2303] = {
    en = "Object does not exist",
    ru = "Объект не существует",
    ua = "Об'єкт не існує",
  },

  E_STATUS_PROHIBITE = 2304,
  [2304] = {
    en = "Object status prohibits operation",
    ru = "Статус объекта не позволяет совершение операции",
    ua = "Статус об'єкта забороняє здійснення операції",
  },

  E_ASSOC_PROHIBITE = 2305,
  [2305] = {
    en = "Object association prohibits operation",
    ru = "Объектные связи не допускают совершение операции",
    ua = "Зв'язки об'єкта не дозволяють здійснення операції",
  },

  E_PARAM_POLICY = 2306,
  [2306] = {
    en = "Parameter value policy error",
    ru = "Значение параметра не соответствует правилам",
    ua = "Значення параметра не відповідає правилам",
  },

  E_SERVICE_UNIMPLEMENTED = 2307,
  [2307] = {
    en = "Unimplemented object service",
    ru = "Данный объект не обслуживается",
    ua = "Даний об'єкт не обслуговується",
  },

  E_DATA_POLICY = 2308, -- currently unused
  [2308] = {
    en = "Data management policy violation",
    ru = "Данные не соответствуют правилам",
    ua = "Дані не відповідають правилам",
  },

  E_REJECTED = 2309,
  [2309] = {
    en = "Application rejected",
    ru = "Заявка отклонена",
    ua = "Заявка відхилена",
  },


  E_FAILED = 2400,
  [2400] = {
    en = "Command failed",
    ru = "Ошибка выполнения команды",
    ua = "Збій при виконанні команди",
  },


  E_FAILED_CLOSING = 2500, -- currently unused
  [2500] = {
    en = "Command failed; server closing connection",
    ru = "Ошибка выполнения команды, сервер закрывает соединение",
    ua = "Збій при виконанні команди, сервер закриває з'єднання",
  },

  E_AUTHENTICATION_CLOSE = 2501,
  [2501] = {
    en = "Authentication error; server closing connection",
    ru = "Ошибка аутентификации, сервер закрывает соединение",
    ua = "Помилка аутентифікації, сервер закриває з'єднання",
  },

  E_SESSION_LIMIT = 2502,
  [2502] = {
    en = "Session limit exceeded; server closing connection",
    ru = "Превышено количество одновременных сессий, сервер закрывает соединение",
    ua = "Перевищено кількість одночасних сесій, сервер закриває з'єднання",
  },
}

return response_codes

