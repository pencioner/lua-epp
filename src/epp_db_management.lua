local Ornament = require 'ornament'
local _ = Ornament.ColumnTypes
local   String,   Enum,   Time,   Integer,   Referring,   Referenced
    = _.String, _.Enum, _.Time, _.Integer, _.Referring, _.Referenced

require 'epp_settings'

local orn = Ornament.new { user = EPP_DB_USER, password = EPP_DB_PWD }
local conn = orn:connect { database = EPP_DB }
conn.handle:autocommit(false)


local epp_maintainer = conn:table "epp_maintainer" {
  id_m              = Integer(),
  m_id              = String(),
  m_organization    = String(),
  m_authInfo        = String(),
  m_clID_m          = Referring(epp_maintainer, 'id_m'),
  m_crID_m          = Referring(epp_maintainer, 'id_m'),
  m_upID_m          = Referring(epp_maintainer, 'id_m'),
  m_crDate          = Referring(epp_maintainer, 'id_m'),
  m_upDate          = Time(),
  m_trDate          = Time(),
  m_epp_settings    = String(),
  m_other_settings  = String(),
}

local epp_contact = conn:table "epp_contact" {
  id_c        = Integer(),
  c_roid      = String(),
  c_id        = String(),
  c_authInfo  = String(),
  c_email     = String(),
  c_crDate    = Time(),
}
local epp_cpi = conn:table "epp_contact_postal_info" {
  cpi_contact_c = Referring(epp_contact, 'id_c'),
}

local epp_domain = conn:table "epp_domain" {
  id_d = Integer(),
  d_roid  = String(),
  d_name = String(),
  d_authInfo = String(),
  d_registrant_c = Referring(epp_contact, 'id_c'),
  d_clID_m = Referring(epp_maintainer, 'id_m'),
  d_crID_m = Referring(epp_maintainer, 'id_m'),
  d_dlID_m = Referring(epp_maintainer, 'id_m'),
  d_upID_m = Referring(epp_maintainer, 'id_m'),
  d_crDate = Time(),
  d_exDate = Time(),
  d_upDate = Time(),
  d_trDate = Time(),
}

local epp_domain_contact = conn:table "epp_domain_contact_lnk" {
  dc_domain_d  = Referring(epp_domain, 'id_d'),
  dc_contact_c = Referring(epp_contact, 'id_c'),
  dc_contact_type = Enum()
}
local epp_host = conn:table "epp_host" {} -- TODO

local epp_domain_ns = conn:table "epp_domain_ns_lnk" {} -- TODO

local epp_domain_transfer = conn:table "epp_domain_transfer" {} -- TODO

local epp_message_queue = conn:table "epp_message_queue" {} -- TODO

local epp_payments = conn:table "epp_payments" {} -- TODO

local get_maintainer_name = function(mnt_id, default_name)
  local mnt = epp_maintainer:select{ id_m = mnt_id }
  return mnt and mnt.m_id or default_name
end

local admin_id, get_admin_id_m
do
  get_admin_id_m = function()
    if not admin_id then
      admin_id = epp_maintainer:select{ m_id = EPP_ADMIN_MNT_ID }.id_m
    end
    return admin_id
  end
end


local db_array_split  = Ornament.Helpers.array_parse
local db_array_merge  = Ornament.Helpers.array_compose

local check_status_gen = function(method, if_true, if_false)
  return function (statusi, str_status)
    for _,v in pairs(statusi) do
      if method(str_status, v) then return if_true end
    end
    return if_false
  end
end
local check_status_present = check_status_gen(string.find, true, false)
local check_status_absent  = check_status_gen(string.find, false, true)

local modify_status_string = function(true_add_false_remove, str, status)
  local statusi = db_array_split(str)
  for i,v in ipairs(statusi) do
    if v == status then -- remove anyway, add only if we are adding
      table.remove(statusi, i)
    end
  end

  if true_add_false_remove then
    statusi[1+#statusi] = status
  end

  return db_array_merge(statusi)
end

local remove_status_from_string = function(str, status)
  return modify_status_string(false, str, status)
end
local add_status_to_string = function(str, status)
  return modify_status_string(true, str, status)
end
-- the shitcodelet here btw, FIXME make good obe without such hardcodelets
local check_and_set_ok_status_on_string = function(str)
  if not str:find'ok' then
    if str == '{}' then
      return '{ok}'
    elseif str == '{linked}' then
      return '{ok,linked}'
    end
  else
    if str ~= '{ok}' and str ~= '{ok,linked}' and str ~= '{linked,ok}' then
      return remove_status_from_string(str, 'ok')
    end
  end
  return str
end

local modify_status_on_obj_gen = function( -- ANCHOR:REFDB
    db_table, -- TODO FIXME refactor ornament so records use refs to db tables
    status_fld, -- x_statusi
    serial_fld, -- this will be removed with beforementioned refactor
    status_action -- the function
  )

  return function(row, status)
    local new_statusi = status_action(row[status_fld], status)
    row[status_fld] = new_statusi -- update row obj before sync with DB
    return db_table:where{ [serial_fld] = row[serial_fld] }
                   :update{ [status_fld] = new_statusi }
  end
end
local remove_status_on_obj_gen = function(db_table, status_fld, serial_fld)
  return modify_status_on_obj_gen(
    db_table, status_fld, serial_fld,
    remove_status_from_string
  )
end
local add_status_on_obj_gen = function(db_table, status_fld, serial_fld)
  return modify_status_on_obj_gen(
    db_table, status_fld, serial_fld,
    add_status_to_string
  )
end
local remove_status_from_domain = remove_status_on_obj_gen(epp_domain, 'd_statusi', 'id_d')
local remove_status_from_host = remove_status_on_obj_gen(epp_host, 'h_statusi', 'id_h')
local remove_status_from_contact = remove_status_on_obj_gen(epp_contact, 'c_statusi', 'id_c')
local add_status_to_domain = add_status_on_obj_gen(epp_domain, 'd_statusi', 'id_d')
local add_status_to_host = add_status_on_obj_gen(epp_host, 'h_statusi', 'id_h')
local add_status_to_contact = add_status_on_obj_gen(epp_contact, 'c_statusi', 'id_c')

-- third arg may be table.topsy_turvy() from db_array_split() one
-- and will be modified within this method
local db_array_add_remove = function(add, rem, raw_or_topsy)
  local topsy = 'table' == type(raw_or_topsy) and raw_or_topsy
                   or table.topsy_turvy(db_array_split(raw_or_topsy))
  for _,r in pairs(rem) do
    topsy[r] = nil
  end
  for _,a in pairs(add) do
    topsy[a] = a
  end

  return table.hash_keys(topsy)
end


local update_record_gen = function( -- ANCHOR:REFDB
    db_table,  -- TODO FIXME refactor ornament so records use refs to db tables
    serial_fld -- this will be removed with beforementioned refactor
  )

  return function(obj_record, upd_data, do_nullify)
    local do_update = function(_upd_data, _do_nullify)
      local where =db_table:where{ [serial_fld] = obj_record[serial_fld] }
      return _do_nullify and where:nullify(_upd_data)
                          or where:update(_upd_data)
    end

    -- kind of auto-partial - no second arg means return partial
    return upd_data and do_update(upd_data, do_nullify) or do_update
  end
end

       local update_host = update_record_gen(epp_host, 'id_h')
     local update_domain = update_record_gen(epp_domain, 'id_d')
    local update_contact = update_record_gen(epp_contact, 'id_c')
 local update_maintainer = update_record_gen(epp_maintainer, 'id_m')
local update_domtransfer = update_record_gen(epp_domain_transfer, 'id_dt')

return {
  tables = {
             epp_cpi = epp_cpi,
            epp_host = epp_host,
          epp_domain = epp_domain,
         epp_contact = epp_contact,
       epp_domain_ns = epp_domain_ns,
     epp_mnt_contact = epp_mnt_contact,
   epp_message_queue = epp_message_queue,
  epp_domain_contact = epp_domain_contact,
 epp_domain_transfer = epp_domain_transfer,

   epp_notifications = epp_notifications,
      epp_maintainer = epp_maintainer,
  },


  db_connection   = conn,
  ornament_inst   = orn,

  helpers = {
      get_maintainer_name = get_maintainer_name,
           get_admin_id_m = get_admin_id_m,
           db_array_split = db_array_split,
           db_array_merge = db_array_merge,
      db_array_add_remove = db_array_add_remove,

    remove_domain_status  = remove_status_from_domain,
       remove_host_status = remove_status_from_host,
    remove_contact_status = remove_status_from_contact,
        add_domain_status = add_status_to_domain,
          add_host_status = add_status_to_host,
       add_contact_status = add_status_to_contact,

  check_set_ok_str_status = check_and_set_ok_status_on_string,
        remove_str_status = remove_status_from_string,
           add_str_status = add_status_to_string,

     check_status_present = check_status_present,
      check_status_absent = check_status_absent,

              update_host = update_host,
            update_domain = update_domain,
           update_contact = update_contact,
        update_maintainer = update_maintainer,
       update_domtransfer = update_domtransfer,
  }
}

