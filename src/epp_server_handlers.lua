require 'epp_settings'

local lanes = require'lanes'.configure(EPP_LANES_CONFIG)
local linda = lanes.linda

local yield = coroutine.yield

-- This linda will be used to exchange data between tcp server (TS)
-- and main XML processing routine (XPR). The protocol is as follows:
--  1) When TS opens the session, it tells ths session_id to XPR
--  2) XPR adds session_id as key to listen on this linda
--  3) XML received by this key are processed, and result are sent
--     to the same key concatenated with string "R"
--  4) when TS removes socket, it tells XPR to remove the key from
--     set of linda keys
-- ts_xpr_mq -> the server to xml processing routine message queue
local ts_xpr_mq = linda()
local TS_XPR_ADD_SESSION = "SESSION_START"
local TS_XPR_DEL_SESSION = "SESSION_END"


local function ip_throttling(env, sock, ip, port)
  -- TODO: throttling limits etc
  return true
end

local init_environment = function(env)
  env.output = {}
  local session_id = env.session_id
  ts_xpr_mq:send(TS_XPR_ADD_SESSION, session_id)
  if not env.listening then
    -- we are busy with generate and set session_id
    ts_xpr_mq:send(session_id, "<hello session='"..session_id.."'/>")
  end
end

local shutdown_close = function(env)
  yield(true)
  ts_xpr_mq:send(TS_XPR_DEL_SESSION, env.session_id)
end

local recv_data = function(env)
  local bitstr = require 'bitstring'
  local length, chunk = 0, ""
  local data, shortdata

  while true do
    -- to process rest in just another pass
    data = data or yield(true)

    if shortdata then 
      data, shortdata = shortdata..data, nil
    end

    if data == "" then
      data = nil
    elseif length == 0 then
      if #data > 4 then -- 4 bytes indicate xml req chunk length
        length, data = bitstr.unpack("32:int", data), data:sub(5)
      else
        shortdata, data = data, nil
      end
    elseif length > #chunk then
      local rest = length - #chunk
      chunk, data = chunk..data:sub(1, rest), data:sub(1+rest)
    end

    if length ~= 0 and length == #chunk then -- received complete EPP message
      ts_xpr_mq:send(env.session_id, chunk) -- let XPR process it btw
      length, chunk = 0, ""
    end
  end
end

local data_ready = function(env)
  local make_epp_message = require 'snippets'.make_epp_message
  local output = env.output
  local sess_id_key = env.session_id.."-R"

  while true do
    -- 0 seconds timeout so we wouldn't block if no response ready yet
    local key, response = ts_xpr_mq:receive(0, sess_id_key)
    output[1+#output] = key and make_epp_message(response)
    yield(#output > 0)
  end
end

local data_prepare = function(env)
  local output = env.output
  while true do
    yield(output[1] or "")
  end
end

local data_sent = function(env)
  local output = env.output
  while true do
    local sent_bytes = yield(true) or 0
    output[1] = output[1]:sub(1+sent_bytes)
    if 0 == #output[1] then
      table.remove(output, 1)
    end
  end
end

local listener_error = function(env)
  while true do
    print("EPP LISTENING socket ERROR:", yield(true)) -- FIXME TODO HERE
  end
end

local acceptor_error = function(env)
  while true do
    print("EPP CONNECTED socket ERROR:", yield(true)) --- FIXME
  end
end

local tls_options = {
  mode        = "server",
  protocol    = "tlsv1",
  key         = EPP_TLS_DIR.."/serverAkey.pem",
  certificate = EPP_TLS_DIR.."/serverA.pem",
  cafile      = EPP_TLS_DIR.."/rootA.pem",
  verify      = {"peer", "fail_if_no_peer_cert"},
  options     = {"all", "no_sslv2"},
}

return {
  tls_options = tls_options,
  ip_throttling = ip_throttling,
  init_environment = init_environment,
  listener_error = listener_error,
  acceptor_error = acceptor_error,
  shutdown_close = shutdown_close,
  recv_data = recv_data,
  data_prepare = data_prepare,
  data_ready = data_ready,
  data_sent = data_sent,

  ts_xpr_mq = ts_xpr_mq,
  TS_XPR_ADD_SESSION = TS_XPR_ADD_SESSION,
  TS_XPR_DEL_SESSION = TS_XPR_DEL_SESSION,
}

