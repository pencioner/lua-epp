do
  -- memoizing suffix adder factory
  local memoize_store = {}
  local function suffix_lambda(suffix)
    local suffix_lambda = memoize_store[suffix]
    if not suffix_lambda then
      suffix_lambda = function(str) return str .. suffix end
      memoize_store[suffix] = suffix_lambda
    end
    return suffix_lambda
  end
end

local function is_integer(what)
  return type(what) == 'number' and what % 1 == 0
end

local function max(v1, v2)
  return v1 > v2 and v1 or v2
end

do -- some table extensions
  local table = table

  local function table_hash_merge(tbl1, tbl2, in_place_modification)
    local rv = in_place_modification and tbl1 or {}
    if not in_place_modification then
      for k,v in pairs(tbl1) do
        rv[k] = v
      end
    end
  
    for k,v in pairs(tbl2) do
      rv[k] = v
    end
  
    return rv
  end
  rawset(table, 'hash_merge', table_hash_merge)

  local function table_array_merge(tbl1, tbl2, in_place_modification)
    local rv = in_place_modification and tbl1 or {}
    if not in_place_modification then
      for i,v in ipairs(tbl1) do
        rv[i] = v
      end
    end

    local cur_size = #rv
    for i,v in ipairs(tbl2) do
      rv[i+cur_size] = v
    end

    return rv
  end
  rawset(table, 'array_merge', table_array_merge)

  local function table_hash_len(tbl)
    local cnt = 0
    for k,v in pairs(tbl) do cnt = 1 + cnt end
    return cnt
  end
  rawset(table, 'hash_len', table_hash_len)

  local function table_hash_wipe(tbl, ...)
    local keys = {...}
    for _,v in ipairs(keys) do
      tbl[v] = nil
    end
  end
  rawset(table, 'hash_wipe', table_hash_wipe)

  local function table_hash_slice(tbl, ...)
    local keys, vals, idx = {...}, {}, 0
    for _,v in ipairs(keys) do
      idx = idx + 1
      vals[idx] = tbl[v]
    end
    return unpack(vals, 1, idx)
  end
  rawset(table, 'hash_slice', table_hash_slice)

  local function table_hash_keys(tbl)
    local rv = {}
    for key,_ in pairs(tbl) do
      if not is_integer(key) then
        rv[1+#rv] = key
      end
    end
    return rv
  end
  rawset(table, 'hash_keys', table_hash_keys)

  local function table_topsy_turvy(tbl)
    local rv = {}
    for val,key in ipairs(tbl) do
      rv[key] = val
    end
    return rv
  end
  rawset(table, 'topsy_turvy', table_topsy_turvy)

  local function table_set_complement(setA, setB) -- does A \ B (complement)
    local rv = {}
    local setB_inside_out = table_topsy_turvy(setB)

    for _,val in ipairs(setA) do
      rv[1+#rv] = not setB_inside_out[val] and val or nil
    end

    return rv
  end
  rawset(table, 'set_complement', table_set_complement)

  local function table_set_union(setA, setB)
    local setA_inside_out = table_topsy_turvy(setA)

    for _,val in ipairs(setA) do rv[1+#rv] = val end
    for _,val in ipairs(setB) do
      rv[1+#rv] = not setA_inside_out[val] and val or nil
    end
  end
  rawset(table, 'set_union', table_set_union)
end -- some table extensions

local posix = require 'posix'
local pcrypt = posix.crypt
local function posix_crypt(pwd, salt)
  local status, rv = pcall(pcrypt, pwd, salt)
  return status and rv or nil
end

-- some EPP-specific etc extensions

local date = require'date'
local function format_datetime_ISO8601(datetime) -- ISO 8601
  if not datetime then
    local d = posix.gettimeofday()
    datetime = d.sec + d.usec/1000000
  end
  local d = date(datetime or true)
  return d:fmt("%FT%t%v"), d
end
local function format_datetime_whois(datetime)
  if datetime then
    return date(datetime):fmt("%F %T%q")
  end
end
local function date_add_years(datetime, how_much)
  return date(datetime):addyears(how_much)
end
local function date_add_days(datetime, how_much)
  return date(datetime):adddays(how_much)
end

local bitstring = require 'bitstring'
local bitstring_pack = bitstring.pack
local bitstring_unpack = bitstring.unpack
local bitstring_hexstream = bitstring.hexstream
local bitstring_fromhexstream = bitstring.fromhexstream

local inet_pton_module = require"thin.libc-inet_pton"
local AF_INET, AF_INET6 = inet_pton_module.AF_INET, inet_pton_module.AF_INET6
local inet_pton = getmetatable(inet_pton_module).__call -- performance optmz

local function check_ip(ip_str, do_unpack)
  local is_v6 = ip_str:find":"
  local ip_buf = inet_pton(nil, is_v6 and AF_INET6 or AF_INET, ip_str)

  if 0 ~= #ip_buf then
    local ip_unpack = do_unpack and { bitstring_unpack(
      is_v6 and "16:int,16:int,16:int,16:int,16:int,16:int,16:int,16:int"
            or "8:int,8:int,8:int,8:int",
      ip_buf
    ) }
    return is_v6 and 'v6' or 'v4', ip_unpack or ip_buf
  end
end

local function pack_ip(ip_str)
  local ip_family, ip_buf = check_ip(ip_str)

  if ip_family then
    return ip_family..bitstring_hexstream(ip_buf)
  end
end

local inet_ntop_module = require"thin.libc-inet_ntop"
local inet_ntop = getmetatable(inet_ntop_module).__call -- performance optmz
local function unpack_ip(hex) -- TODO FIXME unchecked yet
  local ip_family, ip_hex = hex:sub(1,2), hex:sub(3)

  
  return inet_ntop(
    ip_family == 'v6' and AF_INET6 or AF_INET,
    bitstring_fromhexstream(ip_hex)
  )
end

local function pack_time(epoch_secs)
  return ("%08x"):format(epoch_secs or os.time())
end

local function unpack_time(hex)
  return tonumber("0x"..hex)
end

local uuid = require 'uuid'
local uuid_new = uuid.new
local epp_uuid = function() -- TODO comment for time hidden usage
  return (uuid_new"t"):sub(1,18)..(uuid_new""):sub(20)
end

local function generate_uid(ip)
  return epp_uuid() ..'^'.. pack_ip(ip) ..'^'.. pack_time()
end

local function generate_cltrid()
  return epp_uuid():gsub("-", "")
end
local function generate_roid(suffix)
  return epp_uuid():gsub('%-','_') .. '-' .. (suffix or "Z"):upper()
end
local function generate_roid_contact() return generate_roid"C" end
local function generate_roid_domain() return generate_roid"D" end
local function generate_roid_host() return generate_roid"H" end

local function generate_auth() return uuid_new():sub(-12) end

local function make_epp_message(message_xml) -- append length, rfc5734
  local len_prefix = bitstring_pack("32:int", #message_xml)
  return len_prefix .. message_xml
end


return {
                       --   --                        ,
                     --       --                      ,
                    --         --                     ,
                    --         --                     ,
                       qq = qq                        ,
                    qq1   =   qq1                     ,
                 epp_uuid = epp_uuid                  ,
               is_integer = is_integer                ,
              posix_crypt = posix_crypt               ,
            generate_roid = generate_roid             ,
            suffix_lambda = suffix_lambda             ,
                      max = max                       ,
                  pack_ip = pack_ip                   ,
                 check_ip = check_ip                  ,
                unpack_ip = unpack_ip                 ,
                pack_time = pack_time                 ,
              unpack_time = unpack_time               ,
             generate_uid = generate_uid              ,
            generate_auth = generate_auth             ,
            date_add_days = date_add_days             ,
           date_add_years = date_add_years            ,
          generate_cltrid = generate_cltrid           ,
         make_epp_message = make_epp_message          ,
       generate_roid_host = generate_roid_host        ,
     generate_roid_domain = generate_roid_domain      ,
    generate_roid_contact = generate_roid_contact     ,
    format_datetime_whois = format_datetime_whois     ,
  format_datetime_ISO8601 = format_datetime_ISO8601   ,

} -- the QUEEN

