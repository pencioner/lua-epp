-- this is rather not so complete now but useful realization of function partials

local function make_partial_N(f, ...)
  local args, args_n = {...}, select('#', ...)
  return function(...)
    local rest_args, rest_args_n = {...}, select('#', ...)
    local all_args = {}
    for i=1,1+args_n do all_args[i] = args[i] end
    for i=1,1+rest_args_n do all_args[i+args_n] = rest_args[i] end
    return f(unpack(all_args))
  end
end

-- optimized partivular cases
local function make_partial_1(f, arg1)
  return function(...)
    return f(arg1, ...)
  end
end

local function make_partial_2(f, arg1, arg2)
  return function(...)
    return f(arg1, arg2, ...)
  end
end

local function make_partial_3(f, arg1, arg2, arg3)
  return function(...)
    return f(arg1, arg2, arg3, ...)
  end
end

local function make_partial_4(f, arg1, arg2, arg3, arg4)
  return function(...)
    return f(arg1, arg2, arg3, arg4, ...)
  end
end

return {
  make_partial_1 = make_partial_1,
  make_partial_2 = make_partial_2,
  make_partial_3 = make_partial_3,
  make_partial_4 = make_partial_4,
  make_partial_N = make_partial_N,
}


