-- no automatic schema tracking, all should be explicitly exposed

require 'typesng'
local sn = require 'snippets'
local is_integer = sn.is_integer

local DBI = require 'DBI'

local type = type
local table, next, unpack = table, next, unpack
local rawset, rawget = rawset, rawget
local setmetatable = setmetatable


--- TMP FIXME HERE temporary simple logger
local _lf = io.open("/tmp/transfer.log", "w")
function lllog(...)
  local a = {}
  for k,v in pairs{...} do
    a[1+#a] = v
  end
  _lf:write(table.concat(a, '\t'))
  _lf:write '\n'
end

local pairs, ipairs = pairs, ipairs

local print=lllog
--local print=print
local assert=assert --debug
module(...)

local concatenate_fields_comma = function(field_names)
  error'TODO'
end

local make_where_constraints = function(constr)
  error'TODO'
end

local Ornament_instance_mt = {
  connect = function(self, args)
    local opts = {}

    for _, opt in ipairs{'driver', 'host', 'port',
                         'user', 'password', 'database'} do
      opts[opt] = args[opt] or self[opt]
    end

    return Connection.new(self, opts, { -- args.connect true by default though
      connect = type(args.connect) == 'nil' or args.connect, autocommit = args.autocommit
    })
  end,
}
Ornament_instance_mt.__index = Ornament_instance_mt -- loopback

local Ornament_instance_defaults = {
  driver   = 'PostgreSQL',
  host     = 'localhost',
  port     = 5432,
  user     = 'postgres',
  password = ':P',
  database = '',
}

function new(db_options)
  local ornament = table.hash_merge(Ornament_instance_defaults, db_options)
  return setmetatable(ornament, Ornament_instance_mt)
end

local Connection_instance_mt = {
  __gc = function(self)
    self.handle:close()
  end,

  table = function(self, name, options)
    return function(options)
      local tbl = Table.new(self, name, options)
      self.tables[name] = tbl
      return tbl
    end
  end,

  keepalive = function(self, auto_reconnect, retry)
    local is_alive = self.handle:ping()

    if not is_alive and auto_reconnect then
      -- note the Connection:new, not Connection.new below. microhack. we don't need no ornament, use self
      local new_db = Connection:new(unpack(self.constructed_with))
      if new_db then
        setmetatable(new_db, nil)
      else -- bad bad bad. TODO FIXME think about some retry count or so...
        -- not reconnected, so:
        auto_reconnect = false
      end
    end

    return not auto_reconnect and is_alive or self:keepalive(false, true)
  end,

  close = function(self)
    return self.handle:close()
  end,
  commit = function(self)
    return self.handle:commit()
  end,
  rollback = function(self)
    return self.handle:rollback()
  end,
}
Connection_instance_mt.__index = Connection_instance_mt

Connection = {
  new = function(ornament, args, add_args)
    local conn

    local db_handle, err = DBI.Connect(
      args.driver, args.database, args.user, args.password, args.host, args.port
    )

    if db_handle then
      conn = {
        constructed_with = { args, add_args }, -- keep for auto reconnect
        handle   = db_handle,
        name     = args.database,
        ornament = ornament,
        tables   = {},
      }

      setmetatable(conn, Connection_instance_mt)
    end

    return conn, err
  end,
}

-- in favor of DRY for both select and update
local refine_keys_values = function(...)
  local keys, values, array_values = {}, {}, {}

  for _, tbl in ipairs{...} do
    for k,v in pairs(tbl or {}) do
      if is_integer(k) then
        array_values[1+#array_values] = v
      else
        keys[1+#keys] = k
        values[1+#values] = v
      end
    end
  end

  return keys, values, array_values
end

local prepare_where_or_similar = function(concat_with, ...)
  local fld_preds, fld_vals, arr_preds = refine_keys_values(...)

  for i,v in ipairs(fld_preds) do
    fld_preds[i] = v .. '=?'
  end
  table.array_merge(fld_preds, arr_preds, true)
  local sql = 0 ~= #fld_preds and table.concat(fld_preds, concat_with) or nil
  return sql, fld_vals
end

-- TODO refactor to make more consistent. AND's OR's etc
local prepare_where_part = function(obj, predicates)
  local where_sql, and_vals, or_vals

  where_sql, and_vals = prepare_where_or_similar(' AND ', obj:where(predicates).__where)
  where_sql, or_vals  = prepare_where_or_similar(' OR ', { where_sql }, obj.__or_where or {})

  -- order of or_vals and and_vals should be same as above in prepare
  return where_sql or '1=1', table.array_merge(and_vals, or_vals, true)
end

local prepare_nullify = function(fields)
  local nullifies = {}
  for _,fld in ipairs(fields) do
    nullifies[1+#nullifies] = fld..'=NULL'
  end
  return table.concat(nullifies, ',')
end

local prepare_join_part = function(obj)
  local join_data = obj.__join or {}
  local tbl_name = obj.name
  local sql = { tbl_name }

  for _,join_item in ipairs(join_data) do
    tbl_name = join_item[1]
    if 'table' == type(tbl_name) then tbl_name = tbl_name.name end
    local left_col, right_col = next(join_item, 1) -- beware - only one key-val pair per join ;)
--[[    while is_integer(left_col) do -- beware - only one key-val columns pair per join ;)
      left_col, right_col = next(join_item)
    end
--]] -- this is intentionally left but commented for the case where we'd use the join_item[2]

    -- only inner join now TODO FIXME join_item[2]/ to indicate ?? !! join_item[2] will break next() call above
    -- as i use the 'table column prefix' for column names, there is no need of using `tbl.column`
    -- form of column reference (no collisions) btw. IT IS OK at least in this project
    -- BUT should be fixed for the common case though
    table.array_merge(sql, {'join', tbl_name, 'on', left_col, '=', right_col}, true)
  end

  return table.concat(sql, ' ')
end

local store_fill_method_array = function(store, idx, val)
  store[1+#store] = val
end
local store_fill_method_hashmap = function(store, what, val)
  if not is_integer(what) then
    store[what] = val
  end
end
local store_fill_method_combo = function(store, what, val)
  -- array part if integer (not distinct in some cases btw but ok for our cases)
  if is_integer(what) then
    store[1+#store] = val
  else
    store[what] = val
  end
end
local make_foreach_store_method = function(tbl_iter, store_fill_method, store)
  return function(tbl)
    for k,v in tbl_iter(tbl) do
      store_fill_method(store, k, v)
    end
  end
end
local __methods_by_store_type = {
  array   = { ipairs, store_fill_method_array },
  hashmap = { pairs,  store_fill_method_hashmap },
  combo   = { pairs,  store_fill_method_combo },
}

-- hell no. this is a factory of methods for store parts of sql for queries generator
local make_generic_mt_junk_store_method = function(store_type, store_name, preproc_func)
  local iter_method, store_fill_method = unpack(__methods_by_store_type[store_type])

  return function(obj, tbl_to_store)
    local rv = obj
    local tbl = preproc_func and preproc_func(tbl_to_store) or tbl_to_store
    local store = obj[store_name] or {}

    if tbl then
      -- upvalues methods were set up by store_type above
      make_foreach_store_method(iter_method, store_fill_method, store)(tbl)

      --[==[
       little explanation of what is being going here

       we create new table with our store, and set it to proxy to our
       real object so all methods from real object will be invoked 
       with `self' point to our proxy. this way method which use
       self.`store_name' will be using the store created inside this function
       thus allowing us to write the chains like
         obj:order_by{'a'}:where{c=123}:select_one{'*'}
       look to the table instance methods for real HOWTO code example
      --]==]
      if not obj[store_name] then
        rv = setmetatable({ [store_name] = store }, { __index = obj })
      end
    end

    return rv
  end
end
local make_array_mt_junk_store_method = function(store_name, preprocess_func)
  return make_generic_mt_junk_store_method('array', store_name, preprocess_func)
end
local make_hashmap_mt_junk_store_method = function(store_name, preprocess_func)
  return make_generic_mt_junk_store_method('hashmap', store_name, preprocess_func)
end
local make_combo_mt_junk_store_method = function(store_name, preprocess_func)
  return make_generic_mt_junk_store_method('combo', store_name, preprocess_func)
end

local lowercase_str_keys_mt = {
  __index = function(tbl, key) return rawget(tbl, key:lower()) end,
  __newindex = function(tbl, key, val) rawset(tbl, key:lower(), val) end,
}
local Table_instance_mt = {
  -- join arguments are: table name or obj, key-value pair of column names to join
  join     = make_array_mt_junk_store_method('__join',
    -- thus allowing for multiple joins via one call if passed as array of tables...
    function(tbl) if 'table' ~= type(tbl[1]) then return { tbl } end end
  ),

  order_by = make_array_mt_junk_store_method('__order_by'),
  group_by = make_array_mt_junk_store_method('__group_by'),
  limit    = make_array_mt_junk_store_method('__limit'),

  where    = make_combo_mt_junk_store_method('__where'),
  or_where = make_combo_mt_junk_store_method('__or_where'),

  find = function(self, fields_w_preds)
    local fields = {}
    if not fields_w_preds or 0 == #fields_w_preds then
      fields[1] = '*'
    else
      for i, f_name in ipairs(fields_w_preds) do
        fields_w_preds[i] = nil -- remove fields from preds as we'll go WHERE with the rest
        fields[1+#fields] = f_name
      end
    end

    local tbl_sql = prepare_join_part(self)
    local fields_sql = table.concat(fields, ',')
    local groups = self.__group_by
    local group_by_sql = groups and 'GROUP BY ' .. table.concat(groups, ',') or ''

    local orders = self.__order_by
    local order_by_sql = orders and 'ORDER BY ' .. table.concat(orders, ',') or ''
    -- at the time of calling prepare_where_part() fields_w_preds contains only predicates
    local where_sql, where_values = prepare_where_part(self, fields_w_preds)

    local limit = self.__limit
    local limit_sql = limit and 'LIMIT '..limit[#limit] or ''
    
    -- %fields_sql% %tbl_sql% %where_sql% %group_by_sql% %order_by_sql%
    local sql_query = ("SELECT %s FROM %s WHERE %s %s %s %s")
                      :format(fields_sql, tbl_sql, where_sql,
                         group_by_sql, order_by_sql, limit_sql
                      )

    -- TODO FIXME memoization anyone? BUT take care - investigate reusing sth pitfalls
    -- FIXME assert???
    local sth, msg
    sth, msg = self:prepare_statement(sql_query)
    if sth then
      local result
      result, msg = sth:execute(unpack(where_values))
      if result then
        rv = {}
--        local columns = sth:columns()
        for row in sth:rows(true) do
          setmetatable(row, lowercase_str_keys_mt)
          rv[1+#rv] = row
        end
      end
    end

    return rv, msg
  end,

  select = function(self, fields) -- selects one record
    local result, msg = self:limit{1}:find(fields)
    return result[1], msg
  end,

  insert = function(self, what)
    local result, sth, msg
    local fields, values = refine_keys_values(what)
    local sql_query

    if 0 ~= #fields then
      local tbl_sql = self.name
      local fields_sql = table.concat(fields, ',')
      local values_sql = ('?,'):rep(#fields):sub(1,-2)
      sql_query = ("INSERT INTO %s(%s) VALUES(%s)")
                  :format(tbl_sql, fields_sql, values_sql)

      sth, msg = self:prepare_statement(sql_query)
      if sth then
        result, msg = sth:execute(unpack(values))
      end
    end
    return result, msg
  end,

  update = function(self, what)
    local result, sth, msg

    local fields_sql, upd_values = prepare_where_or_similar(',', what)
    local where_sql, where_values = prepare_where_part(self, {})
    local tbl_sql = self.name
    local sql_query = ("UPDATE %s SET %s WHERE %s")
                      :format(tbl_sql, fields_sql, where_sql)

    sth, msg = self:prepare_statement(sql_query)
    if sth then
      result, msg = sth:execute(unpack(table.array_merge(upd_values, where_values, true)))
    end

    return result, msg
  end,

  nullify = function(self, fields)
    local result, sth, msg
    local where_sql, where_values = prepare_where_part(self, {})
    local fields_sql = prepare_nullify(fields)
    local tbl_sql = self.name
    local sql_query = ("UPDATE %s SET %s WHERE %s")
                      :format(tbl_sql, fields_sql, where_sql)
    sth, msg = self:prepare_statement(sql_query)
    if sth then
      result, msg = sth:execute(unpack(where_values))
    end
    return result, msg
  end,

  delete = function(self, where)
    local where_sql, where_values = prepare_where_part(self, where)
    local tbl_sql = self.name
    local sql_query = ("DELETE FROM %s WHERE %s")
                      :format(tbl_sql, where_sql)
    local sth, msg
    sth, msg = self:prepare_statement(sql_query)
    if sth then
      result, msg = sth:execute(unpack(where_values))
    end

    return result, msg
  end,

  get_connection = function(self)
    return self.connection and self.connection.handle
  end,

  prepare_statement = function(self, ...)
    local conn, err = self:get_connection()
    local sth
    if conn then
      sth, err = conn:prepare(...) 
    end
    return sth, err
  end,

  set_column_type = function(self, colname, typeobj)
    -- Table_schema_mt will deal with references types
    typeobj.table = self
    self.schema[colname] = typeobj
  end,
}

Table_schema_mt = {
  -- extend this behaviour if column type semantics demands it on assigning new column
  -- (f.e. on the way of development of automatic schema parsing - may be possible to
  -- ALTER TABLE on very new/changed type columns etc etc etc)
  __newindex = function(tbl, colname, typeobj)
    if "table" == type(typeobj) then
      typeobj.column = colname
      rawset(tbl, colname, typeobj)
    end
  end,
}

-- for shorthand access to columns via tbl instead of tbl.schema
Table_instance_mt.__index = setmetatable(Table_instance_mt, { __index = Table_schema_mt })
Table_instance_mt.__newindex = Table_instance_mt.set_column_type

--[[ TODO FIXME HERE - OK, refactor in the future... now no time for this...
local table_row_prefix_mt_factory = function(prefix)
  local prefix_dash = prefix..'_'
  return {
    __index 
  }
end
--]]

Table = {
  new = function(conn, name, schema, uniq_fld_prefix)
    local tbl_schema = setmetatable({}, Table_schema_mt)
    local tbl = setmetatable({
        connection = conn,
        name       = name,
        schema     = tbl_schema,
        uniq       = uniq_fld_prefix, -- see above about refactoring...
      }, setmetatable(Table_instance_mt, {__index = tbl_schema}))

    for colname, typeobj in pairs(schema) do
      tbl:set_column_type(colname, typeobj)
    end

    return tbl
  end,

}

local generic_lazy_constructor_factory = function(non_empty_constructor)
  return non_empty_constructor
    and function(self, ...) return non_empty_constructor(self, ...) end
    or function(self, ...) return {} end
end

-- TODO FIXME
local String_mt = {
  __call = generic_lazy_constructor_factory()
}
local Enum_mt = {
  __call = generic_lazy_constructor_factory()
}
local Time_mt = {
  __call = generic_lazy_constructor_factory()
}
local Integer_mt = {
  __call = generic_lazy_constructor_factory()
}

local referring_constructor = function(self, ref_table, ref_column)
  local ref_tbl, ref_column = ref_table, ref_column -- shadow upvalues
  local ref_tbl_name = 'table' == type(ref_tbl) and ref_tbl.name or ref_tbl
  local column_name, self_tbl -- memoizing upvalues

  return {
    ref_table = ref_tbl,
    ref_column = ref_column,

    join = function(self)
      -- use closure upvalues ref_tbl, ref_column for performance
      -- alongside with memoizing self.name, self.column (set on schema's
      -- initial assignment via __newindex) as this data are kind of persistent
      -- and obviously should not be changed after initialization

      -- method will redefine itself after memoizing needed values via upvalues

      column_name, self_tbl = self.column, self.table -- on first run

      -- this method is kinda shorthand proxy:
      --   tbl.ref_column:join():select{...}
      -- looks better then
      --   tbl:join{ref_table.name, my_column = 'ref_column'}

      self.join = function() -- redefine itself to use memoized stuff
        return self_tbl:join{ref_tbl_name, [column_name] = ref_column}
      end
      return self:join() -- shallow tail recursion (once and no more)
    end,
  }
end
local Referring_mt = {
  __call = generic_lazy_constructor_factory(referring_constructor)
}


local Referenced_mt = {
  __call = generic_lazy_constructor_factory()
}


ColumnTypes = {
  String     = setmetatable({}, String_mt), -- filtering on insert-update to implement
  Enum       = setmetatable({}, Enum_mt),
  Time       = setmetatable({}, Time_mt), -- timestamp
  Integer    = setmetatable({}, Integer_mt), -- referenced from may apply ??
  Referring  = setmetatable({}, Referring_mt), -- refers to other table
  Referenced = setmetatable({}, Referenced_mt), -- refers from (one to many)
}

Helpers = {
  -- this function misses the quoting issues, because now it is used for
  -- decoupling arrays of custom enum types or ip addresses, and not used
  -- for arrays of generic strings or so.
  -- in order to get a fully fledged method you need to improve that
  array_parse = function(arr_str)
    local rv

    if arr_str and '{' == arr_str:sub(1,1) and '}' == arr_str:sub(#arr_str) then
      arr_str = arr_str:sub(2, -1+#arr_str) -- strip the { } brackets
      rv = {}
      arr_str:gsub("[^,]+", function(item) rv[1+#rv] = item end)
    end

    return rv
  end,

  -- read about quoting issue above (array_parse() comment)
  array_compose = function(tbl)
    return tbl and '{'..table.concat(tbl, ',')..'}'
  end,

}

