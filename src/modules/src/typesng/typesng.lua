local getmetatable = getmetatable
local ipairs = ipairs

local orig_type_func = type

local function use_improved_type()
  if type == orig_type_func then
    type = function(object)
      local rv = orig_type_func(object)

      if rv == "table" then
        local mt = getmetatable(object)
        local type_or_type_func = mt and mt.__type or object.__type
    
        if type_or_type_func then
          if orig_type_func(type_or_type_func) == "function" then
            rv = type_or_type_func(object)
          else -- shall be string if not function, no assertion for performance
            rv = type_or_type_func
          end
        end
      end

      return rv
    end
  end
end

local rawtype = orig_type_func -- we still may have a need in old raw type func

local function type_verifiers_factory(types, env)
  env = env or _G -- default is global lexical scope
  -- some metaprogramming to create is_table(), is_... functions family
  for _,t in ipairs(types) do
    env["is_"..t] = function(object)
      return type(object) == t
    end
    env["isnot_"..t] = function(object)
      return type(object) ~= t
    end
  end
end

local typesng = {
  type_verifiers_factory = type_verifiers_factory,
  use_improved_type = use_improved_type,
}
type_verifiers_factory({ "table", "function", "number", "string", "thread" }, typesng)
use_improved_type()

return typesng

