export EPP_LUA_PATH='/set/your/path'

export LUA_PATH="$EPP_LUA_PATH/modules/lua/?.lua;./?.lua;/usr/share/lua/5.1/?.lua;/usr/share/lua/5.1/?/init.lua;/usr/lib/lua/5.1/?.lua;/usr/lib/lua/5.1/?/init.lua;../?.lua"
export LUA_CPATH="$EPP_LUA_PATH/modules/native/?.so;./?.so;/usr/lib/lua/5.1/?.so;/usr/lib/lua/5.1/loadall.so"

export EPP_PORT=7700
export EPP_BINDADDR='*'
export EPP_DB=epp_db
export EPP_DB_USER=epp_user
export EPP_DB_PWD=epp_db_password
export EPP_TLS_DIR="$EPP_LUA_PATH/keys/"

