from cgi import escape

out_file = "./index.tir"

error_subtab_content = """
"""

html_begin = """<html>
<head>
<title> EPP </title>
<link rel='stylesheet' href='style.css'>
"""

html_middle = """
</head>

<body onload="javascript:init_tabs()">
<br />
"""

html_end = """
{( 'response.tir' )}
</body></html>"""

si_content_tpl = """<div style='display: none' class='contenttab' name='sublayer_%(l)s_%(sl)s' id='sublayer_%(l)s_%(sl)s'>
<a name="item_%(l)s_%(sl)s"></a>
%(c)s
</div>
<a class="hidden" href="#top">Return back</a>
"""

sub_item_tpl = """<div style='display: none' class='bottommenu' name='layer_%(l)s' id='layer_%(l)s'>
<a name="item_%(l)s"></a>
%(c)s
</div>
"""

si_menu_tpl = """
<span class='bottommenuitem' style='width: %(w)spx; ' name='menu_%(l)s_%(sl)s' id='menu_%(l)s_%(sl)s'>
<a onclick="javascript:activate_subtab(%(l)s,%(sl)s); return false;" href='#item_%(l)s_%(sl)s' class='menus_d'>%(t)s</a>
</span>
"""

si_menu_joiner = ""

ti_menu_tpl = """
<span class='topmenuitem' style='width: %(w)spx; ' name='menu_%(l)s' id='menu_%(l)s'>
<a href='#item_%(l)s' onclick='javascript:activate_tab(%(l)s); return false;' class='menu' name='anchor_%(l)s' id='anchor_%(l)s'>%(t)s</a>
</span>
"""

ti_menu_joiner = ""

top_item_tpl = """
<div class='topmenu'>
<a name="top"><a class='hidden' href=''>Reload page<br /></a></a>
%s
</div>

"""

js_template = """
<script language="javascript" type="text/javascript">

var tabs_cnt = %s
var subtabs_cnt = %s

function init_tabs() {
   var tab = getCookie("tabnum") || 1;
   var subtab = getCookie("subtabnum") || 1;
   activate_subtab(tab,subtab);
}

</script>
<script language="javascript" type="text/javascript" src="js.js"></script>
"""

def wrap_in_form(template_name):
  return "<form action='/req:epp' method='post'>" + \
         "{('"+template_name+"')}</form>"

top_items = [
  {'title': "Domains",
  'subitems': [
    {'title': "d::check", 'content': wrap_in_form('domain_check.tir') },
    {'title': "d::info", 'content': wrap_in_form('domain_info.tir') },
    {'title': "d::create", 'content': wrap_in_form('domain_create.tir') },
    {'title': "d::delete", 'content': wrap_in_form('domain_delete.tir') },
    {'title': "d::renew", 'content': wrap_in_form('domain_renew.tir') },
    {'title': "d::transfer", 'content': wrap_in_form('domain_transfer.tir') },
    {'title': "d::update", 'content': wrap_in_form('domain_update.tir') },
    {'title': "d::restore", 'content': wrap_in_form('domain_restore.tir') },
  ]},
  {'title': "Hosts",
  'subitems': [
    {'title': "h::check", 'content':wrap_in_form('host_check.tir')},
    {'title': "h::info", 'content':wrap_in_form('host_info.tir')},
    {'title': "h::create", 'content':wrap_in_form('host_create.tir')},
    {'title': "h::delete", 'content':wrap_in_form('host_delete.tir')},
    {'title': "h::update", 'content':wrap_in_form('host_update.tir')},
  ]},
  {'title': "Contacts",
  'subitems': [
    {'title': "c::check", 'content':wrap_in_form('contact_check.tir')},
    {'title': "c::info", 'content':wrap_in_form('contact_info.tir')},
    {'title': "c::create", 'content':wrap_in_form('contact_create.tir')},
    {'title': "c::delete", 'content':wrap_in_form('contact_delete.tir')},
    {'title': "c::update", 'content':wrap_in_form('contact_update.tir')},
    {'title': "c::transfer", 'content':wrap_in_form('contact_transfer.tir')},
  ]},
  {'title': "Messages",
  'subitems': [
    {'title': "poll::req", 'content':wrap_in_form('poll_req.tir')},
    {'title': "poll::ack", 'content':wrap_in_form('poll_ack.tir')},
  ]},
  {'title': "Session",
  'subitems': [
    {'title': "::login", 'content':wrap_in_form('login.tir')},
    {'title': "::logout", 'content':wrap_in_form('logout.tir')},
    {'title': "::hello", 'content':wrap_in_form('hello.tir')},
    {'title': "History", 'content':wrap_in_form('history.tir')},
  ]},
]

tpl_hidden_hr = "<hr class='hidden' />"

