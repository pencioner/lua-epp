#!/usr/bin/python
from tpl_html_gen import *


width_units_value = 1024

tabs_cnt = len(top_items)
js_subtabs_cnt, i = {}, 1
for top_item in top_items:
	js_subtabs_cnt[i] = len(top_item['subitems'])
	i+=1

ti_menu, si_menus, si_contents, i = [], [], [], 1
width = (width_units_value//tabs_cnt)
for top_item in top_items:
	ti_menu.append(ti_menu_tpl % {'l': i, 'w': width, 't': top_item['title']})
	si_menu, j = [], 1
	for sub_item in top_item['subitems']:
		subs_cnt = len(top_item['subitems'])
		s_width = (width_units_value//subs_cnt)
		si_menu.append(si_menu_tpl % {'l': i, 'sl': j, 'w': s_width, 't': sub_item['title']})
		si_contents.append(si_content_tpl % {'l': i, 'sl': j, 'c': sub_item['content']})
		j+=1
	si_menus.append(sub_item_tpl % {'l': i, 'c': si_menu_joiner.join(si_menu)})
	i+=1

f = open(out_file, "w");
f.write ( html_begin + js_template % (tabs_cnt, repr(js_subtabs_cnt)) +
		  html_middle + top_item_tpl % ti_menu_joiner.join(ti_menu) +
		  tpl_hidden_hr + "".join(si_menus) + tpl_hidden_hr +
		  si_content_tpl % {'l':'', 'sl': 'error', 'c':''} + "".join(si_contents) +
		  html_end
		)
f.close()


